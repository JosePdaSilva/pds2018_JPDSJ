<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Obrix</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="img/obrix_branco.png" style="height: 40px"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#como-funciona" style="text-shadow:1px 1px 1px rgba(0,0,0,0.5)">Como Funciona</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#parceiro" style="text-shadow:1px 1px 1px rgba(0,0,0,0.5)">Faça parte</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="register_fornecedor.php" style="text-shadow:1px 1px 1px rgba(0,0,0,0.5)">Inscreva-se</a>
            </li>
          </ul>
          <a href="index.php"><button type="button" class="btn btn-outline-info" style="font-weight: 500;">Sou Construtor!</button></a>
        </div>
      </div>
    </nav>

    <section id="fornecedores">
      <div class="container">
        <div class="col-lg-4 jumbo" style="position: absolute; top: 35%; padding-right: 15%; margin: 0px;">  
          <div class="jumbotron" style="background: rgb(252,176,13);">
            <h5 style="color: white">Expanda seus horizontes</h5>
            <a href="#como-funciona" class="expandir-como js-scroll-trigger">Veja como <span class="fa-stack expandir-como" style="position: relative; top: -1px;"> <i class="fa fa-angle-right fa-stack-1x"></i></span></a>
          </div>
        </div>
      </div>
    </section>

    <section id="como-funciona">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Como funciona</h2>
            <h3 class="section-subheading">Expanda suas vendas, não fique esperando</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-envelope fa-stack-1x" style="color:rgb(252,176,13)"></i>
            </span>
            <h5>Receba cotações</h5>
            <p style="font-size: 15px;">Receba em seu e-mail os materiais e quantidades que as construtoras estão comprando</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-handshake-o fa-stack-1x" style="color:rgb(252,176,13)"></i>
            </span>
            <h5>Feche negócios</h5>
            <p style="font-size: 15px;">Submeta sua proposta comercial e feche novos negócios</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-shield fa-stack-1x" style="color:rgb(252,176,13)"></i>
            </span>
            <h5>Venda de forma segura</h5>
            <p style="font-size: 15px;">Nós acompanhamos constantemente os pedidos feitos na plataforma</p>
          </div>
        </div>
      </div>  
    </section>

    <!-- Contact -->
    <section id="parceiro" style="padding-bottom: 0px; padding-top: 0px;">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-5 time" style="padding-bottom: 50px; padding-top: 50px"></div>
          <div class="col-lg-7" style="background: #F5F5F5; padding-bottom: 50px;padding-top: 50px;">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Gostou?</h2>
                <h3 class="section-subheading">Mande nos uma mensagem. Entraremos em contato.</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <form id="" name="sentMessage" novalidate="novalidate" method="post" action="envia_email.php">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input class="form-control" id="name" name="email[NOME]" type="text" placeholder="Seu nome *" required="required" data-validation-required-message="Por favor, preencha seu nome.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" name="email[EMAIL]" id="email" type="email" placeholder="Seu e-mail *" required="required" data-validation-required-message="Por favor, preencha seu e-mail.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" name="email[TELL]" id="phone" type="tel" placeholder="Seu telefone *" required="required" data-validation-required-message="Por favor, preencha seu telefone.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" name="email[EMPRESA]" id="company" type="text" placeholder="Sua empresa *" required="required" data-validation-required-message="Por favor, preencha o nome da sua empresa.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" name="email[RAMO]" id="ramo" type="text" placeholder="Ramo (Construtora ou Fornecedora) *" required="required" data-validation-required-message="Por favor, preencha o ramo de sua empresa.">
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <textarea style="height:255px" name="email[MENSAGEM]" class="form-control" id="cnpj" type="text" placeholder="Mensagem *" required="required" data-validation-required-message="Por favor, preencha o cnpj da sua empresa."></textarea>
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 text-center">
                      <div id="success"></div>
                      <button type="submit" id="btn_submit" name="submit" class="btn btn-primary btn-xl text-uppercase" >Seja um parceiro</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Obrix 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Políticas de Privacidade</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Termos de Uso</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    	
	
	<?php 
        if(isset($_SESSION['enviado']))
        {
    ?>
            <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style='background-color:  #28a745'>
                    <h5 class="modal-title">Sucesso!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <p>E-mail enviado com sucesso.</p>
                  <p>Obrigado por entrar em contato, equipe Obrix agradece.</p>
                  
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-success" type="button" data-dismiss="modal">OK</button>
                  </div>
                </div>
              </div>
            </div>
            
            <script type="text/javascript">
        
            var alertModal = $('#alertModal');
        	alertModal.modal("show");
        	
            </script>
    
    <?php
            unset($_SESSION['enviado']);
        }
    ?>

  </body>

</html>
