<?php

require_once("vendor/phpmailer/PHPMailerAutoload.php");

class sendemail
{
    function __construct($qq = array())
    {
        // Inicia a classe PHPMailer
        $mail = new PHPMailer(true);
        
        // Método de envio
        $mail->IsSMTP(); // Enviar por SMTP
        try {
           // $mail->SMTPDebug = 4; // Você pode habilitar esta opção caso tenha problemas. Assim pode identificar mensagens de erro.
            $mail->Host = "smtp-mail.outlook.com"; // Você pode alterar este parametro para o endereço de SMTP do seu provedor
            $mail->Port = 995;
            
            $mail->Username = 'triforce-roupas@hotmail.com'; // Usuário do servidor SMTP (endereço de email)
            $mail->Password = 'triforce666'; // Mesma senha da sua conta de email
            $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            
            $mail->From = "triforce@hotmail.com"; // Seu e-mail
            $mail->FromName = "Triforce";
            // Define o(s) destinatário(s)
            //$mail->AddAddress($received[0], $received[1]);
           $mail->AddAddress('juninho_444@hotmail.com');
            
            $mail->IsHTML(true); // Formato HTML . Use "false" para enviar em formato texto simples.
            $mail->CharSet = 'UTF-8'; // Charset (opcional)
            // Assunto da mensagem
            $mail->Subject = "Fale Conosco";
            
            //Campos abaixo são opcionais
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
            //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
            //$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
            
            
            //Define o corpo do email
            //$mail->MsgHTML('corpo do email');
            
            $html = file_get_contents('temp.email.html');
            
            $vars = array (
                'remetente' => $qq['EMAIL'],
                'nome' => $qq['NOME'],
                'telefone' => $qq['TELL'],
                'mensagem' => $qq['MENSAGEM']
            );
            
            $template = array();
            foreach ($vars as $key => $value) {
                $html = str_replace('{{'.$key.'}}', $value, $html);
            }
            
            //$mail->IsHTML(true);
            //$mail->Body = $html;
            $mail->MsgHTML($html);
            
            ////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
            //$mail->MsgHTML(file_get_contents('arquivo.html'));
            
            $mail->Send();
            // echo "Mensagem enviada com sucesso</p>\n";
            return true;
            //caso apresente algum erro é apresentado abaixo com essa exceção.
        }
        catch (phpmailerException $e) {
            //echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
            return false;
        }
    }
}

if(isset($_POST['submit'])){
    session_start();
    if(new sendemail($_POST['email']))
        $_SESSION['enviado']=1;
    else 
        $_SESSION['enviado']=2;
    header('location: index.php');
    exit();
}
?>