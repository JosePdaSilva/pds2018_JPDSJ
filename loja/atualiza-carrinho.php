<?php
require_once 'config.php';


if(isset($_POST['id_produto']))
{
	if(isset($_POST['quantidade']))
	{
		if(isset($_SESSION['carrinho'][$_POST['id_produto']]))
		{
			$_SESSION['carrinho'][$_POST['id_produto']]['QUANTIDADE'] = $_POST['quantidade'];
			echo "ok";
		}
		else
		{
			echo "Produto não existe mais no carrinho";
		}
	}
	else 
	{
		echo "Quantidade para o produto não foi passada";
	}
}
else
{
	echo "Produto não informado";
}