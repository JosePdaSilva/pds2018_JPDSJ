<?php 
require_once 'config.php';

if(isset($_POST['submit']))
{
    if(isset($_SESSION['carrinho']) && $_SESSION['carrinho']>0)
    {
        foreach ($_SESSION['carrinho'] as $prod)
        {
            $prod['ID_USUARIO'] = $_SESSION['id_usuario'];
            save('TBL_VENDA', $prod);
            
            if($_SESSION['type']=="success")
            {
                $_SESSION['message'] = "Pedido realizado com sucesso";
                unset($_SESSION['carrinho']);
            }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Triforce</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="img/triforce_logo.png" style="height: 40px"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#como-funciona" style="text-shadow:1px 1px 1px rgba(0,0,0,0.5)">Ver carrinho</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="register.php" style="text-shadow:1px 1px 1px rgba(0,0,0,0.5)">Inscreva-se</a>
            </li>
          </ul>
          <a href="index.php"><button type="button" class="btn btn-outline-info" style="font-weight: 500;">Loja</button></a>
          
          <a style="margin-left: 20px" class="btn btn-outline-danger" href="software/logoff.php">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
          
        </div>
      </div>
    </nav>

    <section id="fornecedores">
      <div class="container">
        <div class="col-lg-4 jumbo" style="position: absolute; top: 35%; padding-right: 15%; margin: 0px;">  
          <div class="jumbotron" style="background: rgb(252,176,13);">
          <div class="text-center">
          <i class="fa fa-shopping-cart" style="font-size:100px;"></i>
          </div>
          </div>
        </div>
      </div>
    </section>

    <section id="como-funciona">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Carrinho</h2>
          </div>
        </div>
        <form id="geralProdutos" action="carrinho.php" method="post">
        
        <?php 
     //$roupasF = find_id('tbl_produtos',['SEXO','PRINCIPAL'],['F','S']);
        if(isset($_SESSION['carrinho']) && count($_SESSION['carrinho'])>0)
        {
    foreach ($_SESSION['carrinho'] as $idProduto=>$v)
        {
            $res = find_id('tbl_produtos', 'ID_PRODUTO', $idProduto);
            $roupa = $res[0]; 
        ?>
<div class="row" id="produto<?= $idProduto ?>" style="border:1px solid #ccc; padding:30px;">
   <div class="col-md-2">
          <span><img src="data:image/png;base64,<?php echo base64_encode($roupa['FOTO']);?>" alt="FOTO DA ROUPA" style="height: 200px"></span>
    </div>
  <div class="col-md-4 offset-md-1" style="margin-top:50px">
    <h5 style="color:black"><?php echo $roupa['DESCRICAO']?></h5>
    <h4 style="color:black">R$ <?php echo number_format($roupa['PRECO_UNITARIO'],2,",",".")?></h4>
    <h3 style="font-size: 15px;">Marca: <?php echo $roupa['MARCA']?></h3>
    <h3 style="font-size: 15px;">Tamanhos disponíveis: <?php echo $roupa['TAMANHOS']?></h3>
  </div>
          
   <div class="col-md-4" style="margin-top:50px">
    <h5 style="color:black">Quantidade desejada</h5>
    <div class="col-md-4 offset-md-2">
    <input type="number" class="form-control" required value="<?php if(isset($v['QUANTIDADE'])) echo $v['QUANTIDADE']; ?>" oninput="atualizaProduto(<?= $idProduto ?>, this.value)">
   	</div>
  </div>
          
    <div class="col-md-1 text-center" style="margin-top:50px">
    <h5 style="color:black">Tirar</h5>
    <div class="col-md-4">
    <button type="button" class="btn btn-danger" class="form-control" onclick="excluiProduto(<?= $idProduto ?>)">X</button>
   	</div>
  </div>
  
</div>
		<?php 
       	}
       	?>
       	<br>
       	<div class="row">
        <div class="col-md-2 offset-md-4">
        <button class="btn btn-success" type="submit" name="submit" >Finalizar compra</button>
        </div>
        <div class="col-md-2">
        <a class="btn btn-secondary" href="index.php">Voltar a loja</a>
        </div>
       	</div>
       	<?php
        }
        else 
        {
        ?>
        <div class="row">
        	<div class="col-md-12">
        		<h4 style="color: orange; padding-left:42%"><u>Carrinho Vazio</u></h4>
        	</div>
        </div>
        <br>
        <div class="row">
        <div class="col-md-2">
        <a class="btn btn-secondary" href="index.php">Voltar a loja</a>
        </div>
       	</div>
        <?php 
        }
        ?>	
        </form>
     
       </div>
    </section>

    <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="responseModal" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title"></h5>
    				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
    				<span aria-hidden="true">×</span>
    				</button>
    			</div>
    			<div class="modal-body"></div>
    			<div class="modal-footer">
    				<button class="btn btn-" id="btn-ok" type="button" data-dismiss="modal">OK</button>
    			</div>
    		</div>
    	</div>
    </div>

    <!-- Footer -->
       <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Triforce 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/triforce" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.facebook.com/triforce/" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.linkedin.com/company/triforce/" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
               <strong>Criador:</strong> <a href="https://www.facebook.com/juninho.silva.5076">JPS Jr</a>
              </li>
             
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
    	
	
	<?php 
        if(isset($_SESSION['enviado']))
        {
    ?>
            <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style='background-color:  #28a745'>
                    <h5 class="modal-title">Sucesso!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <p>E-mail enviado com sucesso.</p>
                  <p>Obrigado por entrar em contato, equipe Obrix agradece.</p>
                  
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-success" type="button" data-dismiss="modal">OK</button>
                  </div>
                </div>
              </div>
            </div>
            
            <script type="text/javascript">
        
            var alertModal = $('#alertModal');
        	alertModal.modal("show");
        	
            </script>
    
    <?php
            unset($_SESSION['enviado']);
        }
    ?>

  </body>
  
  <script type="text/javascript">
  function atualizaProduto(id, quantidade)
  {
		var responseModal = $('#responseModal');
  
		$.ajax({
		 url: 'atualiza-carrinho.php',
		 type: "POST",
		 data: "id_produto="+id+"&quantidade="+quantidade,
		 success: function(rep) {
				if(rep == 'ok'){
					
				}else{
					responseModal.find('.modal-title').html('<strong>Erro</strong>');
					responseModal.find('.modal-header').attr('style', "background-color:#dc3545");
					responseModal.find('.modal-body').html('<p><strong>'+rep+'</strong></p>');
					responseModal.find('#btn-ok').attr('class', 'btn btn-danger');
					responseModal.modal("show"); 
				}
				 
			}
		});
  }

  function excluiProduto(id)
  {
		var responseModal = $('#responseModal');
		
		$.ajax({
		 url: 'exclui-carrinho.php',
		 type: "POST",
		 data: "id_produto="+id,
		 success: function(rep) {
				if(rep == 'ok'){
					var filha = document.getElementById('produto'+id);
					var pai = document.getElementById("geralProdutos");
					pai.removeChild(filha);

					responseModal.find('.modal-title').html('<strong>Sucesso</strong>');
					responseModal.find('.modal-header').attr('style', 'background-color:  #28a745');
					responseModal.find('.modal-body').html('<p><strong>Produto removido do carrinho</strong></p>');
					responseModal.find('#btn-ok').attr('class', 'btn btn-success');
				}else{
					responseModal.find('.modal-title').html('<strong>Erro</strong>');
					responseModal.find('.modal-header').attr('style', "background-color:#dc3545");
					responseModal.find('.modal-body').html('<p><strong>'+rep+'</strong></p>');
					responseModal.find('#btn-ok').attr('class', 'btn btn-danger');
				}
				responseModal.modal("show"); 
				 
			}
		});
  }
  <?php 
  if(isset($_SESSION['type']))
  {
      if($_SESSION['type']=="success")
      {
  ?>

  var responseModal = $('#responseModal');
  
	responseModal.find('.modal-title').html('<strong>Sucesso</strong>');
	responseModal.find('.modal-header').attr('style', 'background-color:  #28a745');
	responseModal.find('.modal-body').html('<p><strong><?= $_SESSION['message']; ?></strong></p>');
	responseModal.find('#btn-ok').attr('class', 'btn btn-success');

	responseModal.modal("show"); 
  <?php
  unset($_SESSION['type']);
  unset($_SESSION['message']);
      }
      
  }
  ?>
  </script>

</html>
