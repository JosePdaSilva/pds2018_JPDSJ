<?php 
require_once 'config.php';
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Triforce</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126227889-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126227889-1');
</script>


  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="img/triforce_logo.png" style="height: 40px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
         <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="roupas-masculinas.php">Masculinas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#beneficios">Femininas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="roupas-unissex.php">Unissex</a>
            </li>
          </ul>
          <a href="software/"><button type="button" class="btn btn-outline-info" style="margin-right: 10px;font-weight: 500;">Login</button></a>
          <a href="carrinho.php"><button type="button" class="btn btn-outline-warning" style="font-weight: 500;"><i class="fa fa-shopping-cart"></i> Ver carrinho</button></a>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text" style="padding-top: 23%;">
          <div class="intro-lead-in" style="color: orange; font-size: 30px; text-shadow: 1px 1px 1px rgba(0,0,0,1)">Procurando a mellhor roupa com o melhor preço ? </div>
          <div class="intro-heading" style="font-size: 50px; text-shadow: 1px 1px 1px rgba(0,0,0,1)">Esolha Triforce</div>
         <!--  <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#funcionamento">Quero conhecer!</a> -->
        </div>
      </div>
      
      
        <div class="row text-center">
        <div class="col-md-4">
        	<a href="roupas-masculinas.php" class="btn btn-success">Roupas Masculinas</a>
        </div>
         <div class="col-md-4">
        	<a href="roupas-feminina.php" class="btn btn-danger">Roupas Femininas</a>
        </div>
         <div class="col-md-4">
        	<a href="roupas-unissex.php"class="btn btn-warning">Roupas Unissex</a>
        </div>
        </div>
    </header>


    
    
   
    <!-- Beneficios -->
    <section id="beneficios">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase" style="color:orange;padding-bottom:60px""><u>ROUPAS FEMININAS</u></h2>
          </div>
        </div>
        <div class="row text-center">
         <?php  ;
         $roupasF = find_id('tbl_produtos','SEXO','F');
        /*echo '<pre>';
        print_r($qq);
        echo '</pre>';*/
        
        foreach ($roupasF as $roupa)
        {
        ?>
        
          <div class="col-md-4 col-xs-12 col-sm-6 col-lg-3">
            <span class="fa-stack fa-4x" style='display: inline'>
              <span><img src="data:image/png;base64,<?php echo base64_encode($roupa['FOTO']);?>" alt="FOTO DA ROUPA" style="height: 200px"></span>
            </span>
            <h5 style="color:black"><?php echo $roupa['DESCRICAO']?></h5>
            <h4 style="color:black">R$ <?php echo number_format($roupa['PRECO_UNITARIO'],2,",",".")?></h4>
            <h3 style="font-size: 15px;">Marca: <?php echo $roupa['MARCA']?></h3>
            <h3 style="font-size: 15px;">Tamanhos disponíveis: <?php echo $roupa['TAMANHOS']?></h3>
            <button type="button" class="btn btn-outline-dark" onclick="incluiProduto(<?= $roupa['ID_PRODUTO']; ?>)"><i class="fa fa-shopping-cart"></i> Colocar no carrinho</button>
          </div>
		<?php 
       	}
        ?>
        </div>
      </div>
    </section>

   <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="responseModal" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title"></h5>
    				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
    				<span aria-hidden="true">×</span>
    				</button>
    			</div>
    			<div class="modal-body"></div>
    			<div class="modal-footer">
    				<button class="btn btn-" id="btn-ok" type="button" data-dismiss="modal">OK</button>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Triforce 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/triforce" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.facebook.com/triforce/" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.linkedin.com/company/triforce/" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
               <strong>Criador:</strong> <a href="https://www.facebook.com/juninho.silva.5076">JPS Jr</a>
              </li>
             
            </ul>
          </div>
        </div>
      </div>
    </footer>

    
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>

	
	
  </body>

 <script type="text/javascript">
  function incluiProduto(id)
  {
		var responseModal = $('#responseModal');
  
		$.ajax({
		 url: 'inclui-carrinho.php',
		 type: "POST",
		 data: "id_produto="+id,
		 success: function(rep) {
				if(rep == 'ok'){
					responseModal.find('.modal-title').html('<strong>Sucesso</strong>');
					responseModal.find('.modal-header').attr('style', 'background-color:  #28a745');
					responseModal.find('.modal-body').html('<p><strong>Produto adicionado no carrinho</strong></p>');
					responseModal.find('#btn-ok').attr('class', 'btn btn-success');
				}else{
					responseModal.find('.modal-title').html('<strong>Erro</strong>');
					responseModal.find('.modal-header').attr('style', "background-color:#dc3545");
					responseModal.find('.modal-body').html('<p><strong>'+rep+'</strong></p>');
					responseModal.find('#btn-ok').attr('class', 'btn btn-danger');
					
				}
				responseModal.modal("show");  
			}
		});
  }

  
  </script>
  
</html>
