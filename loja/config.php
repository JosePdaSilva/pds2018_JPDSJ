<?php

/** caminho absoluto para a pasta do sistema **/
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** caminho no server para o sistema **/
if($_SERVER['HTTP_HOST']=='localhost')
{
    if ( !defined('BASEURL') )
        define('BASEURL', '/loja/software/');
}
elseif($_SERVER['HTTP_HOST']=='obrix.com.br')
{
    if ( !defined('BASEURL') )
        define('BASEURL', '/software/');
}
else
{
	echo 'Configure BASEURL para: '. $_SERVER['HTTP_HOST'];
	exit();
	define('BASEURL', 'BASEURL n�o est� definido - o sistema n�o ir� funcionar');
}

	
/** caminho do arquivo de banco de dados **/
if ( !defined('DBAPI') )
		define('DBAPI', ABSPATH . 'includes/database.php');

if(!defined('IMAGE_FOLDER'))
	define('IMAGE_FOLDER',  '../imagens/');
		
if ( !defined('CONFIG_DB') )
	define('CONFIG_DB', ABSPATH . 'config/');

//if ( !defined('FILE_FOLDER') )
//	define('FILE_FOLDER', ABSPATH . 'files/');
	

/** caminhos dos templates de header e footer **/
define('HEADER', ABSPATH. 'includes/header.php');
define('FOOTER', ABSPATH. 'includes/footer.php');
define('DELETEMODAL', ABSPATH. 'includes/delete-modal.php');
define('AVALIAMODAL', ABSPATH. 'includes/avaliacao-modal.php');

require_once DBAPI;	

session_start();

?>