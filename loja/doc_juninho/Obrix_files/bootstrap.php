(function() {
	var _smartsupp = {};
	var smartsupp = window.parent.smartsupp;
	var smartsuppChat = parent.smartsupp.chats[smartchatId];
	
	var _config = {"package":"trial","lang":"br","orientation":"right","hideBanner":false,"hideWidget":false,"hideOfflineBanner":true,"enableRating":false,"ratingComment":false,"requireLogin":false,"hideOfflineChat":false,"muteSounds":false,"isEnabledEvents":true,"banner":{"type":"arrow","options":{}},"translates":{"online":{"title":"Fale conosco","infoTitle":"Obrix","infoDesc":"Ol\u00e1, posso ajudar?","send":"Enviar","textareaPlaceholder":""},"offline":{"submit":"Enviar"},"widget":{"online":"Online"},"banner":{"arrow":{"title":"Fale conosco","desc":""},"bubble":{}}},"colors":{"primary":"#3598dc","banner":"#f8e71c","widget":"#3598dc"},"theme":{"name":"flat","options":{"widgetRadius":3}},"online":{},"offline":{},"api":{"basic":true,"banner":true,"events":true,"groups":true,"theme":true},"transcriptEnabled":false,"privacyNoticeUrl":"","privacyNoticeEnabled":true};
	_config.baseLang = 'br';
	_config.browserLang = 'pt';
	_config.avatar = '/chats/230320/avatar-91m8oilvge.png';
	_config.avatarPath = '/widgets/avatars/CWVd_IJW65.png';
	_config.host = 's18.smartsupp.com';
	_config.packageName = 'trial';
	_config.logoUrl = '';
	_config.logoSrc = '';
	_config.logoSmSrc = '';
	_config.smartlook = window.smartlook;
	
	var smartsuppLoadInterval = setInterval(function() {
		if (!window.miwo) return;
		clearInterval(smartsuppLoadInterval);
		smartsuppChat.setOptions(_smartsupp);
		
		miwo.ready(function() {
			miwo.cookie.document = parent.document;
			miwo.baseUrl = smartsuppChat.getOption('baseUrl');
			miwo.staticUrl = smartsuppChat.getOption('staticUrl');

			var configurator = new Miwo.Configurator();
			configurator.addConfig(App.DefaultConfig.getConfig());
			configurator.addConfig(App.ClientConfig.getConfig(_config));
			configurator.addConfig(App.ChatConfig.getConfig(smartsuppChat));

			configurator.ext(new Chat.ChatExtension());
			var container = configurator.createContainer();
			container.get('miwo.application').run();
		});
	}, 50);
	
	if (_config.api.events && smartsuppChat.getOption('gaKey')) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	}
	
	
	if(!window.parent.visitortrace && !window.parent.smartlook && window.parent._smartsupp && smartsuppChat.getOption('recordingDisable')!==true && smartsuppChat.getOption('visitortraceDisable')!==true) {
		var _smartlook = window.parent._smartlook || window.parent._visitortrace || {};
		_smartlook.host = 'manager.smartlook.com';
		_smartlook.window = window.parent;
		_smartlook.document = window.parent.document;
		if(window.parent._smartsupp.cookieDomain && !_smartlook.cookieDomain){ _smartlook.cookieDomain = window.parent._smartsupp.cookieDomain; }
		if(window.parent._smartsupp.cookiePath && !_smartlook.cookiePath){ _smartlook.cookiePath = window.parent._smartsupp.cookiePath; }
	
		window.smartlook||(function(d) {var o=smartlook=function(){ o.api.push(arguments)},
		s=d.getElementsByTagName('script')[0],c=d.createElement('script');o.api=[];c.type='text/javascript';
		c.async=true;c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';s.parentNode.insertBefore(c,s);})(window.parent.document);
		window.parent.smartlook = window.smartlook;
	
		smartlook('init', '04559758e11edce4dc80a4e352b40f1cf29c643b', _smartlook);
		smartlook('expertForms');
		smartlook('expertAPI');
	}
	
	
	!smartsuppChat.getOption('disableInternalApi') && (function() {
		
	})();
})();
