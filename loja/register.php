<?php

    require_once 'config.php';
    
    if(isset($_POST['submit']))
    {
        
        require_once 'includes/database.php';
        $empresa = (isset($_POST['cliente'])>0)?$_POST['cliente']:array();
        $usuario = (isset($_POST['usuario'])>0)?$_POST['usuario']:array();
        
        
        $all = find_all('TBL_USUARIOS');
        
        $already_exists=false;
        foreach ($all as $user)
        {
            if(strtolower($user['USERNAME'])==strtolower($usuario["USERNAME"]))
            {
                $already_exists = true;
                break;
            }
        }
        
        if($already_exists)
        {
            $salvou = false; 
        }
        else
        {
        $usuario=$_POST['usuario'];
        $usuario['ID_PERFIl'] = "2";
        $usuario["PASSWORD"] = md5($usuario["PASSWORD"]);
        
        $id = save('TBL_USUARIOS', $usuario);
       
        $empresa=$_POST['cliente'];
        $empresa['ID_USUARIO'] = $id;
        $empresa['EMAIL'] = $usuario['EMAIL'];
        
        save('tbl_cliente',$empresa);
        
        $salvou = true; 
        
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

  <head>
  	

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Triforce</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
	
	
    <script src="js/enderecos.js"></script>
    <script src="js/contatos.js"></script>
    <script src="js/carregar.js"></script>
    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="img/triforce_logo.png" style="height: 40px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php"><i class="fa fa-chevron-left"></i> Voltar ao Site</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Beneficios -->
 <section style="background-color: #22262e;background-repeat: no-repeat;background-attachment: scroll;background-position: center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover; margin-top:none">
   <div class="container" style="padding-bottom: 50px;">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">
      <div class="row">  
        <div class="col-md-12">Inscreva-se no sistema!</div>
      </div>
      </div>
      <div class="card-body">
        <form action="register.php" method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">  
                <label for="exampleInputName">Nome</label>
                <input class="form-control" required id="nome" name="cliente[NOME]" type="text" aria-describedby="nameHelp" placeholder="Nome *">
              </div>
              <div class="col-md-6">  
                	<label for="exampleInputEmail1">CPF</label>
                	<input class="form-control" required id="cnpj" type="text" OnKeyPress="formatar('###.###.###-##', this)" maxlength="14" name="cliente[CPF]" aria-describedby="emailHelp" placeholder="CPF *">
              </div>
            </div>
          </div>
   			
   		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-9">  
                <label for="exampleInputName">Endereço</label>
                <input class="form-control" required id="nome" name="cliente[ENDERECO]" type="text" aria-describedby="nameHelp" placeholder="Endereço*">
              </div>
              <div class="col-md-3">  
                <label for="exampleInputName">Telefone</label>
                <input class="form-control" required id="nome" name="cliente[TELEFONE]" type="text" aria-describedby="nameHelp" placeholder="Telefone*">
              </div>
            </div>
          </div>
          
   			
          <div class="form-group">
            <div class="form-row">
            
            <div class="col-md-4">  
                <label for="exampleInputEmail">E-Mail</label>
                <input class="form-control" required id="email" type="text" name="usuario[EMAIL]" aria-describedby="emailHelp" placeholder="E-Mail *">
              </div>
            
             <div class="col-md-4">  
                <label for="exampleInputUsername">Nome de Usuário</label>
                <input class="form-control" required id="username" name="usuario[USERNAME]" type="text" aria-describedby="emailHelp" placeholder="Nome de Usuário *">
              </div>
              
              <div class="col-md-4">  
                <label for="exampleInputSenha">Senha</label>
                <input class="form-control" required id="senha1" name="usuario[PASSWORD]" type="password" aria-describedby="nameHelp" placeholder="Senha *">
              </div>
            </div>
          </div>
          <button class="btn btn-warning btn-block" type="submit" id="btn_submit" name="submit">Inscrever-se</button>
        </form>
      </div>
    </div>
  </div>
</section>

    

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>
    
    <script src="js/buscaCEP.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    
     <?php 
     //$salvou = 0; // Coloquei isso pra parar o erro mas deve dar alguma merda.
     if(isset($_POST['submit']) && $salvou)
            {
        ?>   
        <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background-color: #28a745;">
                    <h5 class="modal-title" style="color:white;">Sucesso!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body"><h4>Cadastro realizado com sucesso</h4>
                  <p>Acompanhe os lançamentos e fique por dentro do que há de melhor no mercado da moda</p>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-success" href="index.php">Ir para a loja</a>
                  </div>
                </div>
              </div>
            </div>
       <script type="text/javascript">
        
            var alertModal = $('#alertModal');
        	alertModal.modal("show");
        	
        </script>
            <?php 
            }
            else if(isset($_POST['submit']) && !$salvou)
            {
            ?>
            <div class="modal fade" id="alertModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background-color: #dc3545;">
                    <h5 class="modal-title" style="color:white;">ERRO!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body"><h4>Este Nome de Usuário ja existe.</h4>
                  <p><strong>OBS: Faça um novo cadastro usando outro nome de usuário!</strong></p>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">OK</button>
                  </div>
                </div>
              </div>
            </div>
            <script type="text/javascript">
        
            var alertModal2 = $('#alertModal2');
        	alertModal2.modal("show");
        	
        </script>
            
            <?php 
            }
            ?>
    

  </body>

</html>
