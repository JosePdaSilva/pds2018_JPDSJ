<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TLoggerTXT
 *
 * @author Eleandro
 */
class TLoggerTXT extends TLogger {
  
    /*
     * 
     * metodo write()
     * escreve uma mensagem no arquivo LOG
     * @param $message = mensagem a ser escrita
     * 
     */
    
    public function write($message)
    {
        $time = date("Y-m-d H:i:s");
        //monta a string
        
        $text= "$time :: $message\n";
        //adiciona ao final do arquivo
        
        $handler = fopen($this->filename, 'a');
        fwrite($handler, $text);
        fclose($handle);
        
    }
    
}
?>
