<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TFilter
 *
 * @author Eleandro
 */
class TFilter extends TExpression{
    //put your code here
    private $variable; //variavel
    private $operator; //operadodr
    private $value; //valor
    
    /*
     * metodo __construct()
     * instancia um novo filtro
     * @param $variable = variável
     * @param $operator = operador (>,<)
     * @param $value = valor a ser comparado
     */
    
    public function __construct($variable, $operador, $value) {
        $this->variable = $variable;
        $this->operator = $operador;
        
        //transforma o valor de acordo com certas regras
        //antes de atribuir a propriedade $this->value
        $this->value = $this->transform($value);
    }
    
    /*
     * metodo transform()
     * recebe um valor e faz as modificações necessárias
     * para ele ser interpretado pelo banco de dados
     * podendo ser um integer/string/boolean ou array
     * @param $value = valor a ser transformado
     * 
     */
    
    private function transform($value){
      //caso seja um array
        if(is_array($value)){
            //percorre os valores
            foreach ($value as $x){
                //se for um inteiro
                if(is_integer($x)){
                  $foo[]=$x;  
                } 
                else if (is_string($x)){
                    //se for string, adiciona aspas
                    $foo[]="'$x'";
                }
            }
            //converte o array em string separada por ","
            $result = '(' . implode(',', $foo) . ')';
        }
        else if (is_string($value))
        {
            //adciona aspas
            $result = "'$value'";
        }
        //caso  seja valor nullo
        else if (is_null($value))
        {
            //armazena null
            $result = 'NULL';
        }
        else if (is_bool($value))
        {
            //armazena TRUE ou FALSE
            $result = $value ? 'TRUE' : 'FALSE';
        }
        else
        {
            $result = $value;
        }
        //retorna o valor
        return $result;
    }
    /*
     * método dump()
     * retorna o filtro em forma de expressão
     */
    
    public function dump()
    {
        //concatena a expressão
        return "{$this->variable}{$this->operator}{$this->value}";
    }
}
