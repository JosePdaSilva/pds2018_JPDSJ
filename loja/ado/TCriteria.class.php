<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * classe TCriteria
 *Esta classe prove uma interface utilizada para definição de critérios
 * 
 */
class TCriteria extends TExpression{
    //put your code here
    
    private $expressions; //armazena a lista de expressoes
    private $operators; //armazena a lista de operadores
    private $properties; //propriedades do critério
    
    /*
     * metodo add()
     * adciona uma expressao ao critério
     * @param $expression = expressao (objeto TExpression)
     * @param $operator = operador lógico de comparação
     */
    
    public function add(TExpression $expression, $operator = self::AND_OPERATOR)
    {
        //na primeira vez, não preciamos de operador lógico para concatenar
        
        if (empty($this->expressions))
        {
            $operator = null;
        }
        
        //agrega o resultado da expressao àlista de expressoes
        $this->expressions[] = $expression;
        $this->operators[]   = $operator;
    }
    
    /*
     * método dump()
     * retorna a expressao final
     */
    
    public function dump()
    {
        //concatena a lista de expressoes
        if(is_array($this->expressions))
        {
            $result = '';
            
            foreach ($this->expressions as $i => $expression)
            {
                $operator = $this->operators[$i];
                //concatena o operador com a respectiva expressao
                $result .= $operator. $expression->dump() . '';
            }
            $result = trim($result);
            return "({$result})";
        }
    }
    
    /*
     * metodo setProperty()
     * define o valor de um propriedade
     * @param $property = propriedade
     * @param $value = valor
     */
    
    public function setProperty($property, $value)
    {
        $this->properties[$property] = $value;
    }
    
      /*
     * metodo getProperty()
     * retorna o valor de um propriedade
     * @param $property = propriedade
     * 
     */
    
    public function getProperty($property)
    {
        return $this->properties[$property];
    }
    
}