<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TSqlSelect
 *Esta classe prove meios para manipulação de uma instrução de SELECT no banco de dados
 * @author Eleandro
 */
final class TSqlSelect extends TSqlInstruction {
    //put your code here
    private $columns;
    
    /*
     * metodo addColumn
     * ad iona uma coluna a ser retornada pelo Select
     * #param $column = coluna da tabela
     */
    public function addColumn($column)
    {
        //adiciona a coluna no array
        $this->columns[] = $column;
    }
    
    /*
     * metodo getInstruction()
     * retorna a instrução de SELECT em forma de string
     * 
     */
    
    public function getInstruction() {
        //monta a instrução de SELECT em forma de string
        $this->sql= 'SELECT ';
        //monta string com os nomes de colunas
        $this->sql .= implode(',',$this->columns);
        //adiciona na cláusula from o nome da tabela
        $this->sql .= ' FROM ' . $this->entity;
        
        //obtem a clausula WHERE do objeto criteria
        if ($this->criteria)
        {
            $expression = $this->criteria->dump();
            if($expression)
            {
                $this->sql .= ' WHERE ' . $expression;
            }
            //obtem as propriedades do critério
            $order = $this->criteria->getProperty('order');
            $limit = $this->criteria->getProperty('limit');
            $offset = $this->criteria->getProperty('offset');
            
            //obtem a ordenação do SELECT
            if($order)
            {
                $this->sql .= ' ORDER BY ' . $order;
            }
            if($limit)
            {
                $this->sql .= ' LIMIT ' . $limit;
            }
            if($offset)
            {
                $this->sql .= ' OFFSET ' .$offset;
            }
        }
        return $this->sql;
    }
}
?>
