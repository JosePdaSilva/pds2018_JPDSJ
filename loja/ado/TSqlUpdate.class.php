<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Classe TSqlUpdate
 *Esta Classe prove meios para manipulação de uma instrução de UPDATE no banco de dados
 * @author Eleandro
 */
final class TSqlUpdate extends TSqlInstruction {
    //put your code here
/*
 * metodo SetRowData()
 * Atribui valores a determinadas colunas no banco de dados que serão modificadas
 * @param $column = oluna da tabela
 * @param $value = valor a ser armazenado
 * 
 */

	public function setRowData($column, $value)
	{
	    //monta um array indeado pelo nome da coluna
	    
		if(is_resource($value))
		{
			$this->columnValues[$column] = "'$value'";
			$this->bindPar[':'.$column]= PDO::PARAM_LOB;
		}
		else if (is_string($value))
		{
			//adciona \ em aspas
			$value = addslashes($value);
			//caso seja um string
			$this->columnValues[$column] = "'$value'";
			$this->bindPar[':'.$column]= PDO::PARAM_STR;
		}
		else if (is_bool($value))
		{
			//caso seja um boolean
			$this->columnValues[$column] = $value ? 'TRUE' : 'FALSE';
			$this->bindPar[':'.$column]= PDO::PARAM_BOOL;
		}
		else if (isset($value))
		{
			//caso seja outro tipo de dado
			$this->columnValues[$column] = $value;
			$this->bindPar[':'.$column]= PDO::PARAM_STR;
		}
		else
		{
			//caso seja NULL
			$this->columnValues[$column] = "NULL";
			$this->bindPar[':'.$column] = PDO::PARAM_NULL;
		}
	}
	
	public function getInstructionToPrepare()
	{
		$this->sql['sql'] = "UPDATE {$this->entity}";
		//monta uma string contendo os nomes de colunas
		if ($this->columnValues)
		{
			foreach ($this->columnValues as $column => $value)
			{
				$set[] = "{$column} = :{$column}";
			}
		}
		$this->sql['sql'] .= ' SET ' . implode(', ', $set);
		if($this->criteria)
		{
			$this->sql['sql'] .= ' WHERE ' . $this->criteria->dump();
		}
		
		$this->sql['bindPar'] = $this->bindPar;
		
		print_r($this->sql);
		
		return $this->sql;
		
	}
	
	/*
	 * metodo getInstruction()
	 * retorna a instrução de UPDATE em forma de string
	 */
	public function getInstruction() {
	    //mostra a string de UPDATE
	    $this->sql = "UPDATE {$this->entity}";
	    
	    //monta os pares: coluna=valor,...
	    if ($this->columnValues)
	    {
	        foreach ($this->columnValues as $column => $value)
	        {
	            $set[] = "{$column} = {$value}";
	        }
	    }
	    $this->sql .= ' SET ' . implode(', ', $set);
	    
	    //retorna a cláusula Where do objeto $this->criteria
	    if($this->criteria)
	    {
	        $this->sql .= ' WHERE ' . $this->criteria->dump();
	    }
	    
	    return $this->sql;
	}

}
?>