<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TExpression
 *
 * @author Eleandro
 */
abstract class TExpression {
    //put your code here
    
    //operadores lógicos
    const AND_OPERATOR = ' AND ';
    const OR_OPERATOR = ' OR ';
    
    //marca método dump como obrigatório
    abstract public function dump();
    
}
?>