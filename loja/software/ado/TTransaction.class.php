<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TTransaction
 *
 * @author Eleandro
 */
final class TTransaction {
    //put your code here
    
    private static $conn; //conexao ativa
    private static $logger;//objetvo LOG
    
    /*
     * metodo __construct()
     * esta declarado como private para impedir que se crie instancias de TTransaction
     */
    
    private function __construct() {
        
    }
    
    public static function open($database)
    {
        //abre uma conexaoe marmazena na propriedade estática $conn
        if (empty(self::$conn))
        {
            self::$conn = TConnection::open($database);
            //inicia a transação
            self::$conn->beginTransaction();
            
            //desliga o log de SQL
            self::$logger = NULL;
        }
    }
    
    /*
     * metodo get()
     * retorna a conexao ativa da transação
     */
    
    public static function get()
    {
        //retorna a conexao ativa
        return self::$conn;
    }
    
    /*
     * metodo rollback()
     * desfaz todas operacoes realizadas na transação
     */
    
    public static function rollback()
    {
        if(self::$conn)
        {
            //desfaz as operações realizadas durante a transação
            self::$conn->rollback();
            self::$conn = NULL;
        }
    }
    
    /*
     * metodo close()
     * aplica todas operações realizadas e fecha a transação
     */
    
    public static function close()
    {
        if (self::$conn)
        {
            //aplica as operações realizadas
            //durante a transação
            self::$conn->commit();
            self::$conn = NULL;
        }
    }
    
    /*
     * método setLogger()
     * define qual estratégia (algoritmo de LOG será usado)
     */
    public static function setLogger(TLogger $logger)
    {
        self::$logger = $logger;
    }
    
    /*
     * metodo log()
     * armazena uma mensagem no arquivo de LOG
     * baseada na estratégia ($logger)atual
     */
    
    public static function log($message)
    {
        //verifica existe um logger
        if (self::$logger)
        {
            self::$logger->write($message);
        }
    }
    
}
?>