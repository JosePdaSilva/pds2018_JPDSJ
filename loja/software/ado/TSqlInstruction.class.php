<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * classe TSqlInstruction
 *Esta classe provê os metodos em comum entre todas instruções
 * SQL (SELECT, INSERT, DELETE, UPDATE
 */
abstract class TSqlInstruction {
    //put your code here
   
    protected $sql; //armazena a instrução SQL
    protected $criteria; //armazena o objeto critério
    
    /*
     * metodo setEntity()
     * define o nome da entidade (tabela) manipulada pela instrução sql
     * @param $entity = tabela
     */
    
    final public function setEntity($entity)
    {
        $this->entity = $entity;
    }
    
    /*
     * método getEntity()
     * retorno o nome da entidade (tabela)
     */
    
    final public function getEntity()
    {
        return $this->entity;
    }
    
    /*
     * método setCriteria()
     * Define um critério de seleção dos dados através da composição de um objeto
     * do tipo TCriteria, que oferece uma interface para definição de critérios
     * @param $criteria = objeto do tip TCriteria
     */
    
    public function setCriteria(TCriteria $criteria)
    {
        $this->criteria = $criteria;
    }
    /*
     * metodo getInstruction()
     * declarando-o como <abstract> obrigamos sua declaração nas classes filhas,
     * uma vez que seu comportamento será distinto em cada uma delas, configurando polimorfismo
     */
    
    abstract function getInstruction();
}
