<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * classe TSqlInsert
 *Esta classe prove meios para manipulação de uma instrução de INSERT no banco de dados
 * @author Eleandro
 */
final class TSqlInsert extends TSqlInstruction {
    //put your code here
    
    /*
     * metodo setRowData()
     * Atribui valores a determinadas colunas no banco de dados que serao inseridas
     * @param $column = coluna da tabela
     * @param $value = valor a ser armazenado
     */
    public function setRowData($column, $value)
    {
        //monta um array indexado pelo nome da coluna
        if(is_resource($value))
        {
        	$this->columnValues[$column] = "'$value'";
        	$this->bindPar[':'.$column]= PDO::PARAM_LOB;
        }
        else if (is_string($value))
        {
            //adciona \ em aspas
            $value = addslashes($value);
            //caso seja um string
            $this->columnValues[$column] = "'$value'";
            $this->bindPar[':'.$column]= PDO::PARAM_STR;
        } 
        else if (is_bool($value))
        {
            //caso seja um boolean
            $this->columnValues[$column] = $value ? 'TRUE' : 'FALSE';
            $this->bindPar[':'.$column]= PDO::PARAM_BOOL;
        }
        else if (isset($value))
        {
            //caso seja outro tipo de dado
            $this->columnValues[$column] = $value;
            $this->bindPar[':'.$column]= PDO::PARAM_STR;
        }
        else
        {
            //caso seja NULL
            $this->columnValues[$column] = "NULL";
            $this->bindPar[':'.$column] = PDO::PARAM_NULL;
        }
        
        
    }
    
    /*
     * método setCriteria()
     * não existe no contexto desta classe, logo, irá lançar um erro ser for executado
     */
    
    public function setCriteria(TCriteria $criteria) {
        //lança o erro
        throw new Exception("Cannot call setCriteria from " . __CLASS__);
    }
    
    /*
     * método getInstruction()
     * retorna a instrução de INSERT em forma de string
     */
    
    public function getInstructionToPrepare()
    {
    	$this->sql['sql'] = "INSERT INTO {$this->entity} (";
    	//monta uma string contendo os nomes de colunas
    	$columns = implode(', ', array_keys($this->columnValues));
    	//monta uma string contendo os valores
    	$values = implode(', ', array_keys($this->bindPar));
    	$this->sql['sql'] .= $columns . ')';
    	$this->sql['sql'] .= " values ({$values})";
    	
    	$this->sql['bindPar'] = $this->bindPar;
    	
    	return $this->sql;
    	
    }
    
    public function getInstruction() {
    
        $this->sql = "INSERT INTO {$this->entity} (";
        //monta uma string contendo os nomes de colunas
        $columns = implode(', ', array_keys($this->columnValues));
        //monta uma string contendo os valores
        $values = implode(', ', array_values($this->columnValues));
        $this->sql .= $columns . ')';
        $this->sql .= " values ({$values})";
        //$_SESSION['message'] = $this->sql;
        return $this->sql;
    
    }
}
