<?php

/** caminho absoluto para a pasta do sistema **/
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** caminho no server para o sistema **/
if($_SERVER['HTTP_HOST']=='localhost')
{
	if ( !defined('BASEURL') )
		define('BASEURL', '/loja/software/');
}
elseif($_SERVER['HTTP_HOST']=='obrix.com.br')
{
	if ( !defined('BASEURL') )
		define('BASEURL', '/software/');
}
else
{
	echo 'Configure BASEURL para: '. $_SERVER['HTTP_HOST'];
	exit();
	define('BASEURL', 'BASEURL n�o est� definido - o sistema n�o ir� funcionar');
}

	
/** caminho do arquivo de banco de dados **/
if ( !defined('DBAPI') )
		define('DBAPI', ABSPATH . 'includes/database.php');

		
		
if(!defined('IMAGE_FOLDER'))
	define('IMAGE_FOLDER',  '../imagens/');
		
if ( !defined('CONFIG_DB') )
	define('CONFIG_DB', ABSPATH . 'config/');

//if ( !defined('FILE_FOLDER') )
//	define('FILE_FOLDER', ABSPATH . 'files/');
	

/** caminhos dos templates de header e footer **/
define('HEADER', ABSPATH. 'includes/header.php');
define('FOOTER', ABSPATH. 'includes/footer.php');
define('DELETEMODAL', ABSPATH. 'includes/delete-modal.html');
//define('AVALIAMODAL', ABSPATH. 'includes/avaliacao-modal.php');

require_once DBAPI;	

session_start();

function verify($verificarPagina = "")
{
    if (isset ( $_SESSION ['username'] ))
    {
        $find = find_id('VIEW_USUARIOS_LOGIN', 'ID_USUARIO', $_SESSION['id_usuario']);
        if(count($find)>0)
        {
            $fields = $find[0];
            $_SESSION["id_usuario"] = $fields['ID_USUARIO'];
            $_SESSION["username"] = $fields['USERNAME'];
            $_SESSION["id_perfil"] = $fields['ID_PERFIL'];
            $_SESSION["id_empresa"] = $fields['ID_EMPRESA'];
            
            if($fields['ID_CONSTRUTOR']!="")
            {
                $_SESSION["id_construtor"] = $fields['ID_CONSTRUTOR'];
                if(isset($_SESSION["id_fornecedor"]))
                    unset($_SESSION["id_fornecedor"]);
            }
            if($fields['ID_FORNECEDOR']!="")
            {
                $_SESSION["id_fornecedor"] = $fields['ID_FORNECEDOR'];
                if(isset($_SESSION['id_construtor']))
                    unset($_SESSION["id_construtor"]);
            }
            
            if($fields['DT_CHANGED_PASS']>$_COOKIE["data_login"])
            {
                header('Location: logoff.php');
                exit();
            }
            else
            {
                if($fields['APROVACAO']!='S' && ($fields['ID_CONSTRUTOR']!="" || $fields['ID_FORNECEDOR']!=""))
                {
                    header('Location: logoff.php');
                    exit();
                }
                
                if($fields['TEMP_PASSWORD']=='S')
                {
                    header('Location: '.BASEURL.'redefinir-senha.php');
                    exit();
                }
                else
                {
                    if($verificarPagina!="" && !find_acessos($_SESSION['id_perfil'], $verificarPagina))
                    {
                        header('Location:'.BASEURL.'home.php');
                        exit();
                    }
                }
            }
            
            
        }
        else
        {
            header('Location: '.BASEURL.'index.php');
            exit();
        }
        
    }
    else
    {
        header('Location: '.BASEURL.'index.php');
        exit();
    }
}

?>