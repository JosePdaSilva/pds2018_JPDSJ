<?php
session_start();
if(isset($_SESSION['username']))
{
	header('Location: home.php');
	exit();
}
if(isset($_COOKIE["id_usuario"]))
{
	if($_COOKIE["id_usuario"]!="deleted")
	{
		header('Location:login.php');
		exit();
	}
}
$erro = isset($_GET['erro']) ? $_GET['erro'] : 0 ;


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Triforce</title>
  <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark background_login">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
         <form role="form" action="login.php" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Nome de Usuário</label>
            <input class="form-control" id="exampleInputEmail1" name="usuario[USERNAME]" type="text" aria-describedby="emailHelp" placeholder="Digite seu Nome de Usuário">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Senha</label>
            <input class="form-control" id="exampleInputPassword1" name="usuario[PASSWORD]" type="password" placeholder="Digite sua senha">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" name="remember" type="checkbox">Lembrar sua senha</label>
            </div>
          </div>
          <div class="text-center">
          <button type="submit" class="btn btn-outline-primary">Login</button>
          <a class="btn btn-outline-secondary" href="../">Voltar</a>
          </div>
          				<?php
                            if ($erro == 1){
                                echo '<br><font color="red"> Usuário e ou senha inválido(s)!</font>';
                            }
                            if ($erro == 2){
                                echo '<br><font color="red"> Usuário ainda não validado!<br> Aguarde sua aprovação.</font>';
                            }
                            if ($erro == 3){
                                echo '<br><font color="red"> Usuário não cadastrado!</font>';
                            }
                        ?> 
        </form>
        <div style="margin-top:15px">
          <a href="forgot-password.php">Esqueceu sua senha?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
