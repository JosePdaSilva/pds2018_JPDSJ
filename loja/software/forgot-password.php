<?php
require_once('config.php');

if(isset($_SESSION['username']))
{
	header('Location: home.php');
}
require_once 'sendEmailsenha.php';
if(isset($_POST['email_user']))
{
	$e_user = $_POST['email_user'];

	if (filter_var($e_user, FILTER_VALIDATE_EMAIL)) {
		try {
			$findAccount = find('TBL_USUARIOS', 'EMAIL', $e_user);

			if (count($findAccount) > 0) {
				$fields = $findAccount[0];
				$username = $fields['USERNAME'];

				$bytes = openssl_random_pseudo_bytes(4);
				$pwdGenerator = bin2hex($bytes);
				
				$today = new DateTime();
				$update = update('TBL_USUARIOS', array('PASSWORD' => md5($pwdGenerator), 'TEMP_PASSWORD'=>'S', 'DT_CHANGED_PASS'=>$today->format('d.m.Y H:i:s')), 'ID_USUARIO', $fields['ID_USUARIO']);

				if((new sendemail(['EMAIL'=>$e_user, 'USERNAME'=>$username, 'PASSWORD'=>$pwdGenerator]))==true)
				{
					$em = explode("@",$e_user);
					$name = $em[0];
					$len = strlen($name);
					$showLen = floor($len/2);
					$str_arr = str_split($name);
					for($ii=$showLen;$ii<$len;$ii++){
						$str_arr[$ii] = '*';
					}
					$em[0] = implode('',$str_arr);
					$hide_e_user = implode('@',$em);
					
					$_SESSION['hide_e_user']=$hide_e_user;
				}
			}
			else {
				$_SESSION['type'] = 'danger';
				$_SESSION['message'] = 'Este email não foi cadastrado em nossa base de dados';
			}
		
		} catch (Exception $e) {
		    $_SESSION['type'] = 'danger';
		    $_SESSION['message'] = $e->getMessage();
		}
	  
	} else {
	    $_SESSION['type'] = 'danger';
	    $_SESSION['message'] = 'E-mail invalido!';
	}
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Obrix</title>
  <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark background_login">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Resete sua senha</div>
      <div class="card-body">
      	<?php 
      	 if(!isset($_SESSION['hide_e_user']))
      	 {
      	?>
      	<?php 
      	if(isset($_SESSION['message']))
      	{
      	?>
      		<p style="color:red">Erro: <?php echo $_SESSION['message']?></p>
      	<?php 
      	 unset($_SESSION['type']);
      	 unset($_SESSION['message']);
      	}
      	?>
        <div class="text-center mt-4 mb-5">
          <h4>Esqueceu sua senha?</h4>
          <p>Digite seu e-mail e nós enviaremos a instrução para mudar sua senha por e-mail.</p>
        </div>
        <form action="forgot-password.php" method="post">
          <div class="form-group">
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Seu e-mail *" name="email_user">
          </div>
          <button type="submit" class="btn btn-warning btn-block">Resetar senha</button>
        </form>
        <?php 
      	 }
      	 elseif(isset($_SESSION['hide_e_user']))
      	 {
        ?>
        	<p style="color:green;text-align:center">Nova senha enviada para: <?php echo $_SESSION['hide_e_user']?></p>
        <?php 
            unset($_SESSION['hide_e_user']);
      	 }
        ?>
        <div class="text-center" style="margin-top:15px">
          <a href="index.php">Página de login</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
