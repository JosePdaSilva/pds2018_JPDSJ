$(document).ready(function(){
	$('.table').DataTable({
		responsive: true,
		"language": {
			"lengthMenu": "Mostrando _MENU_ registros por página",
			"zeroRecords": "Nenhum registro disponível",
			"info": "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty": "Mostrando página 0 de 0",
			"infoFiltered": "(filtrado de _MAX_ registros)",
			"search": "Buscar:",
			"paginate": {
				"first": "Primeira",
				"previous": "Anterior",
				"next": "Próximo",
				"last": "Última"
			}
		}
	});
	
	
	$('#deleteModal').on('show.bs.modal', function (event)
	{
		var button = $(event.relatedTarget);
		var modal = $(this);
		var id = button.data('id');
		var tipo = button.data('tipo');
		if(tipo=="PERFIL")
		{
			var perfil = button.data('perfil');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+perfil+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O PERFIL '+perfil+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="USUÁRIO")
		{
			var usuario = button.data('usuario');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+usuario+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O USUÁRIO - '+usuario+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="SETOR")
		{
			var setor = button.data('setor');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+setor+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O SETOR: '+setor+'</p></strong><br><p><strong>OBS: Ao excluir um setor você estará excluindo seus respectivos produtos e especificações ! </strong></P>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="PRODUTO")
		{
			var produto = button.data('produto');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+produto+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O PRODUTO: '+produto+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="UM")
		{
			var um = button.data('um');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+um+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A UNIDADE DE MEDIDA: '+um+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="ESPECIFICAÇÃO")
		{
			var especificacao = button.data('especificacao');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+especificacao+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A ESPECIFICAÇÃO: '+especificacao+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="VINCULAR")
		{
			var vincular = button.data('vincular');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+vincular+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A VINCULAÇÃO: '+vincular+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="OPERADORA")
		{
			var operadora = button.data('operadora');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+operadora+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A OPERADORA/CONVÊNIO '+operadora+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="OBRA")
		{
			var obra = button.data('obra');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+obra+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A ROUPA: '+obra+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		
		else if(tipo=="PROCEDIMENTO")
		{
			var procedimento = button.data('procedimento');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+procedimento+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O PROCEDIMENTO '+procedimento+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		
		else if(tipo=="FATURA")
		{
			modal.find('.modal-title').html('<strong>Excluir #'+id+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR A FATURA '+id+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="ITEM FATURA")
		{
			var fatura = button.data('fatura');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - Fatura:"+fatura+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O ITEM '+id+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete-item.php?id-item='+id+"&id-fat="+fatura);
		}
		else if(tipo=="CLÍNICAXOPERADORA")
		{
			var clinica = button.data('clinica');
			var operadora = button.data('operadora');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+clinica+"x"+operadora+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O VÍNCULO '+clinica+"x"+operadora+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		
		else if(tipo=="EXTRATO")
		{
			modal.find('.modal-title').html('<strong>Excluir #'+id+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O EXTRATO'+id+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="RECEBIMENTO")
		{
			var clinica = button.data('clinica');
			var operadora = button.data('operadora');
			var gto = button.data('gto');
			var extrato = button.data('extrato');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - GTO: "+gto+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O RECEBIMENTO<br/>GTO: '+gto+"<br/>CLÍNICA: "+clinica+"<br/>OPERADORA: "+operadora+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete-recebimento.php?id-recebimento='+id+'&id-ext='+extrato);
		}
		else if(tipo=="RECURSO")
		{
			modal.find('.modal-title').html('<strong>Excluir #'+id+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O RECURSO '+id+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="ITEM RECURSO")
		{
			var fatura = button.data('fatura');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - Recurso:"+fatura+"</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O ITEM '+id+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete-item.php?id-item='+id+"&id-fat="+fatura);
		}

	});
	
	$('#finaliza-cancelaModal').on('show.bs.modal', function (event)
	{
		var button = $(event.relatedTarget);
		var modal = $(this);
		var id = button.data('id');
		var tipo = button.data('tipo');
		if(tipo=="FATURA")
		{
			modal.find('.modal-title').html('<strong>Finalizar/Cancelar a Fatura #'+id+" </strong>");
			modal.find('.modal-body').html('...carregando...');
			$.ajax({url: "carregar-info-fatura.php", type: "GET", data: "id="+id, success: function(result){
				modal.find('.modal-body').html(result);
				if(document.getElementById('funcao').innerHTML=="FINALIZAR")
					modal.find('#btnSim').attr('href', 'finalizar-fatura.php?id='+id);
				else
					modal.find('#btnSim').attr('href', 'cancelar-fatura.php?id='+id);
			}});
		}
		else if(tipo=="EXTRATO")
		{
			modal.find('.modal-title').html('<strong>Finalizar/Cancelar a Fatura #'+id+" </strong>");
			modal.find('.modal-body').html('...carregando...');
			$.ajax({url: "carregar-info-extrato.php", type: "GET", data: "id="+id, success: function(result){
				modal.find('.modal-body').html(result);
				if(document.getElementById('funcao').innerHTML=="FINALIZAR")
					modal.find('#btnSim').attr('href', 'finalizar-extrato.php?id='+id);
				else
					modal.find('#btnSim').attr('href', 'cancelar-extrato.php?id='+id);
			}});
		}
		else if(tipo=="RECURSO")
		{
			modal.find('.modal-title').html('<strong>Finalizar/Cancelar o Recurso #'+id+" </strong>");
			modal.find('.modal-body').html('...carregando...');
			$.ajax({url: "carregar-info-recurso.php", type: "GET", data: "id="+id, success: function(result){
				modal.find('.modal-body').html(result);
				if(document.getElementById('funcao').innerHTML=="FINALIZAR")
					modal.find('#btnSim').attr('href', 'finalizar-recurso.php?id='+id);
				else
					modal.find('#btnSim').attr('href', 'cancelar-recurso.php?id='+id);
			}});
		}
	});
});

