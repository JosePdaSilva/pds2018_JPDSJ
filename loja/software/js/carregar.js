/*PARA CARREGAR OS CAMPOS DOS CONTATOS OBRIGATÓRIOS*/

function carregarContatos(){
	for(var i=0;i<document.getElementById("qtde_contatos").value;i++)
	{
		var divLinha = document.createElement("DIV");
		divLinha.setAttribute("class", "form-group");
		divLinha.id="contato"+i;

			//-----SELECT OPCAO------//
			var divOpcao = document.createElement("DIV");
			divOpcao.setAttribute("class", "col-md-3 col-sm-3 col-xs-6");
			
			var nameop="opcao"+i;
			var opcao = document.createElement("SELECT");
			opcao.name="contato['"+nameop+"']";
			opcao.id=nameop;
			opcao.setAttribute("onchange", "updatedContato(this,'valor"+i+"');");
			opcao.setAttribute("class", "form-control");
			

				//-----OPTIONS OPCAO------//
				var optopcao1 = document.createElement("OPTION");
				var optopcao2 = document.createElement("OPTION");
				var optopcao3 = document.createElement("OPTION");
				optopcao1.text= "Telefone";
				optopcao1.value= "T";
				optopcao2.text= "Celular";
				optopcao2.value= "C";
				optopcao3.text= "Email";
				optopcao3.value= "E";
				//-----OPTIONS OPCAO------//
			
			
			opcao.appendChild(optopcao1);
			opcao.appendChild(optopcao2);
			opcao.appendChild(optopcao3);
			divOpcao.appendChild(document.createTextNode("Opção"));
			divOpcao.appendChild(opcao);
			//-----SELECT OPCAO------//
			
			
			//-----SELECT TIPO------//
			var divTipo = document.createElement("DIV");
			divTipo.setAttribute("class", "col-md-3 col-sm-3 col-xs-6");
			var nametp="tipo"+i;
			var tipo = document.createElement("SELECT");
			tipo.name="contato['"+nametp+"']";
			tipo.id=nametp;
			tipo.setAttribute("class", "form-control");

				//-----OPTIONS TIPO------//
				var opttipo1 = document.createElement("OPTION");
				var opttipo2 = document.createElement("OPTION");
				var opttipo3 = document.createElement("OPTION");
				opttipo1.text= "Residencial";
				opttipo1.value= "R";
				opttipo2.text= "Comercial";
				opttipo2.value= "C";
				opttipo3.text= "Recado";
				opttipo3.value= "A";
				//-----OPTIONS TIPO------//
				
			tipo.appendChild(opttipo1);
			tipo.appendChild(opttipo2);
			tipo.appendChild(opttipo3);
			divTipo.appendChild(document.createTextNode("Tipo"));
			divTipo.appendChild(tipo);
			//-----SELECT TIPO------//
			
			//-----INPUT VALOR------//
			var divValor = document.createElement("DIV");
			divValor.setAttribute("class", "col-md-3 col-sm-3 col-xs-6");
			
			var nameinpt="valor"+i;
			var valor = document.createElement("INPUT");
			valor.name="contato['"+nameinpt+"']";
			valor.id=nameinpt;
			valor.type="text";
			valor.placeholder="00-0000-0000";
			valor.setAttribute("OnKeyPress", "formatar('##-####-####', this)");
			valor.setAttribute("maxlength", "12");
			valor.setAttribute("class", "form-control");
			valor.required="required";
			
			divValor.appendChild(document.createTextNode("Valor"));
			divValor.appendChild(valor);
			//-----INPUT VALOR------//
		
		
		divLinha.appendChild(divOpcao);
		divLinha.appendChild(divTipo);
		divLinha.appendChild(divValor);
		document.getElementById("contatos").appendChild(divLinha);
	}
}

function carregarEnderecos()
{
	for(var j=0;j<document.getElementById("qtde_enderecos").value;j++)
	{
		
		addEndereco(false);
		
		/*
		var divEnd = document.createElement("DIV");
		divEnd.id="endereco"+atual_end;
		
		var divTipoEnd = document.createElement("DIV");
		divTipoEnd.setAttribute("class", "form-group");
		
		
		//--LINHA 1--//
		var divLinha1 = document.createElement("DIV");
		divLinha1.setAttribute("class", "form-group");
		

		var divLinhaRow1 = document.createElement("DIV");
		divLinhaRow1.setAttribute("class", "form-row");
			
			//--CEP--//
			var divCEP = document.createElement("DIV");
			divCEP.setAttribute("class", "col-md-2 col-sm-4 col-xs-6 ");
			
			var txtcep = document.createElement("INPUT");
			txtcep.name="endereco["+atual_end+"][CEP]";
			txtcep.id="cep"+atual_end;
			txtcep.type="text";
			txtcep.placeholder="somente numeros";
			txtcep.setAttribute("OnKeyPress", "formatar('#####-###', this)");
			txtcep.setAttribute("oninput","buscarCEP("+atual_end+")");
			txtcep.setAttribute("maxlength", "9");
			txtcep.setAttribute("class", "form-control");
			txtcep.required="required";
			
			divCEP.appendChild(document.createTextNode("CEP"));
			divCEP.appendChild(txtcep);
			//--CEP--//
			
			//--RUA--//
			var divRua = document.createElement("DIV");
			divRua.setAttribute("class", "col-md-8 col-sm-4 col-xs-6 ");
			
			var txtrua = document.createElement("INPUT");
			txtrua.name="endereco["+atual_end+"][LOGRADOURO]";
			txtrua.id="logradouro"+atual_end;
			txtrua.type="text";
			txtrua.placeholder="Logradouro";
			txtrua.setAttribute("class", "form-control");
			txtrua.required="required";
			
			divRua.appendChild(document.createTextNode("Logradouro"));
			divRua.appendChild(txtrua);
			//--RUA--//
			
			//--NUMERO--//
			var divN = document.createElement("DIV");
			divN.setAttribute("class", "col-md-2 col-sm-4 col-xs-6 ");
			
			var txtn = document.createElement("INPUT");
			txtn.name="endereco["+atual_end+"][NUMERO]";
			txtn.id="numero"+atual_end;//
			txtn.type="text";
			txtn.placeholder="Número";
			txtn.setAttribute("class", "form-control");
			
			divN.appendChild(document.createTextNode("Número"));
			divN.appendChild(txtn);
			//--NUMERO--//
		
		divLinhaRow1.appendChild(divCEP);
		divLinhaRow1.appendChild(divRua);
		divLinhaRow1.appendChild(divN);
		
		divLinha1.appendChild(divLinhaRow1);
		
		//--LINHA 1--//
		
		//--LINHA 2--//
		var divLinha2 = document.createElement("DIV");
		divLinha2.setAttribute("class", "form-group");
		
		var divLinhaRow2 = document.createElement("DIV");
		divLinhaRow2.setAttribute("class", "form-row");
			
			//--COMPLEMENTO--//
			var divCompl = document.createElement("DIV");
			divCompl.setAttribute("class", "col-md-3 col-sm-3 col-xs-6 ");
			
			var txtcompl = document.createElement("INPUT");
			txtcompl.name="endereco["+atual_end+"][COMPLEMENTO]";
			txtcompl.id="complemento"+atual_end;
			txtcompl.type="text";
			txtcompl.placeholder="Complemento";
			txtcompl.setAttribute("class", "form-control");
			
			divCompl.appendChild(document.createTextNode("Complemento"));
			divCompl.appendChild(txtcompl);
			//--COMPLEMENTO--//
			
			//--BAIRRO--//
			var divBair = document.createElement("DIV");
			divBair.setAttribute("class", "col-md-3 col-sm-3 col-xs-6 ");
			
			var txtbair = document.createElement("INPUT");
			txtbair.name="endereco["+atual_end+"][BAIRRO]";
			txtbair.id="bairro"+atual_end;
			txtbair.type="text";
			txtbair.placeholder="Bairro";
			txtbair.setAttribute("class", "form-control");
			txtbair.required="required";
			
			divBair.appendChild(document.createTextNode("Bairro"));
			divBair.appendChild(txtbair);
			//--BAIRRO--//
			
			//--CIDADE--//
			var divCid = document.createElement("DIV");
			divCid.setAttribute("class", "col-md-3 col-sm-3 col-xs-6 ");
			
			var txtcid = document.createElement("INPUT");
			txtcid.name="endereco["+atual_end+"][MUNICIPIO]";
			txtcid.id="cidade"+atual_end;
			txtcid.type="text";
			txtcid.placeholder="Cidade";
			txtcid.setAttribute("class", "form-control");
			txtcid.required="required";
			
			divCid.appendChild(document.createTextNode("Cidade"));
			divCid.appendChild(txtcid);
			//--CIDADE--//
			
			//--ESTADO--//
			var divUF = document.createElement("DIV");
			divUF.setAttribute("class", "col-md-2 col-sm-3 col-xs-6 ");
			
			var txtuf = document.createElement("INPUT");
			txtuf.name="endereco["+atual_end+"][ESTADO]";
			txtuf.id="estado"+atual_end;
			txtuf.type="text";
			txtuf.placeholder="Estado";
			txtuf.setAttribute("class", "form-control");
			txtuf.required="required";
			
			divUF.appendChild(document.createTextNode("Estado"));
			divUF.appendChild(txtuf);
			//--ESTADO--//
			
			
			
			
			
			divLinhaRow2.appendChild(divCompl);
			divLinhaRow2.appendChild(divBair);
			divLinhaRow2.appendChild(divCid);
			divLinhaRow2.appendChild(divUF);
			
			divLinha2.appendChild(divLinhaRow2);
			
			divEnd.appendChild(divTipoEnd);
			divEnd.appendChild(divLinha1);
			divEnd.appendChild(divLinha2);
			
			document.getElementById("enderecos").appendChild(divEnd);
	    //var text = "<input type=id='contato['" +atual_end+ "']' value=' "+atual_end+ "'>";
	    /*var text = 	"<div class='form-group'>"
						+"<div class='col-md-2 col-sm-3 col-xs-6' >"
							+"Selecione a Opção"
							+"<select class='form-control' id='opcao' name="+"contato['"+name+"']"+" onchange='updatedContato("+atual_end+")'>"
								+"<option value='T'>Telefone</option>"
								+"<option value='C'>Celular</option>"
								+"<option value='E'>Email</option>"
							+"</select>"
						+"</div>"
						+"<div class='col-md-2 col-sm-3 col-xs-6' >"
							+"Selecione o Tipo"
							+"<select class='form-control' id='tipo' name='contato['tipo']'>"
								+"<option value='R'>Residencial</option>"
								+"<option value='C'>Comercial</option>"
								+"<option value='R'>Recado</option>"
							+"</select>"
						+"</div>"
						+"<div class='col-md-2 col-sm-3 col-xs-6 '>"
							+"Valor"
							+"<input type='text' id='valor' name='contato['valor']' required='required' OnKeyPress='formatar('##-#####-####', this)' maxlength='13' placeholder='Telefone' class='form-control col-md-6 col-xs-12'>"
						+"</div>"
					+"</div>"
	    
		//document.getElementById("qtde_enderecos").value++;
		atual_end++;
		*/
	}
}