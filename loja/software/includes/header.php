<?php 

if($_SESSION['id_perfil']!=1)
{
    header('location: ../index.php');
}

if (!isset ( $_SESSION ['username'] ))
{
	header('Location: '.BASEURL.'index.php');
	exit();
}

$conn = TConnection::open('myconection');
$sql = new TSqlSelect();
$sql->setEntity('tbl_paginas');
$sql->addColumn(' * ');

$menu_paginas = array();
$menu_paginas = $conn->query($sql->getInstruction());
foreach ($menu_paginas as $menu_pagina)
{
	$result = array();
	
	$sql = new TSqlSelect();
	$sql->setEntity('view_acessos');
	$sql->addColumn(' * ');
	
	$criteria = new TCriteria;
	$criteria->add(new TFilter('ID_PERFIL', ' = ', $_SESSION['id_perfil']));
	$criteria->add(new TFilter('ID_PAGINA', ' = ', $menu_pagina['ID_PAGINA']));
	
	$sql->setCriteria($criteria);
	
	$result = $conn->query($sql->getInstruction());
	
	$row = $result->fetch(PDO::FETCH_ASSOC);
	$menu_acessos[$menu_pagina['URL']] = (!empty($row))?true:false;
}


$res = find_id('tbl_paginas', 'URL', str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));
if(count($res)>0)
{
	$pagina = $res[0];
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Triforce - <?php echo $pagina['TITULO'];?></title>
  <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASEURL?>../img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASEURL?>../img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASEURL?>../img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASEURL?>../img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASEURL?>../img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASEURL?>../img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASEURL?>../img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASEURL?>../img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASEURL?>../img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo BASEURL?>../img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo BASEURL?>../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo BASEURL?>../img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASEURL?>../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo BASEURL?>../img/favicon/manifest.json">
    <script src="<?php echo BASEURL?>js/enderecos.js"></script>
    <script src="<?php echo BASEURL?>js/carregar.js"></script>
    <script src="<?php echo BASEURL?>js/contatos.js"></script>
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!-- Bootstrap core CSS-->
  <link href="<?php echo BASEURL?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo BASEURL?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="<?php echo BASEURL?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- DataTables CSS -->
<link href="<?php echo BASEURL; ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DataTables Responsive CSS -->
    <link href="<?php echo BASEURL; ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo BASEURL?>css/sb-admin.css" rel="stylesheet">
</head>

<body class="sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
    <a class="navbar-brand" href="<?php echo BASEURL?>home.php"><img src="<?php echo BASEURL?>img/triforce_logo.png" style="height: 40px;"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
      
      			<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Perfis">
                  <a class="nav-link" href="<?php echo BASEURL?>home.php">
                    <i class="fa fa-tachometer fa-fw"></i>
                    <span class="nav-link-text">Página Inicial</span>
                  </a>
                </li>
      
        		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Perfis">
                  <a class="nav-link" href="<?php echo BASEURL?>pages/perfis/index.php">
                    <i class="fa fa-address-book-o fa-fw"></i>
                    <span class="nav-link-text">Perfis</span>
                  </a>
                </li>
      
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Usuários">
          <a class="nav-link" href="<?php echo BASEURL?>pages/usuarios/index.php">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">Usuários</span>
          </a>
        </li>
        
         
     
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Obras">
          <a class="nav-link" href="<?php echo BASEURL?>pages/roupas/index.php">
            <i class="fa fa-fw fa-tags"></i>
            <span class="nav-link-text">Roupas</span>
          </a>
        </li>
        
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Obras">
          <a class="nav-link" href="<?php echo BASEURL?>pages/pedidos/index.php">
            <i class="fa fa-fw fa-shopping-cart"></i>
            <span class="nav-link-text">Pedidos</span>
          </a>
        </li>
       
            
        <li class="navbar-nav nav-item" data-toggle="tooltip" data-placement="right" title="Ocultar Menu">
          <a class="nav-link" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
            <span class="nav-link-text">Ocultar menu</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
       <!--  <li class="nav-item">
          <!--  <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Pesquisar...">
              <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
          <?php 
          if(isset($_SESSION ['id_construtor'])){
          ?>
              <a class="btn btn-info" style="margin-right: 10px" href="<?php echo BASEURL?>pages/convidar-parceiros/index.php">Convide parceiros</a>
			<?php 
          }
			?>		          
        </li> -->
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
    
    <!-- 
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Início</a>
        </li>
        <li class="breadcrumb-item active">Minha empresa</li>
      </ol>
      -->
      