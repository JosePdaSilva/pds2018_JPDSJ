</div> <!-- /.container-fluid-->
  </div><!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Obrix</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deseja sair do Obrix?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Clique em Logout para sair da sua conta.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="<?php echo BASEURL?>logoff.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo BASEURL?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo BASEURL?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo BASEURL?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="<?php echo BASEURL?>vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo BASEURL?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo BASEURL?>vendor/datatables/dataTables.bootstrap4.js"></script>
    
    
    <script src="<?php echo BASEURL?>js/buscaCEP.js"></script>
    <script src="<?php echo BASEURL?>js/odonto.js"></script>
    <script src="<?php echo BASEURL?>js/contatos.js"></script>
    
    
    <script src="<?php echo BASEURL?>js/sb-admin.min.js"></script>
    <script src="<?php echo BASEURL?>js/sb-admin-datatables.min.js"></script>
    <script src="<?php echo BASEURL?>js/sb-admin-charts.js"></script>
    
    <?php 
        if(isset($_SESSION['type']))
        {
    ?>
            <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" <?php if($_SESSION['type']=="success") echo "style='background-color:  #28a745';"; else echo "style='background-color:#dc3545';";?>>
                    <h5 class="modal-title"><?php if($_SESSION['type']=="success") echo "Sucesso!"; else echo "Erro!";?></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body"><?php echo $_SESSION['message']; ?></div>
                  <div class="modal-footer">
                    <button class="btn btn-<?php echo $_SESSION['type']; ?>" type="button" data-dismiss="modal">OK</button>
                  </div>
                </div>
              </div>
            </div>
            
            <script type="text/javascript">
        
            var alertModal = $('#alertModal');
        	alertModal.modal("show");
        	
            </script>
    
    <?php
            unset($_SESSION['type']);
            unset($_SESSION['message']);
        }
    ?>
</body>

</html>
