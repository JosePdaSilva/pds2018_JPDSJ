<?php
require_once(ABSPATH.'ado/TConnection.class.php');
require_once(ABSPATH.'ado/TTransaction.class.php');
require_once(ABSPATH.'ado/TSqlInstruction.class.php');
require_once(ABSPATH.'ado/TExpression.class.php');
require_once(ABSPATH.'ado/TFilter.class.php');
require_once(ABSPATH.'ado/TCriteria.class.php');
require_once(ABSPATH.'ado/TSqlSelect.class.php');
require_once(ABSPATH.'ado/TSqlInsert.class.php');
require_once(ABSPATH.'ado/TSqlUpdate.class.php');
require_once(ABSPATH.'ado/TSqlDelete.class.php');

/**
 *  Pesquisa um Registro pelo ID em uma Tabela
 */
function find( $table = null, $property = null, $id = null ) {
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    $found = null;
    try {
        if ($id) {
            $sql = new TSqlSelect;
            $sql->setEntity($table);
            $sql->addColumn(' * ');
            
            $criteria = new TCriteria;
            if(is_array($id))
            {
                $propriedade = array();
                foreach ($property as $prop)
                {
                    $propriedade[] = str_replace("'", "", $prop);
                }
                $i = 0;
                foreach ($id as $value)
                {
                    $criteria->add(new TFilter($propriedade[$i], ' = ', $value));
                    $i++;
                }
                
            }
            else
            {
                $criteria->add(new TFilter($property, ' = ', $id));
            }
            $sql->setCriteria($criteria);
            
            $conn = TTransaction::get();
            //echo $sql->getInstruction();
            $result = $conn->Query($sql->getInstruction());
            
            if ($result) {
                $found = array();
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                        //$values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]=$value;
                    }
                    array_push($found, $values);
                }
            }
            
        } else {
            
            $sql = new TSqlSelect;
            $sql->setEntity($table);
            $sql->addColumn(' * ');
            
            
            $conn = TTransaction::get();
            //echo $sql->getInstruction();
            $result = $conn->Query($sql->getInstruction());
            
            
            if ($result) {
                $found = array();
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                       // $values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]= $value;
                    }
                    array_push($found, $values);
                }
                
            }
        }
        TTransaction::close();
        return $found;
    } catch (Exception $e) {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}

/**
 *  Pesquisa Todos os Registros de uma Tabela
 */
function find_all( $table ) {
    return find($table, null, null);
}

function find_id( $table, $property, $id){
    return find($table, $property, $id);
}

/*
 function find_docente_ptd($turma, $disc )
 {
 
 TTransaction::open('myconection');
 
 $found = null;
 try {
 $sql = new TSqlSelect;
 $sql->setEntity('VIEW_DOCENTEXTURMA');
 $sql->addColumn(' * ');
 
 $criteria = new TCriteria;
 $criteria->add(new TFilter('CODI_TURMA', ' = ', $turma));
 $criteria->add(new TFilter('CODI_DISCIPLINA', ' = ', $disc));
 $sql->setCriteria($criteria);
 
 $conn = TTransaction::get();
 $result = $conn->Query($sql->getInstruction());
 
 
 
 if ($result) {
 $found = array();
 while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
 array_push($found, $row);
 }
 }
 
 TTransaction::close();
 return $found;
 }catch (Exception $e) {
 $_SESSION['message'] = $e->GetMessage();
 $_SESSION['type'] = 'danger';
 TTransaction::rollback();
 }
 
 }*/

function check_login($usuario, $senha)
{
    TTransaction::open('myconection');
    try
    {
        $sql = new TSqlSelect;
        $sql->setEntity('view_usuarios_login');
        $sql->addColumn(' * ');
        
        $criteria = new TCriteria;
        $criteria->add(new TFilter('USERNAME', ' = ', $usuario));
        $criteria->add(new TFilter('PASSWORD', ' = ', $senha));
        
        $sql->setCriteria($criteria);
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        
        ($result = $conn->query($sql->getInstruction()));
        
        //if(!$result)throw new Exception("ERRO");
        if ($result) {
            $row = $result->fetch(PDO::FETCH_ASSOC);
        }
        TTransaction::close();
        //if(isset($row))
        //{
        //$row['THUMB'] = find_id('THUMBS', 'ID', $row['THUMB_ID'])[0];
        //return $row;
        //}
        return $row;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}


/* SAVE*/
/**
 *  Salva um Registro em uma Tabela
 *  my é para saber se é mysql
 */
function save($table, $array)
{
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    try
    {
        $sql = new TSqlInsert();
        $sql->setEntity($table);
        foreach ($array as $key=>$value)
        {
            $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
                
        }
        $conn = TTransaction::get();
        //echo "<br>".$sql->getInstruction()."<br>";
        //$conn->exec($sql->getInstruction());
        $conn->query($sql->getInstruction());
        //$id = $conn->lastInsertId();
        TTransaction::close();
        $_SESSION['type'] = 'success';
        return $id;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}
/* SAVE*/

/* SAVE*/
function update($table, $array, $property, $id)
{
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    try
    {
        $sql = new TSqlUpdate();
        $sql->setEntity($table);
        $criteria = new TCriteria;
        if(is_array($id))
        {
            $propriedade = array();
            foreach ($property as $prop)
            {
                $propriedade[] = $prop;
            }
            $i = 0;
            foreach ($id as $value)
            {
                $criteria->add(new TFilter($propriedade[$i], ' = ', $value));
                $i++;
            }
            
        }
        else
        {
            $criteria->add(new TFilter($property, ' = ', $id));
        }
        $sql->setCriteria($criteria);
        foreach ($array as $key=>$value)
        {
            $key = str_replace("'", "", $key);
            
                $sql->setRowData($key, $value);
        }
        $conn = TTransaction::get();
//         echo $sql->getInstruction();
        
//         TTransaction::rollback();
//         exit();
        $conn->query($sql->getInstruction());
        
        TTransaction::close();
        $_SESSION['type'] = 'success';
    }
    catch(Exception $e)
    {
        //echo $e->GetMessage();
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
        
    }
}
/* SAVE*/

function remove($table, $property, $id)
{
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    try
    {
        $sql = new TSqlDelete();
        $sql->setEntity($table);
        $criteria = new TCriteria;
        if(is_array($id))
        {
            $propriedade = array();
            foreach ($property as $prop)
            {
                $propriedade[] = $prop;
            }
            $i = 0;
            foreach ($id as $value)
            {
                $criteria->add(new TFilter($propriedade[$i], ' = ', $value));
                $i++;
            }
            
        }
        else
        {
            $criteria->add(new TFilter($property, ' = ', $id));
        }
        $sql->setCriteria($criteria);
        
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        $result = $conn->query($sql->getInstruction());
        
        TTransaction::close();
        $_SESSION['type'] = 'success';
    }
    catch(Exception $e)
    {
        //echo $e->GetMessage();
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
}

function contar($table, $columns, $property, $id)
{
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    try
    {
        $sql = new TSqlSelect;
        $sql->setEntity($table);
        if(is_array($columns))
        {
            foreach ($columns as $column)
            {
                $sql->addColumn(' COUNT('.$column.') AS '.$column);
            }
        }
        else
        {
            $sql->addColumn(' COUNT('.$columns.')');
        }
        if(isset($id))
        {
            $criteria = new TCriteria;
            if(is_array($id))
            {
                $propriedade = array();
                foreach ($property as $prop)
                {
                    $propriedade[] = $prop;
                }
                $i = 0;
                foreach ($id as $value)
                {
                    $criteria->add(new TFilter($propriedade[$i], ' = ', $value));
                    $i++;
                }
                
            }
            else
            {
                $criteria->add(new TFilter($property, ' = ', $id));
            }
            
            $sql->setCriteria($criteria);
            
        }
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        $result = $conn->Query($sql->getInstruction());
        //print_r($result);
        
        if ($result) {
            $rows = $result->fetch(PDO::FETCH_ASSOC);
            if(count($rows)==1)
            {
                if($rows=="") $rows=0;
                $found = $rows;
            }
            else if(count($rows)>1)
            {
                foreach($rows as $key=>$value) {
                    if($value=="") $value=0;
                    $found["$key"] = $value;
                }
            }
            else
            {
                
            }
            
        }
        TTransaction::close();
        return $found;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
}

function sum($table, $columns, $property=null, $id=null)
{
    TTransaction::open('myconection');
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    try
    {
        $sql = new TSqlSelect;
        $sql->setEntity($table);
        if(is_array($columns))
        {
            foreach ($columns as $column)
            {
                $sql->addColumn(' SUM('.$column.') AS '.$column);
            }
        }
        else
        {
            $sql->addColumn(' SUM('.$columns.')');
        }
        if(isset($id))
        {
            $criteria = new TCriteria;
            $criteria->add(new TFilter($property, ' = ', $id));
            
            $sql->setCriteria($criteria);
            
        }
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        $result = $conn->Query($sql->getInstruction());
        //print_r($result);
        
        if ($result) {
            $rows = $result->fetch(PDO::FETCH_ASSOC);
            if(count($rows)==1)
            {
                if($rows=="") $rows=0;
                $found = $rows;
            }
            else if(count($rows)>1)
            {
                foreach($rows as $key=>$value) {
                    if($value=="") $value=0;
                    $found["$key"] = $value;
                }
            }
            else
            {
                
            }
            
        }
        TTransaction::close();
        return $found;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
}

function save_arquivo($table, $array)
{
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    
    if (file_exists(CONFIG_DB."myconection.ini"))
    {
        //lê INI e retorno um array
        $db = parse_ini_file(CONFIG_DB."myconection.ini");
    }
    else
    {
        //se não existir, lança um erro
        throw new Exception("Arquivo myconection não encontrado");
    }
    
    //lê as informações contidas no arquivo
    $user = $db['user'];
    $pass = $db['pass'];
    $name = $db['name'];
    $host = $db['host'];
    $type = $db['type'];
    
    if($type!="ibase")
    {
        TTransaction::open('myconection');
        
        try
        {
            $sql = new TSqlInsert();
            $sql->setEntity($table);
            foreach ($array as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
            }
            $conn = TTransaction::get();
            $query = ($sql->getInstructionToPrepare());
            $res = $conn->prepare($query['sql']);
            
            $i = 0;
            foreach ($query['bindPar'] as $key=>$value)
            {
                $res->bindParam($key, $array[str_replace(':', '', $key)], $value);
                $i++;
            }
            //$conn->exec($sql->getInstruction());
            //echo $res->queryString;
            $res->execute();
            //$conn->query($sql->getInstruction());
            //$id = $conn->lastInsertId();
            
            $id = $conn->lastInsertId();
            
            TTransaction::close();
            $_SESSION['type'] = 'success';
            return $id;
        }
        catch(Exception $e)
        {
            $_SESSION['message'] = $e->GetMessage();
            $_SESSION['type'] = 'danger';
            TTransaction::rollback();
        }
    }
    else
    {
        //echo 'a';
        $campos = "";
        $valores = "";
        foreach ($array as $key=>$value)
        {
            if($key!="DADOS")
            {
                $campos.=$key.", ";
                if(is_numeric($value))
                {
                    $valores.=$value.", ";
                }
                else
                {
                    $valores.= "'".$value."', ";
                }
            }
        }
        
        
        $dbh = ibase_connect($name, $user, $pass) or die(ibase_errmsg());
        $blob = ibase_blob_import($array['DADOS']);
        $result = ibase_query($dbh, "INSERT into arquivos ($campos DADOS ) values ($valores ?); ", $blob)
        or die(ibase_errmsg());
        
        
        
        $select = ibase_query($dbh, "SELECT * FROM arquivos WHERE NOME='".$array['NOME']."';");
        
        if($result!=false)
        {
            $_SESSION['type']="success";
            $arr = ibase_fetch_assoc($select);
            return $arr['ID'];
        }
        else
        {
            $_SESSION['type']="danger";
        }
    }
    
}


function update_arquivo($table, $array, $property, $id)
{
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    if (file_exists(CONFIG_DB."myconection.ini"))
    {
        //lê INI e retorno um array
        $db = parse_ini_file(CONFIG_DB."myconection.ini");
    }
    else
    {
        //se não existir, lança um erro
        throw new Exception("Arquivo myconection não encontrado");
    }
    
    //lê as informações contidas no arquivo
    $user = $db['user'];
    $pass = $db['pass'];
    $name = $db['name'];
    $host = $db['host'];
    $type = $db['type'];
    
    if($type!="ibase")
    {
        TTransaction::open('myconection');
        
        try
        {
            $sql = new TSqlUpdate();
            $sql->setEntity($table);
            $criteria = new TCriteria;
            $criteria->add(new TFilter($property, ' = ', $id));
            $sql->setCriteria($criteria);
            foreach ($array as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
            }
            $conn = TTransaction::get();
            $query = ($sql->getInstructionToPrepare());
            $res = $conn->prepare($query['sql']);
            
            $i = 0;
            foreach ($query['bindPar'] as $key=>$value)
            {
                //echo $array[str_replace(':', '', $key)].'oi';
                $res->bindParam($key, $array[str_replace(':', '', $key)], $value);
                $i++;
            }
            //$conn->exec($sql->getInstruction());
            $res->execute();
            //$conn->query($sql->getInstruction());
            TTransaction::close();
            $_SESSION['type'] = 'success';
        }
        catch(Exception $e)
        {
            $_SESSION['message'] = $e->GetMessage();
            $_SESSION['type'] = 'danger';
            TTransaction::rollback();
        }
    }
    
    else
    {
        if(isset($array['DADOS']))
        {
            $valores="";
            foreach ($array as $key=>$value)
            {
                if($key!="DADOS")
                {
                    $valores.=$key."="."'".$value."', ";
                }
            }
            
            $dbh = ibase_connect($name, $user, $pass) or die(ibase_errmsg());
            $blob = ibase_blob_import($array['DADOS']);
            //echo "UPDATE arquivos set $valores DADOS=' ? ' WHERE ID='$id'";
            
            $result = ibase_query($dbh, "UPDATE $table set $valores DADOS=? WHERE $property='$id'", $blob)
            or die(ibase_errmsg());
            
            if($result!=false)
            {
                $_SESSION['type']="success";
            }
            else
            {
                $_SESSION['type']="danger";
            }
            fclose($array['DADOS']);
        }
        else
        {
            update($table, $array, $property, $id);
        }
    }
    
}


function save_acessos($array_acessos)
{
   /* echo'<pre>';
     print_r($array_acessos);
     echo '</pre>';
     exit;*/
    
    TTransaction::open('myconection');
    
    
    
    try
    {
        $sqls = array();
        for($i=0; $i<count($array_acessos);$i++)
        {
            $sqls[$i] = new TSqlInsert();
            $sqls[$i]->setEntity('tbl_acessos');
            foreach ($array_acessos[$i] as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                    $sqls[$i]->setRowData($key, $value);
                    
            }
        }
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        //$conn->exec($sql->getInstruction());
        foreach ($sqls as $sql)
        {
            $conn->exec($sql->getInstruction());
        }
        //$id = $conn->lastInsertId();
        TTransaction::close();
        $_SESSION['type'] = 'success';
        //return $id;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}


function find_acessos($id_perfil, $url_page)
{
    $url_page = str_replace(BASEURL, "/", $url_page);
    //return true;
    TTransaction::open('myconection');
    
    try
    {
        $sql = new TSqlSelect;
        $sql->setEntity('view_acessos');
        $sql->addColumn(' * ');
        
        $criteria = new TCriteria;
        $criteria->add(new TFilter('ID_PERFIL', ' = ', $id_perfil));
        $criteria->add(new TFilter('URL', ' = ', $url_page));
        $sql->setCriteria($criteria);
        
        $conn = TTransaction::get();
        $result = $conn->Query($sql->getInstruction());
        $qtde = $result->fetchAll();
        
        if (count($qtde)>0) {
            $retorna = true;
        }
        
        else
        {
            $retorna = false;
        }
        TTransaction::close();
        return $retorna;
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
        return false;
    }
}

function find_like($table, $property, $id)
{
    return find($table, $property, "%".$id."%");
}











function save_obra($obra, $enderecos)
{
    TTransaction::open('myconection');
    $conn = TTransaction::get();
    
    
    try
    {
        $sql = new TSqlInsert();
        $sql->setEntity('tbl_obras');
        
        foreach ($obra as $key=>$value)
        {
            $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
        }
        //echo $sql->getInstruction();
        $conn->query($sql->getInstruction());
        $idObra = $conn->lastInsertId();

        
        
        foreach ($enderecos as $endereco){
            $sql = new TSqlSelect;
            $sql->setEntity('tbl_enderecos');
            $sql->addColumn(' * ');
            
            $criteria = new TCriteria;
            
            $criteria->add(new TFilter('CEP', ' = ', $endereco['CEP']));
            $criteria->add(new TFilter('NUMERO', ' = ', $endereco['NUMERO']));
            $criteria->add(new TFilter('COMPLEMENTO', ' = ', $endereco['COMPLEMENTO']));
            $criteria->add(new TFilter('DESCRICAO', ' = ', $endereco['DESCRICAO']));
            
            $sql->setCriteria($criteria);
            
            $conn = TTransaction::get();
            //echo $sql->getInstruction();
            $result = $conn->Query($sql->getInstruction());
            
            if ($result) {
                $found = array();
                
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                        //$values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]=$value;
                    }
                    array_push($found, $values);
                }
            }
            if(count($found)>0)
            {
                $idEndereco = $found[0]['ID_ENDERECO'];
                
            }
            else{
                
                $sql = new TSqlInsert();
                $sql->setEntity('tbl_enderecos');
                
                foreach ($endereco as $key=>$value)
                {
                    $key = str_replace("'", "", $key);
                        $sql->setRowData($key, $value);
                }
                
                $conn->query($sql->getInstruction());
                $idEndereco = $conn->lastInsertId();
                
            }
            
            $tipoEnd['ID_OBRA'] = $idObra;
            $tipoEnd['ID_ENDERECO'] = $idEndereco;
            
            $sql = new TSqlInsert();
            $sql->setEntity('tbl_endereco_obra');
            
            foreach ($tipoEnd as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                    $sql->setRowData($key, $value);
            }
            
            $conn->query($sql->getInstruction());
            
        }
        
        
        TTransaction::close();
        $_SESSION['type'] = 'success';
        
        return $idObra;
        
    }
    catch(Exception $e)
    {
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}








function edit_obra($obras, $id, $enderecos)
{
    TTransaction::open('myconection');
    $conn = TTransaction::get();
    try
    {
        
    
    $sql = new TSqlUpdate();
    $sql->setEntity('tbl_obras');
    
    $criteria = new TCriteria;
    $criteria->add(new TFilter('ID_OBRA', ' = ', $id));
    
    $sql->setCriteria($criteria);
        
    foreach ($obras as $key=>$value)
    {
        $key = str_replace("'", "", $key);
        if(is_string($value))
            //$value = utf8_decode($value);
            $sql->setRowData($key, $value);
    }
    
    //echo $sql->getInstruction();
    //exit();
    //echo "<br/>".$sql->getInstruction()."<br/>";
    $conn->query($sql->getInstruction());
    
    
    foreach ($enderecos as $endereco){
        $sql = new TSqlSelect;
        $sql->setEntity('tbl_enderecos');
        $sql->addColumn(' * ');
        
        $criteria = new TCriteria;
        
        $criteria->add(new TFilter('CEP', ' = ', $endereco['CEP']));
        $criteria->add(new TFilter('NUMERO', ' = ', $endereco['NUMERO']));
        $criteria->add(new TFilter('COMPLEMENTO', ' = ', $endereco['COMPLEMENTO']));
        $criteria->add(new TFilter('DESCRICAO', ' = ', $endereco['DESCRICAO']));
        
        $sql->setCriteria($criteria);
        
        $conn = TTransaction::get();
        //echo $sql->getInstruction();
        $result = $conn->Query($sql->getInstruction());
        
        if ($result) {
            $found = array();
            
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                
                $values = array();
                foreach($row as $key=>$value)
                {
                   // $values[$key]=(is_string($value))? utf8_encode($value) : $value;
                    $values[$key]= $value;
                }
                array_push($found, $values);
            }
        }
        if(count($found)>0)
        {
            $idEndereco = $found[0]['ID_ENDERECO'];
            
        }
        else{
            
            $sql = new TSqlInsert();
            $sql->setEntity('tbl_enderecos');
            
            foreach ($endereco as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                    $sql->setRowData($key, $value);
            }
            
            $conn->query($sql->getInstruction());
            $idEndereco = $conn->lastInsertId();
            
        }
        
        $tipoEnd['ID_OBRA'] = $id;
        $tipoEnd['ID_ENDERECO'] = $idEndereco;
        
        $sql = new TSqlInsert();
        $sql->setEntity('tbl_endereco_obra');
        
        foreach ($tipoEnd as $key=>$value)
        {
            $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
        }
        
        $conn->query($sql->getInstruction());
        
    }
    
    
   TTransaction::close();
   //TTransaction::rollback();
    $_SESSION['type'] = 'success';
}

catch(Exception $e)
{
    echo $e->GetMessage();
    $_SESSION['message'] = $e->GetMessage();
    $_SESSION['type'] = 'danger';
    TTransaction::rollback();
}
}



function edit_empresa($enderecos,$contatos)
{
    TTransaction::open('myconection');
    $conn = TTransaction::get();
    try
    {
        
        foreach ($enderecos as $endereco){
            $sql = new TSqlSelect;
            $sql->setEntity('tbl_enderecos');
            $sql->addColumn(' * ');
            
            $criteria = new TCriteria;
            
            
            $criteria->add(new TFilter('DESCRICAO', ' = ', $endereco['DESCRICAO']));
            $criteria->add(new TFilter('CEP', ' = ', $endereco['CEP']));
            $criteria->add(new TFilter('NUMERO', ' = ', $endereco['NUMERO']));
            $criteria->add(new TFilter('COMPLEMENTO', ' = ', $endereco['COMPLEMENTO']));
            
            $sql->setCriteria($criteria);
            
            $conn = TTransaction::get();
            //echo $sql->getInstruction();
            $result = $conn->Query($sql->getInstruction());
            
            if ($result) {
                $found = array();
                
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                        // $values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]= $value;
                    }
                    array_push($found, $values);
                }
            }
            if(count($found)>0)
            {
                $idEndereco = $found[0]['ID_ENDERECO'];
                
            }
            else{
                
                $sql = new TSqlInsert();
                $sql->setEntity('tbl_enderecos');
                
                foreach ($endereco as $key=>$value)
                {
                    $key = str_replace("'", "", $key);
                    $sql->setRowData($key, $value);
                }
                $conn->query($sql->getInstruction());
                $idEndereco = $conn->lastInsertId();
                
            }
            
            $tipoEnd['ID_EMPRESA'] = $_SESSION["id_empresa"];
            $tipoEnd['ID_ENDERECO'] = $idEndereco;
            
            $sql = new TSqlInsert();
            $sql->setEntity('tbl_endereco_empresa');
            
            foreach ($tipoEnd as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
            }
            
            $conn->query($sql->getInstruction());
            
        }
        
        
        
        
        foreach ($contatos as $contato){
            $sql = new TSqlSelect;
            $sql->setEntity('tbl_contatos');
            $sql->addColumn(' * ');
            
            $criteria = new TCriteria;
            
            $criteria->add(new TFilter('TIPO_VALOR', ' = ', $contato['TIPO_VALOR']));
            $criteria->add(new TFilter('VALOR', ' = ', $contato['VALOR']));
            
            $sql->setCriteria($criteria);
            
            $conn = TTransaction::get();
            //echo $sql->getInstruction();
            $result = $conn->Query($sql->getInstruction());
            
            if ($result) {
                $found = array();
                
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                        // $values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]= $value;
                    }
                    array_push($found, $values);
                }
            }
            if(count($found)>0)
            {
                $idContato = $found[0]['ID_CONTATO'];
                
            }
            else{
                
                $sql = new TSqlInsert();
                $sql->setEntity('tbl_contatos');
                
                foreach ($contato as $key=>$value)
                {
                    $key = str_replace("'", "", $key);
                    $sql->setRowData($key, $value);
                }
                
                $conn->query($sql->getInstruction());
                $idContato = $conn->lastInsertId();
                
            }
            
            $tipoCont['ID_EMPRESA'] = $_SESSION["id_empresa"];
            $tipoCont['ID_CONTATO'] = $idContato;
            
            $sql = new TSqlInsert();
            $sql->setEntity('tbl_contato_empresa');
            
            foreach ($tipoCont as $key=>$value)
            {
                $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
            }
            
            $conn->query($sql->getInstruction());
            
        }
        
        
        TTransaction::close();
        //TTransaction::rollback();
        $_SESSION['type'] = 'success';
    }
    
    catch(Exception $e)
    {
        echo $e->GetMessage();
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
}






function saveAll($itens)
{
    TTransaction::open('myconection');
    $conn = TTransaction::get();
    try
    {
        
        foreach ($itens as $item){
            
            $item['ID_FORNECEDOR'] = $_SESSION["id_fornecedor"];
            
            $sql = new TSqlSelect;
            $sql->setEntity('tbl_produtos_fornecedor');
            $sql->addColumn(' * ');
            
            $criteria = new TCriteria;
            
            $criteria->add(new TFilter('ID_ESPECIFICACAO', ' = ', $item['ID_ESPECIFICACAO']));
            $criteria->add(new TFilter('ID_FORNECEDOR', ' = ', $_SESSION["id_fornecedor"]));
            
            $sql->setCriteria($criteria);
            
            
            
            $conn = TTransaction::get();
            echo $sql->getInstruction();
            
            
            $result = $conn->Query($sql->getInstruction());
            
            if ($result) {
                $found = array();
                
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    
                    $values = array();
                    foreach($row as $key=>$value)
                    {
                        //$values[$key]=(is_string($value))? utf8_encode($value) : $value;
                        $values[$key]=$value;
                    }
                    array_push($found, $values);
                }
            }
            if(count($found)>0)
            {
                $idItem = $found[0]['ID_PRODUTO_FORNECEDOR'];
                
            }
            else{
                
                $sql = new TSqlInsert();
                $sql->setEntity('tbl_produtos_fornecedor');
                
                foreach ($item as $key=>$value)
                {
                    $key = str_replace("'", "", $key);
                        $sql->setRowData($key, $value);
                }
                echo $sql->getInstruction();
                $conn->query($sql->getInstruction());
                $idItem = $conn->lastInsertId();   
                
            }
            
        }
        
        TTransaction::close();
        $_SESSION['type'] = 'success';
        return $itensRDM['N_RDM'];
        
    }
    catch(Exception $e)
    {
        echo $e->GetMessage();
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}


function save_pedido($pedidos,$itens)
{
    TTransaction::open('myconection');
    $conn = TTransaction::get();
    try
    {
        
        $sql = new TSqlInsert();
        $sql->setEntity('tbl_pedido');
        
        $pedidos['ID_CONSTRUTOR'] = $_SESSION["id_construtor"];
        
        foreach ($pedidos as $key=>$value)
        {
            $key = str_replace("'", "", $key);
                $sql->setRowData($key, $value);
        }
        echo $sql->getInstruction();
        $conn->query($sql->getInstruction());
        $idPedido = $conn->lastInsertId();
        
        
        foreach ($itens as $item){
            
            $item['ID_PEDIDO'] = $idPedido;
            
            {
                
                $sql = new TSqlInsert();
                $sql->setEntity('tbl_itens_pedido');
                
                foreach ($item as $key=>$value)
                {
                    $key = str_replace("'", "", $key);
                        $sql->setRowData($key, $value);
                }
                echo $sql->getInstruction();
                $conn->query($sql->getInstruction());
                $idItem = $conn->lastInsertId();
                
            }
            
        }
        
        TTransaction::close();
        $_SESSION['type'] = 'success';
        return $idPedido;
        
    }
    catch(Exception $e)
    {
        echo $e->GetMessage();
        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
        TTransaction::rollback();
    }
    
}


function relatorios($table, $dataInicio, $dataFim)
{
    $table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
    $dataInicio = new DateTime($dataInicio);
    $dataFim = new DateTime($dataFim);
    $conn = TConnection::open('myconection');
    $found=array();
        try
        {
            $sql = new TSqlSelect();
            $sql->addColumn(" * ");
            $sql->setEntity($table);
            
            
            
            $criteria = new TCriteria();
            $criteria->add(new TFilter("(DATA_OFERTA", ' BETWEEN ', $dataInicio->format("Y-m-d")."' AND '".$dataFim->format("Y-m-d")));
            $criteria->add(new TFilter("FINALIZADO", ' = ','S'), ') AND ');
                
                
                
                $sql->setCriteria($criteria);
                echo $sql->getInstruction();  
                $result = $conn->query($sql->getInstruction());
           
            if ($result)
            {
                while ($row = $result->fetch(PDO::FETCH_ASSOC))
                {
                    array_push($found, $row);
                }
                
            }
                
        }
        catch (Exception $e)
        {
            $_SESSION['message'] = $e->GetMessage();
            $_SESSION['type'] = 'danger';
            //TTransaction::rollback();
        }
        return $found;
}

?>