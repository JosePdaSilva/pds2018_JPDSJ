<?php
require_once 'config.php';

if(!isset($_SESSION["username"]))
{
    header('Location: index.php');
    exit();
}
$find = find_id('VIEW_USUARIOS_LOGIN', 'ID_USUARIO', $_SESSION['id_usuario']);
if(count($find)>0)
{
    $fields = $find[0];
    if($fields['TEMP_PASSWORD']!='S')
    {
        header('Location: home.php');
        exit();
    }
}
if(isset($_POST['submit']))
{
    $find = find_id('TBL_USUARIOS', ['ID_USUARIO','PASSWORD'], [$_SESSION['id_usuario'], md5($_POST['TEMP_PASS'])]);
    if(count($find)>0)
    {
        $fields = $find[0];
        update('TBL_USUARIOS', ['PASSWORD'=>md5($_POST['PASSWORD']), 'TEMP_PASSWORD'=>'N'], 'ID_USUARIO', $_SESSION['id_usuario']);
        if($_SESSION['type']=='success')
        {
            $_SESSION['message'] = "Senha redefinida com sucesso";
            header('Location: home.php');
            exit();
        }
    }
    else
    {
        header('Location: redefinir-senha.php?erro=1');
    }
}
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $pagina['TITULO'];?></title>
  <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASEURL?>../img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASEURL?>../img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASEURL?>../img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASEURL?>../img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASEURL?>../img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASEURL?>../img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASEURL?>../img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASEURL?>../img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASEURL?>../img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo BASEURL?>../img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo BASEURL?>../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo BASEURL?>../img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASEURL?>../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo BASEURL?>../img/favicon/manifest.json">
    <script src="<?php echo BASEURL?>js/enderecos.js"></script>
    <script src="<?php echo BASEURL?>js/carregar.js"></script>
    <script src="<?php echo BASEURL?>js/contatos.js"></script>
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!-- Bootstrap core CSS-->
  <link href="<?php echo BASEURL?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo BASEURL?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="<?php echo BASEURL?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- DataTables CSS -->
<link href="<?php echo BASEURL; ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DataTables Responsive CSS -->
    <link href="<?php echo BASEURL; ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo BASEURL?>css/sb-admin.css" rel="stylesheet">
</head>

<body class="sticky-footer bg-dark" id="page-top">
	
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="login-panel card">
                    <div class="card-heading">
                        <h3 class="panel-title">Redefinir senha</h3>
                    </div>
                    <div class="card-body">
                    	<?php 
                    		if(isset($_GET['erro']))
                    		{
	                    		if($_GET['erro']==1)
	                    		{
                    	?>
	                    			<div class="row">
				                    	<div class="col-lg-12">
											<div class="alert alert-danger" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												Senha Temporária Incorreta
											</div>
										</div>
									</div>
                    	<?php
                    			}
                    		}
                    	?>
                        <form role="form" action="redefinir-senha.php" method="post">
                            <fieldset>
                            	<div class="row">
	                                <div class="form-group col-lg-12">
	                                    <input class="form-control" id="senha_temp" placeholder="Senha Temporária" name="TEMP_PASS" type="password">
	                                </div>
                                </div>
                                
                                <div class="row">
	                                <div class="form-group col-lg-12">
	                                    <input class="form-control" id="campo_senha" placeholder="Nova Senha" name="PASSWORD" type="password">
	                                </div>
                                </div>
                                
                               	 
                               	 
                               	 <div class="row col-lg-12">
                               	 	<div class="form-group">
										<button type="submit" name="submit" class="btn btn-primary" id="btn_login">Redefinir</button>
										<a href="logoff.php" class="btn btn-secondary">Logout</a>
									</div>
								 </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deseja sair do Obrix?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Clique em Logout para sair da sua conta.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="<?php echo BASEURL?>logoff.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo BASEURL?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo BASEURL?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo BASEURL?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="<?php echo BASEURL?>vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo BASEURL?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo BASEURL?>vendor/datatables/dataTables.bootstrap4.js"></script>
    
    
    <script src="<?php echo BASEURL?>js/buscaCEP.js"></script>
    <script src="<?php echo BASEURL?>js/odonto.js"></script>
    <script src="<?php echo BASEURL?>js/contatos.js"></script>
    
    
    <script src="<?php echo BASEURL?>js/sb-admin.min.js"></script>
    <script src="<?php echo BASEURL?>js/sb-admin-datatables.min.js"></script>
    <script src="<?php echo BASEURL?>js/sb-admin-charts.js"></script>
</body>

</html>
