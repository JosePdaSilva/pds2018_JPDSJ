<?php

require_once 'config.php';

if(isset($_SESSION["username"]))
{
    $find = find_id('VIEW_USUARIOS_LOGIN', 'ID_USUARIO', $_SESSION['id_usuario']);
    
    if(count($find)>0)
    {
        $fields = $find[0];
        if($fields['TEMP_PASSWORD']=='S')
        {
            header('Location: '.BASEURL.'redefinir-senha.php');
            exit();
        }
        else
        {
            if($_SESSION["id_perfil"]==1)
            {
                header('Location: home.php');
            }
            else
            {
                header('Location: ../index.php');
            }
            exit();
        }
    }
    else
    {
        header('Location: '.BASEURL.'index.php');
        exit();
    }
    
}

else if(isset($_COOKIE["id_usuario"]) && isset($_COOKIE["data_login"]))
{
    if($_COOKIE["id_usuario"]!="deleted" && $_COOKIE["data_login"]!="deleted")
    {
        $find = find_id('VIEW_USUARIOS_LOGIN', 'ID_USUARIO', $_COOKIE["id_usuario"]);
        if(count($find)>0)
        {
            
            $result = $find[0];
            setcookie("id_usuario", $result['ID_USUARIO'], time()+3600*24*365);
            $agora = new DateTime();
            setcookie("data_login", $agora->format('Y-m-d H:i:s'), time()+3600*24*365, BASEURL);
            $_SESSION["id_usuario"] = $result['ID_USUARIO'];
            $_SESSION["username"] = $result['USERNAME'];
            $_SESSION["id_perfil"] = $result['ID_PERFIL'];
            
            if($result['TEMP_PASSWORD']!='S')
            {
                header('Location: redefinir-senha.php');
                exit();
            }
            else
            {
                if($_SESSION["id_perfil"]==1)
                {
                    header('Location: home.php');
                }
                else
                {
                    header('Location: ../index.php');
                }
                exit();
            }
        }
        else
        {
            unset($_COOKIE["id_usuario"]);
            header('Location: index.php');
            exit();
        }
    }
}


if(!isset($_POST))
{
    header('Location: index.php');
    exit();
}

else
{
    
    $usuario = $_POST['usuario'];
    //$senha   = md5($_POST['senha']);
    $result = check_login($usuario['USERNAME'], md5($usuario['PASSWORD']));
    if($result)
    {
        if(isset($_POST['remember']))
        {
            setcookie("id_usuario", $result['ID_USUARIO'], time()+3600*24*365);
        }
        $agora = new DateTime();
        setcookie("data_login", $agora->format('Y-m-d H:i:s'), time()+3600*24*365, BASEURL);
        $_SESSION["id_usuario"] = $result['ID_USUARIO'];
        $_SESSION["username"] = $result['USERNAME'];
        $_SESSION["id_perfil"] = $result['ID_PERFIL'];
        
        
        if($result['TEMP_PASSWORD']=='S')
        {
            header('Location: redefinir-senha.php');
            exit();
        }
        else
        {
            if($_SESSION["id_perfil"]==1)
            {
                header('Location: home.php');
            }
            else
            {
                header('Location: ../index.php');
            }
            exit();
        }
    }
    else
    {
        header('Location: index.php?erro=1');
        exit();
    }
}