<?php 
require_once 'functions.php';
index();
include HEADER;


?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Convidar parceiros</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
		
<div class="row">
	<div class="col-lg-12">
        <div class="card mb-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                   		<i class="fa fa-paper-plane-o"></i> Convide seus parceiros
                    </div>
                </div>
            </div>

    		<form action="index.php" method="POST">
    			<div class="card-body">
    				<div class="row">
    					<div class="col-md-12">
    						<div class="col-md-6 offset-md-3">
    							<div id="myList"></div>
    						</div>
    
    						<div class="row">
    							<div class="col-md-3  offset-md-3" style="margin-bottom: 10px">
    								<button type="button" class="btn btn-success  btn-block"
    									onclick="myFunction()">
    									<i class="fa fa-plus"></i> Adicionar Email
    								</button>
    							</div>
    							<div class="col-md-3">
    								<button class="btn btn-info  btn-block" type="submit"
    									id="btn_submit" name="submit">
    									<i class="fa fa-paper-plane-o"></i> Enviar convite
    								</button>
    							</div>
    						</div>
    						<br>
    					</div>
    				</div>
    			</div>
    		</form>
    	</div>
	</div>
</div>


<?php
include DELETEMODAL;
include FOOTER;
?>

<script>
var qq=0;
function myFunction() {
	
    var node = document.createElement("div");
	node.setAttribute('class', 'row');
	node.setAttribute('id', 'email'+qq);
	
    var colEmail = document.createElement("div");
    colEmail.setAttribute("class", "col-md-10 col-sm-10");
    
        var email =  document.createElement("INPUT");
        email.setAttribute("type", "email");
        email.setAttribute("name", "email["+qq+"][EMAIL]");
        email.setAttribute("placeholder", "Digite o email do parceiro");
        email.setAttribute("style", "margin-bottom:10px");
        email.setAttribute("required","required");
        email.setAttribute("class", "form-control");
   	colEmail.appendChild(email);


   	var colBtnExcluir = document.createElement("div");
   	colBtnExcluir.setAttribute("class", "col-md-2 col-sm-2");
        var btnExcluir =  document.createElement("button");
        btnExcluir.setAttribute("type", "button");
        btnExcluir.setAttribute("class", "btn btn-danger");
        btnExcluir.setAttribute("id", "btnExcluir"+qq);
        btnExcluir.setAttribute("onclick", "removeEmail("+qq+")");
		btnExcluir.innerHTML = "X";
        
    colBtnExcluir.appendChild(btnExcluir);

	node.appendChild(colEmail);
	node.appendChild(colBtnExcluir);
    
    document.getElementById("myList").appendChild(node);
    qq++;
}

function removeEmail(idEmail)
{
	//$('#btnExcluir'+idAula).tooltip('dispose');
	
	var filha = document.getElementById("email"+idEmail);
	var pai = document.getElementById("myList");

	pai.removeChild(filha);
}
</script>