<?php 
require_once 'functions.php';
add();
include HEADER;
?>

<script type="text/javascript">
function selectAllByDir(dir, checked)
{
	var checkboxes = document.getElementsByClassName("acesso-"+dir);
	var i;
    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = checked;
    }
	allSelected(sistema);
}

function selectAll(checked)
{
	var checkboxes = document.getElementsByClassName("acesso");
	var i;
    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = checked;
    }
}

function allSelectedDir(dir, checked)
{
	if(checked==true)
	{
		var checkboxes = document.getElementsByClassName("acesso-"+dir);
		var i=0;
		var todos=true;
		while(i<checkboxes.length && todos==true)
	    {
	        if(checkboxes[i].checked==false)
	        {
	            todos=false;
	        }
	        i++;
	    }
	    if(todos==true)
	    {
	    	document.getElementById("selectdir"+dir).checked=true;
	    	allSelected();
	    }
	    else
	    {
			document.getElementById("selectdir"+dir).checked=false;
			document.getElementById("selectall").checked=false;
	    }
	}
	else
	{
		document.getElementById("selectdir"+dir).checked=false;
		document.getElementById("selectall").checked=false;
	}
}

function allSelected()
{
	var checkboxes = document.getElementsByClassName("acesso");
	var i=0;
	var todos=true;
	while(i<checkboxes.length && todos==true)
    {
        if(checkboxes[i].checked==false)
        {
            todos=false;
        }
        i++;
    }
	if(todos==true)
    {
		document.getElementById("selectall").checked=true;
    }
	else
	{
		document.getElementById("selectall").checked=false;
	}
}

</script>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Cadastrar perfil</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<form action="add.php" method="post" role="form">
	<div class="row">

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 offset-md-4">
    				<div class="form-group">
    					<label for="campo_usuario">Nome do perfil</label>
    					<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado um nome de perfil"></i>
    					 <input
    						type="text" class="form-control" id="campo_usuario"
    						name="perfil['NOME_PERFIL']" placeholder="Nome do pefil" required
    						autofocus />
    				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<hr>
				</div>
			</div>
			
	<div class="row">
		<div class="col-xs-12 col-md-6 offset-md-3" >
			<br>			
			<p >
				<label  for="selectall">Selecionar todos os diretórios do sistema</label> <input
					id="selectall" type="checkbox" 
					onchange="selectAll(this.checked)" />
			</p>
			
			
			
			
				<?php
                        foreach ($paginas as $k => $v) {
                            ?>
                            
                            
			<div class="card">
				<div class="card-header">
					<div class='row'>
						<div class='col-xs-12' >
							<p><strong><?php echo $k;?></strong></p> <span
								class="float-right caption"> <label 
								for="selectdir<?php echo $k?>">Selecionar todos deste
									diretório</label> <input style="float:right #inportant;"id="selectdir<?php echo $k?>"
								class="acesso" type="checkbox"
								onchange="selectAllByDir('<?php echo $k;?>', this.checked)" />
							</span>
						</div>
					</div>
				</div>

				<div class='card-body'>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">

								<table id="tabela-acessos"
									class="table-responsible table-striped">
									<thead>

										<tr class="rcb">
											<th width="100%" style="border:none">Página</th>
											<th style="text-align: center;border:none">Permitir</th>
										</tr>
									</thead>
									<tbody>
											<?php
                                                    foreach ($v as $pagina) {
                                                        ?>
										<tr class="rc">
											<td style="padding: 0;"><label
												style="margin: 0; padding: 8px; width: 100%; height: 100%; font-weight: 400;"
												for="<?php echo $pagina['URL'] ?>"><?php echo $pagina['TITULO'] ?></label></td>
											<td class="check" style="text-align: center;"><input
												class="acesso acesso-<?php echo $k;?>"
												name="acesso[<?php echo $pagina['ID_PAGINA'] ?>]"
												type="checkbox" value="<?php echo $pagina['ID_PAGINA'] ?>"
												id="<?php echo $pagina['URL'] ?>"
												
												onchange="allSelectedDir('<?php echo $k;?>', this.checked)"></td>
										</tr>
											<?php
}
?>
												</tbody>
								</table>

							</div>
							<!-- form-group -->
						</div>
					</div>
					<!-- row -->
				</div>
				<!-- panel body -->
			</div>
			<!-- panel -->
			<br/>
				<?php
}
?>
		</div>
	</div>
			<div class="row">
						<div class="col-md-3 offset-md-3">
							<button type="submit" name="submit"
								class="btn btn-primary btn-lg btn-block"
								style="font-size: 15px;">
								<i class="fa fa-check-square"></i> Salvar
							</button>
						</div>

						<div class="col-md-3">
							<a href="index.php" class="btn btn-secondary btn-lg btn-block"
								style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
							</a>
						</div>
			</div>

<br>

		<!-- /.row (nested) -->
			
		</div>
	</div>
	<!-- /.panel-body -->
</form>

<?php 
include FOOTER;
?>