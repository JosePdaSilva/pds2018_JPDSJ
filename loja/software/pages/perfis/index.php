<?php 
require_once 'functions.php';
index();
include HEADER;


?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Perfis</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

	
<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
		<div class="row">        
          <div class="col-md-11">
          <i class="fa fa-table"></i> Perfis cadastrados
          </div>
          <div class="col-md-1">
      		<a data-toggle="tooltip" data-placement="left" title="Cadastrar novo perfil" href="<?php echo BASEURL?>pages/perfis/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>
          </div>
        </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome perfil</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
							foreach ($perfis as $perfil)
								{
						?>
							<tr class="odd gradeX">
										<td><?php echo $perfil['NOME_PERFIL']?></td>
												<td style="text-align:center">
															<a href="edit.php?id=<?php echo $perfil['ID_PERFIL'];?>" class="btn btn-sm btn-warning"><i data-toggle="tooltip" data-placement="left" title="Editar perfil selecionado" class="fa fa-edit"></i></a>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $perfil['ID_PERFIL']?>" data-id="<?php echo $perfil['ID_PERFIL']?>" data-perfil="<?php echo $perfil['NOME_PERFIL']?>" data-tipo='PERFIL'><i data-toggle="tooltip" data-placement="left" title="Excluir perfil selecionado" class="fa fa-trash-o"></i></a>
												</td>
									</tr>
									<?php 
											}
										?>
										    
                
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>