<?php
require_once 'functions.php';
add();
include HEADER;
?>

<script type="text/javascript">
function selectAllByDir(dir, sistema, checked)
{
	var checkboxes = document.getElementsByClassName("acesso-"+sistema+" acesso-"+dir);
	var i;
    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = checked;
    }
	allSelected(sistema);
}

function selectAll(checked, sistema)
{
	var checkboxes = document.getElementsByClassName("acesso-"+sistema);
	var i;
    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = checked;
    }
}

function disabledAll(checked, sistema)
{
	var checkboxes = document.getElementsByClassName("acesso-"+sistema);
	var checkall = document.getElementById("selectall"+sistema);
	var i;
	var todos=true;
	for (i = 0; i < checkboxes.length; i++)
	{
		checkboxes[i].disabled = !checked;
	}
	checkall.disabled = !checked;
}

function allSelectedDir(dir, sistema, checked)
{
	if(checked==true)
	{
		var checkboxes = document.getElementsByClassName("acesso-"+sistema+" acesso-"+dir);
		var i=0;
		var todos=true;
		while(i<checkboxes.length && todos==true)
	    {
	        if(checkboxes[i].checked==false)
	        {
	            todos=false;
	        }
	        i++;
	    }
	    if(todos==true)
	    {
	    	document.getElementById("selectdir"+sistema+dir).checked=true;
	    	allSelected(sistema);
	    }
	    else
	    {
			document.getElementById("selectdir"+sistema+dir).checked=false;
			document.getElementById("selectall"+sistema).checked=false;
	    }
	}
	else
	{
		document.getElementById("selectdir"+sistema+dir).checked=false;
		document.getElementById("selectall"+sistema).checked=false;
	}
}

function allSelected(sistema)
{
	var checkboxes = document.getElementsByClassName("acesso-"+sistema);
	var i=0;
	var todos=true;
	while(i<checkboxes.length && todos==true)
    {
        if(checkboxes[i].checked==false)
        {
            todos=false;
        }
        i++;
    }
	if(todos==true)
    {
		document.getElementById("selectall"+sistema).checked=true;
    }
	else
	{
		document.getElementById("selectall"+sistema).checked=false;
	}
}

</script>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Cadastrar Perfil</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<form action="add.php" method="post" role="form">
	<div class="row">

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 offset-md-4">
    				<div class="form-group">
    					<label for="campo_usuario">Nome do Perfil</label> <input
    						type="text" class="form-control" id="campo_usuario"
    						name="perfil['PERFIL']" placeholder="Nome do Pefil" required
    						autofocus />
    				</div>
				</div>
			</div>
			
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">Permissão de Acesso</h3>

                    		<ul class="nav nav-tabs" role="tablist">
                    			 <li class="nav-item">
                                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#home" role="tab" href="#">Master</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab">Fornecedor</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#messages" role="tab">Construtors</a>
                                  </li>
                    		</ul>


                    <div class="tab-content">
                    
                      <div class="tab-pane active" id="home" role="tabpanel">
                      	<div class="col-xs-12 col-md-6 offset-md-3">
							<br>
						
							
							<p>
								<label for="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>">Selecionar todas as páginas</label> <input
									id="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox" 
									onchange="selectAll(this.checked, '<?php //echo $sistema['info']['SIGLA_SISTEMA'] ?>')" />
							</p>
						
                                            
                                            
							<div class='panel panel-default'>
								<div class='panel-heading'>
									<div class='row'>
										<div class='col-md-12'>
											<strong><?php //echo $k;?></strong> <span
												class="pull-right caption"> <label
												for="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>">Selecionar todos deste
													diretório</label> <input id="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>"
												class="acesso acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox"
												onchange="selectAllByDir('<?php //echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)" />
											</span>
										</div>
									</div>
								</div>

								<div class='panel-body'>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">

												<table id="tabela-acessos"
													class="table table-responsible table-striped">
													<thead>

														<tr class="rcb">
															<th width="75%">Página</th>
															<th style="text-align: center;">Permitir</th>
														</tr>
													</thead>
													<tbody>
														
							
											<?php
                							foreach ($paginas['1'] as $pagina)
                							{
                                            ?>
														<tr class="rc">
															<td style="padding: 0;"><label
																style="margin: 0; padding: 8px; width: 100%; height: 100%; font-weight: 400;"
																for="<?php echo $pagina['URL'] ?>"><?php echo $pagina['TITULO'] ?></label></td>
															<td class="check" style="text-align: center;"><input
																class="acesso acesso-<?php //echo $k;?> acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>"
																name="acesso[<?php //echo $pagina['ID_PAGINA'] ?>]"
																type="checkbox" value="<?php //echo $pagina['ID_PAGINA'] ?>"
																id="<?php //echo $sistema['info']['SIGLA_SISTEMA'].$pagina['URL'] ?>"
																
																onchange="allSelectedDir('<?php // echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)"></td>
														</tr>
										
								  <?php
                                                }
        							
                                  ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
					
						</div>
  		</div>
        <div class="tab-pane" id="profile" role="tabpanel">
               <div class="tab-pane active" id="home" role="tabpanel">
                      	<div class="col-xs-12 col-md-6 offset-md-3">
							<br>
						
							
							<p>
								<label for="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>">Selecionar todas as páginas</label> <input
									id="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox"
									onchange="selectAll(this.checked, '<?php //echo $sistema['info']['SIGLA_SISTEMA'] ?>')" />
							</p>
							
							<div class='panel panel-default'>
								<div class='panel-heading'>
									<div class='row'>
										<div class='col-md-12'>
											<strong><?php //echo $k;?></strong> <span
												class="pull-right caption"> <label
												for="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>">Selecionar todos deste
													diretório</label> <input id="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>"
												class="acesso acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox"
												onchange="selectAllByDir('<?php //echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)" />
											</span>
										</div>
									</div>
								</div>

								<div class='panel-body'>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">

												<table id="tabela-acessos"
													class="table table-responsible table-striped">
													<thead>

														<tr class="rcb">
															<th width="75%">Página</th>
															<th style="text-align: center;">Permitir</th>
														</tr>
													</thead>
													<tbody>
													
											<?php
							
                							foreach ($paginas['2'] as $pagina)
                							{
                                            ?>
                                            
                                            
														<tr class="rc">
															<td style="padding: 0;"><label
																style="margin: 0; padding: 8px; width: 100%; height: 100%; font-weight: 400;"
																for="<?php echo $pagina['URL'] ?>"><?php echo $pagina['TITULO'] ?></label></td>
															<td class="check" style="text-align: center;"><input
																class="acesso acesso-<?php //echo $k;?> acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>"
																name="acesso[<?php //echo $pagina['ID_PAGINA'] ?>]"
																type="checkbox" value="<?php //echo $pagina['ID_PAGINA'] ?>"
																id="<?php //echo $sistema['info']['SIGLA_SISTEMA'].$pagina['URL'] ?>"
																onchange="allSelectedDir('<?php // echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)"></td>
														</tr>
																	
								  <?php
                                            }
        							
                                  ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
  		</div>
        </div>
        <div class="tab-pane" id="messages" role="tabpanel">
			     <div class="tab-pane active" id="home" role="tabpanel">
                      	<div class="col-xs-12 col-md-6 offset-md-3">
							<br>
							<p>
								<label for="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>">Selecionar todas as páginas</label> <input
									id="selectall<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox" 
									onchange="selectAll(this.checked, '<?php //echo $sistema['info']['SIGLA_SISTEMA'] ?>')" />
							</p>
							
							<div class='panel panel-default'>
								<div class='panel-heading'>
									<div class='row'>
										<div class='col-md-12'>
											<strong><?php //echo $k;?></strong> <span
												class="pull-right caption"> <label
												for="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>">Selecionar todos deste
													diretório</label> <input id="selectdir<?php //echo $sistema['info']['SIGLA_SISTEMA'].$k?>"
												class="acesso acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']  ?>" type="checkbox" 
												onchange="selectAllByDir('<?php //echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)" />
											</span>
										</div>
									</div>
								</div>

								<div class='panel-body'>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">

												<table id="tabela-acessos"
													class="table table-responsible table-striped">
													<thead>

														<tr class="rcb">
															<th width="75%">Página</th>
															<th style="text-align: center;">Permitir</th>
														</tr>
													</thead>
													<tbody>
													
											<?php
							
                							foreach ($paginas['3'] as $pagina)
                							{
                                            ?>
                                            
                                            
														<tr class="rc">
															<td style="padding: 0;"><label
																style="margin: 0; padding: 8px; width: 100%; height: 100%; font-weight: 400;"
																for="<?php echo $pagina['URL'] ?>"><?php echo $pagina['TITULO'] ?></label></td>
															<td class="check" style="text-align: center;"><input
																class="acesso acesso-<?php //echo $k;?> acesso-<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>"
																name="acesso[<?php //echo $pagina['ID_PAGINA'] ?>]"
																type="checkbox" value="<?php //echo $pagina['ID_PAGINA'] ?>"
																id="<?php //echo $sistema['info']['SIGLA_SISTEMA'].$pagina['URL'] ?>"
																onchange="allSelectedDir('<?php // echo $k;?>','<?php //echo $sistema['info']['SIGLA_SISTEMA']; ?>', this.checked)"></td>
														</tr>
															
								  <?php
                                               }
        							
                                  ?>
                                  
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						</div>
  		</div>
        </div>
          
       </div>
        
        <br>
		
	</div>
		</div>
			<div class="row">
						<div class="col-md-3 offset-md-3">
							<button type="submit" name="submit"
								class="btn btn-primary btn-lg btn-block"
								style="font-size: 15px;">
								<i class="fa fa-check-square"></i> Salvar
							</button>
						</div>

						<div class="col-md-3">
							<a href="ver-perfis.php" class="btn btn-secondary btn-lg btn-block"
								style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
							</a>
						</div>
			</div>



		<!-- /.row (nested) -->
			
		</div>
	</div>
	<!-- /.panel-body -->
</form>

<?php 
include FOOTER;
?>