<?php 
require_once '../../config.php';
//verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
    global $perfis;
    $perfis = find_all('VIEW_PERFIS');
	
}


function view($id)
{
    if(isset($id))
    {
        global $perfil;
        $result = find_id('TBL_PERFIS','ID_PERFIL',$id);
        if(count($result)!=0)
        {
            $perfil = $result[0];
        }
        else
        {
            $_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
            $_SESSION['type'] = 'danger';
            header('Location: index.php');//
            exit;
        }
        
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
        
    }
}


function add()
{
    if(isset($_POST['submit']))
       {       
           $perfil = $_POST['perfil'];
           
           $all = find_all('VIEW_PERFIS');
           
           $already_exists=false;
           foreach ($all as $perfil_cad)
           {
               if(strtolower($perfil_cad['NOME_PERFIL'])==strtolower($perfil["'NOME_PERFIL'"]))
               {
                   $already_exists = true;
                   break;
               }
           }
           
           
           if($already_exists)
           {
               $_SESSION['type']= "danger";
               $_SESSION['message']= "Este Perfil Já Existe existe";
               
               global $paginas;
               $busca = find_all('TBL_PAGINAS');
               
               foreach($busca as $pagina)
               {
                   $str="";
                   $arr=array();
                   $arr = explode('/',$pagina['URL']);
                   if(count($arr)>1)
                   {
                       unset($arr[count($arr)-1]);
                       $str = implode('/', $arr);
                   }
                   else
                       $str = "/";
                       
                       $paginas[$str][] = $pagina;
                       
               }
               
           }
           else
           {
               save('TBL_PERFIS', $perfil);
               
               
               if($_SESSION['type']=="success")
               {
                   $res = find_id('TBL_PERFIS', 'NOME_PERFIL', $perfil["'NOME_PERFIL'"]);
                   $id = $res[0];
                   
                   $array= array();
                   foreach ($_POST['acesso'] as $acesso)
                   {
                       $array[] = array
                       (
                           'ID_PAGINA'=>$acesso,
                           'ID_PERFIL'=>$id['ID_PERFIL']
                       );
                   }
                   save_acessos($array);
                   
                   $_SESSION['message']= "Perfil adicionado com sucesso";
                   header('Location: index.php');
                   exit;
               }
               else
               {
                   global $paginas;
                   $busca = find_all('TBL_PAGINAS');
                   
                   foreach($busca as $pagina)
                   {
                       $str="";
                       $arr=array();
                       $arr = explode('/',$pagina['URL']);
                       if(count($arr)>1)
                       {
                           unset($arr[count($arr)-1]);
                           $str = implode('/', $arr);
                       }
                       else
                           $str = "/";
                           
                           $paginas[$str][] = $pagina;
                           
                   }
                   
               }
           }
           
    
    
       }
       
    global $paginas;
    $busca = find_all('TBL_PAGINAS');
    
    foreach($busca as $pagina)
    {
        $str="";
        $arr=array();
        $arr = explode('/',$pagina['URL']);
        if(count($arr)>1)
        {
            unset($arr[count($arr)-1]);
            $str = implode('/', $arr);
        }
        else
            $str = "/";
            
            $paginas[$str][] = $pagina;
            
    }
        
}





function edit($id)
{
    if(isset($id))
    {
        if(isset($_POST['submit']))
        {
            
            $perfil = $_POST['perfil'];
            
            $all = find_all('TBL_PERFIS');
            
            $already_exists=false;
            foreach ($all as $perfil_cad)
            {
                if(strtolower($perfil_cad['NOME_PERFIL'])==strtolower($perfil["'NOME_PERFIL'"]) && $perfil_cad['ID_PERFIL']!=$id)
                {
                    $already_exists = true;
                    break;
                }
            }
            
            if($already_exists)
            {
                $_SESSION['type']= "danger";
                $_SESSION['message']= "Este Perfil Já Existe existe";
                
            }
            else
            {
                
                update('TBL_PERFIS', $perfil, 'ID_PERFIL', $id);
                
                if($_SESSION['type']=="success")
                {
                    remove('TBL_ACESSOS', 'ID_PERFIL', $id);
                    $array= array();
                    foreach ($_POST['acesso'] as $acesso)
                    {
                        $array[] = array
                        (
                            'ID_PAGINA'=>$acesso,
                            'ID_PERFIL'=>$id
                        );
                        
                        
                    }
                    save_acessos($array);
                    $_SESSION['message']= "Perfil editado com sucesso";
                    header('Location: index.php');
                    exit;
                }
                else
                {
                    view($id);
                    global $paginas;
                    $busca = find_all('TBL_PAGINAS');
                    
                    foreach($busca as $pagina)
                    {
                        $str="";
                        $arr=array();
                        $arr = explode('/',$pagina['URL']);
                        if(count($arr)>1)
                        {
                            unset($arr[count($arr)-1]);
                            $str = implode('/', $arr);
                        }
                        else
                            $str = "/";
                            
                            $paginas[$str][] = $pagina;
                            
                    }
                }
            }
            
        }
        else
        {
            view($id);
            global $paginas;
            $busca = find_all('TBL_PAGINAS');
            
            foreach($busca as $pagina)
            {
                $str="";
                $arr=array();
                $arr = explode('/',$pagina['URL']);
                if(count($arr)>1)
                {
                    unset($arr[count($arr)-1]);
                    $str = implode('/', $arr);
                }
                else
                    $str = "/";
                    
                    $paginas[$str][] = $pagina;
                    
            }
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
    }
}






function delete($id)
{
    if(isset($id))
    {
        remove('TBL_PERFIS', 'ID_PERFIL', $id);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Perfil excluido com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}

?>