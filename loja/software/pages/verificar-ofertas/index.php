<?php 
require_once 'functions.php';
index();
include HEADER;

$verificar = (isset($menu_acessos['/pages/verificar-ofertas/view.php']) && $menu_acessos['/pages/verificar-ofertas/view.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Verificação das melhores ofertas</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Melhores ofertas</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Construtora</th>
                  <th>Nome da obra</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
               <?php 
                                foreach ($pedidos as $pedido)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo date('d/m/Y',strtotime($pedido['DATA_PEDIDO']))?></td>
										<td><?php echo $pedido['RAZAO_SOCIAL']?> </td>
										<td><?php echo $pedido['NOME_OBRA']?></td>
										
										<?php
										if($verificar)
											{
										?>
												<td class="text-center">
													<?php 
													 
													if($verificar)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Selecione o pedido para ver as melhores ofertas" href="view.php?id=<?php echo $pedido['ID_PEDIDO'];?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
													<?php 
														}
													?>
												   
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
	</div>
</div>


<!-- Modal -->



<?php 
include FOOTER;
?>