<?php 
require_once 'functions.php';
view($_GET['id']);
include HEADER;

$finalizar = (isset($menu_acessos['/pages/verificar-ofertas/finalizar.php']) && $menu_acessos['/pages/verificar-ofertas/finalizar.php']);

?>



<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Melhores orçamentos dos pedidos </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-12">
	
	
	<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Dados do pedido</div>
        <div class="card-body">
          <div class="row">
          <div class="col-md-6">
          <strong>Data do Pedido: </strong><?php 
          $date = new DateTime($pedidos['DATA_PEDIDO']);
          echo $date->format('d / m / Y');?>
          </div>
          <div class="col-md-6">
          <strong>Nome da obra: </strong><?php echo $pedidos['NOME_OBRA']?>
          </div>
          </div>
        </div>
      </div>
	
	
	<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Itens do pedido</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" style="border:1px solid #ccc" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Qtd solicitada</th>
                  <th>Preço unitário</th>
                  <th>Valor total</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
               <?php 
               /*
               echo '<pre>';
               print_r($itens);
               echo '</pre>';
               */
               foreach ($itens as $qq)
               {
                   foreach ($qq as $item)
								{
								    
								  /* echo '<pre>';
								   print_r($item);
								   echo '</pre>';*/
								   
								    
						?>
									<tr class="odd gradeX">
										<td><?php echo $item['NOME_PRODUTO']?> </td>
										<td><?php echo $item['ESPECIFICACAO']?></td>
										<td><?php echo $item['UM']?></td>   
										<td><?php echo $item['QTD_SOLICITADA']?></td> 
										<td> <?php if($item['VALOR_OFERTADO']==''){echo "Sem Oferta";}else{ echo "R$".$item['VALOR_OFERTADO'];}?></td>
										<td>R$: <?php echo $item['VALOR_OFERTADO']*$item['QTD_SOLICITADA']?></td>
										<td>
										
										<?php
										if($finalizar)
											{
										?>
										<button  type="button" data-toggle="modal" data-target="#exampleModal<?php echo $item['ID_ITEM_PEDIDO']?>" class="btn btn-<?php if($item['STATUS_OFERTA']=='F'){echo 'success';}else{echo 'primary';}?>" <?php if($item['STATUS_OFERTA']=='F'){echo 'disabled';}?>><t data-toggle="tooltip" data-placement="left" title="Aceite a proposto e aguarde o retorno da Obrix"><?php if($item['STATUS_OFERTA']=='F'){echo 'Finalizada';}else{echo 'Finalizar oferta';}?></t></button>
										<?php 
										}
										?>
										</td>
									</tr>
									
									
<div class="modal fade" id="exampleModal<?php echo $item['ID_ITEM_PEDIDO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deseja Realmente finalizar o pedido ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
                  <p><strong>Produto:</strong> <?php echo $item['NOME_PRODUTO']?> </p>
                  <p><strong>Especificação:</strong> <?php echo $item['ESPECIFICACAO']?></p>
                  <p><strong>UM:</strong> <?php echo $item['UM']?></p>
                  <p><strong>Qtd solicitada:</strong> <?php echo $item['QTD_SOLICITADA']?></p>
                  <p><strong>Preço unitário:</strong> <?php if($item['VALOR_OFERTADO']==''){echo "Sem Oferta";}else{ echo "R$".$item['VALOR_OFERTADO'];}?></p>
                  <p><strong>Valor total:</strong> R$ <?php echo $item['VALOR_OFERTADO']*$item['QTD_SOLICITADA']?></p>
      
      </div>
      <div class="modal-footer">
     <form method="post" action="envia_email.php">
    	<input type="hidden" name="email[EMAIL]" value="<?php echo $item['EMAIL']?>">
    	<input type="hidden" name="pedido[ID_PEDIDO]" value="<?php echo $item['ID_PEDIDO']?>">
    	<input type="hidden" name="itempedido[ID_ITEM_PEDIDO]" value="<?php echo $item['ID_ITEM_PEDIDO']?>">
    	<input type="hidden" name="orcamento[ID_ORCAMENTO]" value="<?php echo $item['ID_ORCAMENTO']?>">
        <button type="submit" class="btn btn-success"   id="btn_submit" name="submit" >Sim</button>
	</form>
	
        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
      </div>
    </div>
  </div>
</div>
									
						<?php 
								}
               }
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
      		<div class="row">
				<div class="col-md-4 offset-md-4 col-xs-6">
					<a href="<?php echo BASEURL?>pages/solicitar-precos/view.php?id=<?php echo $pedidos['ID_PEDIDO'] ?>" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-arrow-left" aria-hidden="true"></i>  Voltar
					</a>
				</div>
			</div>
		
		<br>
		
	</div>
</div>





<?php 
include FOOTER;
?>

