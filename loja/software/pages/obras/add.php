<?php 
require_once 'functions.php';
add();
include HEADER;
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de obras </span>
		</h1>
	</div>
</div>


<div class="row">
	<div class="col-md-10 offset-md-1 ">
		<form action="add.php" method="post">
		
	<fieldset>
	<legend>Informações</legend>
		<div class="row">
			<div class="col-md-4 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Data de início *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado uma data de início da obra"></i>
			
					<input type="date"
						class="form-control" id="campo_numero" name="obra[DATA_INICIO]"
						required oninput="verificar_numero();" />
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Data de término *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado uma data de previsão de término da obra"></i>
			
					<input type="date"
						class="form-control" id="campo_numero" name="obra[DATA_FIM]"
						required oninput="verificar_numero();" />
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Nome da obra  *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado um nome para a obra"></i>
			
					<input type="text"
						class="form-control" id="campo_numero" name="obra[NOME_OBRA]"
						required oninput="verificar_numero();" placeholder="Nome da Obra" />
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-4 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Tipo da obra  *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o tipo da obra"></i>
						<select class="form-control" name="obra[TIPO_OBRA]">
						
						<option value="Edificação">Edificação</option>
						<option value="Infraestrutura">Infraestrutura</option>
						<option value="Reforma">Reforma</option>
						<option value="Serviço Especializado">Serviço Especializado</option>
						<option value="Outros">Outros</option>
						</select>
					
				</div>
			</div>
		</div>
	</fieldset>
			<hr>
			
			<fieldset>
						<legend>Endereços</legend>
				
				<!-- ENDERECOS -->
					<div id="geral_enderecos">
					
					
						<div id="enderecos" >
							<input type="hidden" id="qtde_enderecos"
								value=1>
							<script>carregarEnderecos();</script>

						</div>
						
							
						<div class="form-group">
							<div class="col-md-3 col-sm-3 col-xs-6 ">


								<input type="button" class="btn" onclick="addEndereco()"
									value="Adicionar endereço">
							</div>



						</div>
					</div>
					
					
					<!-- ENDERECOS -->
				
			</fieldset>
			
			<fieldset>
	<legend>Descrição</legend>
		<div class="row">
			<div class="col-md-12 col-xs-12">
					<div id="div_campo_complemento" class="form-group has-feedback">
						<i class="" id="icon_campo_complemento"></i>
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Escreva uma breve descrição da obra"></i>
				
						<textarea class="form-control" name="obra[DESCRICAO]" placeholder="Descreva com poucas palavras a obra"></textarea>
					</div>
				</div>
		</div>
	</fieldset>
			
			<div class="row">
				<div class="offset-md-2 col-md-4 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-4 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
			<br>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

<script>
//cadastro de convenios
var resp_razao=false;
var resp_nome=false;
var resp_cnpj=false;
var resp_ie=false;
var resp_im=false;
var resp_cep=false;
var resp_numero=false;
var resp_contato=false;

function libera_botao()
{
	if(resp_razao && resp_nome && resp_cnpj && resp_ie && resp_im && resp_cep && resp_numero && resp_contato)
		document.getElementById('btn_submit').disabled = false;
	else
		document.getElementById('btn_submit').disabled = true;
}



function verificar_razao_social()
{
	var val = document.getElementById('campo_razao_social').value; 
	if(val!="")
	{
		$.ajax({url: "verificar-razao-social.php", type: "GET", data: "razao-social="+val, success: function(result){
			if(result=="ok")
			{
				document.getElementById('div_campo_razao_social').setAttribute("class", "form-group has-feedback has-success");
				document.getElementById('icon_campo_razao_social').setAttribute("class", "fa fa-check");
				document.getElementById('icon_campo_razao_social').setAttribute("style","color: green");
				document.getElementById('resposta_campo_razao_social').innerHTML = "";
				resp_razao=true;
				libera_botao();
			}
			else
			{
				document.getElementById('div_campo_razao_social').setAttribute("class", "form-group has-feedback has-error");
				document.getElementById('icon_campo_razao_social').setAttribute("class", "fa fa-times");
				document.getElementById('icon_campo_razao_social').setAttribute("style","color: red");
				document.getElementById('resposta_campo_razao_social').innerHTML = "Esta Razão social já foi cadastrada";
				resp_razao=false;
				libera_botao();
			}
			
		}});
	}
	else
	{
		document.getElementById('div_campo_razao_social').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_razao_social').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_razao_social').setAttribute("style","color: red");
		document.getElementById('resposta_campo_razao_social').innerHTML = "A razão social não pode ser vazia";
		resp_razao=false;
		libera_botao();
	}
}

function verificar_nome_fantasia()
{
	var val = document.getElementById('campo_nome_fantasia').value;
	if(val!="")
	{
		document.getElementById('div_campo_nome_fantasia').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_nome_fantasia').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_nome_fantasia').setAttribute("style","color: green");
		document.getElementById('resposta_campo_nome_fantasia').innerHTML = "";
		resp_nome=true;
	}
	else
	{
		document.getElementById('div_campo_nome_fantasia').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_nome_fantasia').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_nome_fantasia').setAttribute("style","color: red");
		document.getElementById('resposta_campo_nome_fantasia').innerHTML = "O nome fantasia não pode ser vazio";
		resp_nome=false;
	}
	libera_botao();
}

function validarCNPJ(cnpj) {
	 
    //cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

function maskCNPJ(cnpj)
{
	//cnpj = cnpj.replace(/[^\d]+/g,'');
	var arr= cnpj.split("");
	var string = "";
	for(i=0;i<arr.length;i++)
	{
		if(i==2 || i==5)
			string+=".";
		else if(i==8)
			string+="/";
		else if(i==12)
			string+="-";
		string+=arr[i];
		
	}
	return string;
}

function maskCEP(cep)
{
	//cnpj = cnpj.replace(/[^\d]+/g,'');
	var arr= cep.split("");
	var string = "";
	for(i=0;i<arr.length;i++)
	{
		if(i==2)
			string+=".";
		else if(i==5)
			string+="-";
		string+=arr[i];
		
	}
	return string;
}

function verificar_cnpj()
{
	var val = document.getElementById('campo_cnpj').value;
	val = val.replace(/[^\d]+/g,'');
	if(validarCNPJ(val))
	{
		$.ajax({url: "verificar-cnpj.php", type: "GET", data: "cnpj="+val, success: function(result){
			if(result=="ok")
			{
				document.getElementById('div_campo_cnpj').setAttribute("class", "form-group has-feedback has-success");
				document.getElementById('icon_campo_cnpj').setAttribute("class", "fa fa-check");
				document.getElementById('icon_campo_cnpj').setAttribute("style","color: green");
				document.getElementById('resposta_campo_cnpj').innerHTML = "";
				resp_cnpj=true;
			}
			else
			{
				document.getElementById('div_campo_cnpj').setAttribute("class", "form-group has-feedback has-error");
				document.getElementById('icon_campo_cnpj').setAttribute("class", "fa fa-times");
				document.getElementById('icon_campo_cnpj').setAttribute("style","color: red");
				document.getElementById('resposta_campo_cnpj').innerHTML = "Este CNPJ já foi cadastrado";
				resp_cnpj=false;
			}
			libera_botao();
		}});
		
	}
	else
	{
		document.getElementById('div_campo_cnpj').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_cnpj').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_cnpj').setAttribute("style","color: red");
		document.getElementById('resposta_campo_cnpj').innerHTML = "CNPJ Inválido";
		resp_cnpj=false;
	}
	libera_botao();
	document.getElementById('campo_cnpj').value = maskCNPJ(val);
}

function verificar_ie()
{
	var val = document.getElementById('campo_ie').value;
	val = val.replace(/[^\d]+/g,'');
	if(val!="")
	{
		document.getElementById('div_campo_ie').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_ie').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_ie').setAttribute("style","color: green");
		document.getElementById('resposta_campo_ie').innerHTML = "";
		resp_ie=true;
	}
	else
	{
		document.getElementById('div_campo_ie').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_ie').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_ie').setAttribute("style","color: red");
		document.getElementById('resposta_campo_ie').innerHTML = "A Inscrição Estadual não pode ser vazio";
		resp_ie=false;
	}
	document.getElementById('campo_ie').value = val;
	libera_botao();
}

function ieIsento(checked)
{
	if(checked)
	{
		document.getElementById('campo_ie').value = "ISENTO";
		document.getElementById('campo_ie').readOnly = true;
		document.getElementById('div_campo_ie').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_ie').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_ie').setAttribute("style","color: green");
		document.getElementById('resposta_campo_ie').innerHTML = "";
		resp_ie=true;
	}
	else
	{
		document.getElementById('campo_ie').value = "";
		document.getElementById('campo_ie').readOnly = false;
		document.getElementById('div_campo_ie').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_ie').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_ie').setAttribute("style","color: red");
		document.getElementById('resposta_campo_ie').innerHTML = "A Inscrição Municipal não pode ser vazio";
		resp_ie=false;
	}
	libera_botao();
}

function verificar_im()
{
	var val = document.getElementById('campo_im').value;
	val = val.replace(/[^\d]+/g,'');
	if(val!="")
	{
		document.getElementById('div_campo_im').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_im').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_im').setAttribute("style","color: green");
		document.getElementById('resposta_campo_im').innerHTML = "";
		resp_im=true;
	}
	else
	{
		document.getElementById('div_campo_im').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_im').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_im').setAttribute("style","color: red");
		document.getElementById('resposta_campo_im').innerHTML = "A Inscrição Municipal não pode ser vazio";
		resp_im=false;
	}
	document.getElementById('campo_im').value = val;
	libera_botao();
}

function imIsento(checked)
{
	if(checked)
	{
		document.getElementById('campo_im').value = "ISENTO";
		document.getElementById('campo_im').readOnly = true;
		document.getElementById('div_campo_im').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_im').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_im').setAttribute("style","color: green");
		document.getElementById('resposta_campo_im').innerHTML = "";
		resp_im=true;
	}
	else
	{
		document.getElementById('campo_im').value = "";
		document.getElementById('campo_im').readOnly = false;
		document.getElementById('div_campo_im').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_im').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_im').setAttribute("style","color: red");
		document.getElementById('resposta_campo_im').innerHTML = "A Inscrição Municipal não pode ser vazio";
		resp_im=false;
	}
	libera_botao();
}

function verificar_cep()
{
	var val = document.getElementById('campo_cep').value;
	val = val.replace(/[^\d]+/g,'');
	if(val.length==8)
	{
		$.getJSON("https://viacep.com.br/ws/"+ val +"/json/?callback=?", function(dados) {

            if (!("erro" in dados)) {
                //Atualiza os campos com os valores da consulta.
                $("#campo_logradouro").val(dados.logradouro);
                $("#campo_bairro").val(dados.bairro);
                $("#campo_municipio").val(dados.localidade);
                $("#campo_estado").val(dados.uf);

                document.getElementById('div_campo_cep').setAttribute("class", "form-group has-feedback has-success");
        		document.getElementById('icon_campo_cep').setAttribute("class", "fa fa-check");
        		document.getElementById('icon_campo_cep').setAttribute("style","color: green");
        		document.getElementById('resposta_campo_cep').innerHTML = "";
        		resp_cep=true;
                //$("#ibge").val(dados.ibge);
            } //end if.
            else {
                //CEP pesquisado não foi encontrado.
                //limpa_formulário_cep();
                $("#campo_logradouro").val("");
		        $("#campo_bairro").val("");
		        $("#campo_municipio").val("");
		        $("#campo_estado").val("");
            	document.getElementById('div_campo_cep').setAttribute("class", "form-group has-feedback has-error");
        		document.getElementById('icon_campo_cep').setAttribute("class", "fa fa-times");
        		document.getElementById('icon_campo_cep').setAttribute("style","color: red");
        		document.getElementById('resposta_campo_cep').innerHTML = "CEP não encontrado";
        		resp_cep=false;
            }
            libera_botao();
        });
	}
	else
	{
		$("#campo_logradouro").val("");
        $("#campo_bairro").val("");
        $("#campo_municipio").val("");
        $("#campo_estado").val("");
        document.getElementById('div_campo_cep').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_cep').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_cep').setAttribute("style","color: red");
		document.getElementById('resposta_campo_cep').innerHTML = "O CEP deve ser preenchido completamente";
		resp_cep=false;
		libera_botao();
	}
	document.getElementById('campo_cep').value = maskCEP(val);
}

function verificar_numero()
{
	var val = document.getElementById('campo_numero').value;
	if(val!="")
	{
		document.getElementById('div_campo_numero').setAttribute("class", "form-group has-feedback has-success");
		document.getElementById('icon_campo_numero').setAttribute("class", "fa fa-check");
		document.getElementById('icon_campo_numero').setAttribute("style","color: green");
		document.getElementById('resposta_campo_numero').innerHTML = "";
		resp_numero=true;
	}
	else
	{
		document.getElementById('div_campo_numero').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_numero').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_numero').setAttribute("style","color: red");
		document.getElementById('resposta_campo_numero').innerHTML = "O número não pode ser vazio";
		resp_numero=false;
	}
	libera_botao();
}

function maskTelefone(tel)
{
	//cnpj = cnpj.replace(/[^\d]+/g,'');
	var arr= tel.split("");
	var string = "";
	for(i=0;i<arr.length;i++)
	{
		if(i==0)
			string+="(";
		else if(i==2)
			string+=")";
		else if(i==6)
			string+="-";
		string+=arr[i];
		
	}
	return string;
}

function IsEmail(mail){
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string")
	{
		if(er.test(mail))
		{
			return true;
		}	
	}
	else if(typeof(mail) == "object")
	{
		if(er.test(mail.value))
		{
			return true;
		}
	}
	else
	{
		return false;		
	}
}

function troca_tipo()
{
	val = document.getElementById('campo_tipo_valor').value;
	if(val=="TELEFONE")
	{
		document.getElementById('campo_contato').type="text";
		document.getElementById('campo_contato').setAttribute("maxlength", "13");
		document.getElementById('campo_contato').placeholder="(00)0000-0000";
	}
	if(val=="EMAIL")
	{
		document.getElementById('campo_contato').type="email";
		document.getElementById('campo_contato').setAttribute("maxlength", "255");
		document.getElementById('campo_contato').placeholder="email@dominio.com";
	}
	document.getElementById('campo_contato').value = "";
	document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback");
	document.getElementById('icon_campo_contato').setAttribute("class", "");
	document.getElementById('icon_campo_contato').setAttribute("style","");
	document.getElementById('resposta_campo_contato').innerHTML = "";
	resp_contato=false;
}

function verificar_contato()
{
	var val = document.getElementById('campo_contato').value;
	if(val!="")
	{
		//var bool=false;
		if(document.getElementById('campo_tipo_valor').value=="TELEFONE")
		{
			val = val.replace(/[^\d]+/g,'');
			if(val.length==10)
			{
				document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback has-success");
				document.getElementById('icon_campo_contato').setAttribute("class", "fa fa-check");
				document.getElementById('icon_campo_contato').setAttribute("style","color: green");
				document.getElementById('resposta_campo_contato').innerHTML = "";
				resp_contato=true;
			}
			else
			{
				document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback has-error");
				document.getElementById('icon_campo_contato').setAttribute("class", "fa fa-times");
				document.getElementById('icon_campo_contato').setAttribute("style","color: red");
				document.getElementById('resposta_campo_contato').innerHTML = "Não é um telefone válido";
				resp_contato=false;
			}
			document.getElementById('campo_contato').value = maskTelefone(val);
		}
		else if(document.getElementById('campo_tipo_valor').value=="EMAIL")
		{
			if(IsEmail(val))
			{
				document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback has-success");
				document.getElementById('icon_campo_contato').setAttribute("class", "fa fa-check");
				document.getElementById('icon_campo_contato').setAttribute("style","color: green");
				document.getElementById('resposta_campo_contato').innerHTML = "";
				resp_contato=true;
			}
			else
			{
				document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback has-error");
				document.getElementById('icon_campo_contato').setAttribute("class", "fa fa-times");
				document.getElementById('icon_campo_contato').setAttribute("style","color: red");
				document.getElementById('resposta_campo_contato').innerHTML="Não é um e-mail válido";
				resp_contato=false;
			}
		}
		
	}
	else
	{
		document.getElementById('div_campo_contato').setAttribute("class", "form-group has-feedback has-error");
		document.getElementById('icon_campo_contato').setAttribute("class", "fa fa-times");
		document.getElementById('icon_campo_contato').setAttribute("style","color: red");
		document.getElementById('resposta_campo_contato').innerHTML = "O contato não pode ser vazio";
		resp_contato=false;
	}
	libera_botao();
	
}

//
</script>
<script><!-- >src="<?php BASEURL?>/enderecos.js.download"> --></script>
