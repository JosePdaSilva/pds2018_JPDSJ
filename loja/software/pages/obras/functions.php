<?php 

require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $obras;
	$obras = find_id('VIEW_OBRAS',"ID_CONSTRUTOR",$_SESSION["id_construtor"]);
}


function view($id)
{
	if(isset($id))
	{
		global $obra;
		$result = find_id('view_obras',['ID_OBRA','ID_CONSTRUTOR'],[$id,$_SESSION['id_construtor']]);
		if(count($result)==0)
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
		$obra = $result[0];
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}

function add()
{
    if(isset($_POST['submit']))
    {
        $obra = (isset($_POST['obra'])>0)?$_POST['obra']:array();
        
        //tratar cep e cnpj
        $enderecos=array();
        foreach ($_POST['endereco'] as $endereco)
        {
            $endereco['CEP'] = str_replace("-","",$endereco['CEP']);
            $enderecos[] = $endereco;
        }
        
        $obra['ID_CONSTRUTOR'] = $_SESSION["id_construtor"]; // depois precisa colocar i id do construtor em sessao
        
       $id= save_obra($obra, $enderecos);
        
       

        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Obra adicionada com sucesso";
            header('Location: view.php?id='.$id);
            exit;
        }
	  
  
    }
    else
    {
    }
}


function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
		    $obras = (isset($_POST['obra'])>0)?$_POST['obra']:array();
			
		    $enderecos=array();
		    if(count($_POST['enderecosDeletados']) > 0 ){
		    foreach ($_POST['enderecosDeletados'] as $endereco)
		    {
		        $enderecos[] = $endereco;
		        
		        remove('tbl_endereco_obra','ID_ENDERECO_OBRA',$endereco['ID_ENDERECO_OBRA']);
		        /*echo '<pre>';
		        print_r($endereco);
		        echo '</pre>';*/
		    }
		    }
		    
		    $enderecosNew=array();
		    foreach ($_POST['endereco'] as $enderecoNew)
		    {
		        $enderecoNew['CEP'] = str_replace("-","",$enderecoNew['CEP']);
		        $enderecosNew[] = $enderecoNew;
		        /*echo '<pre>';
		        print_r($enderecoNew);
		        echo '</pre>';*/
		    }
		    
		    
		    $enderecosUp=array();
		    foreach ($_POST['enderecoEdit'] as $enderecoUp)
		    {
		        $enderecoUp['CEP'] = str_replace("-","",$enderecoUp['CEP']);
		        $enderecosUp[] = $enderecoUp;
		      /* echo '<pre>';
		        print_r($enderecoUp);
		        echo '</pre>';*/
		        
		        update('tbl_enderecos',$enderecoUp,"ID_ENDERECO",$enderecoUp['ID_ENDERECO']);
		        
		    }
		    
		       edit_obra($obras,$id, $enderecosNew);
				    
				if($_SESSION['type']=="success")
				{
					$_SESSION['message']= "Obra alteradaz com sucesso";
					header('Location: index.php');
					exit;
			}
		}
		view($id);
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
    if(isset($id))
    {
        view($id);
        remove('TBL_OBRAS', 'ID_OBRA', $id);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Obra excluida com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}
?>