<?php 
require_once 'functions.php';
index();
include HEADER;

$ver = (isset($menu_acessos['/pages/obras/ver.php']) && $menu_acessos['/pages/obras/ver.php']);
$add = (isset($menu_acessos['/pages/obras/add.php']) && $menu_acessos['/pages/obras/add.php']);
$edit = (isset($menu_acessos['/pages/obras/edit.php']) && $menu_acessos['/pages/obras/edit.php']);
$delete = (isset($menu_acessos['/pages/obras/delete.php']) && $menu_acessos['/pages/obras/delete.php']);

?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Obras</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


		<div class="row">
			<div class="col-lg-1">
				
			</div>
		</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Obras cadastradas
          </div>
              <?php 
	   if($add)
	   {
	?>
          <div class="col-md-1">
        		<a data-toggle="tooltip" data-placement="left" title="Cadastrar uma nova obra" href="<?php echo BASEURL?>pages/obras/add.php"
					class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
					class="fa fa-plus"></i>
				</a>
          </div>
          <?php 
	   }
          ?>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome da obra</th>
                  <th>Tipo da obra</th>
                  <th>Data de início</th>
                  <th>Data de término</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                
                	 <?php 
							foreach ($obras as $obra)
								{
								    $enderecos = find_id("view_enderecos_obra","ID_OBRA",$obra['ID_OBRA']);
								    /*echo '<pre>';
								    print_r($enderecos);
								    echo '</pre>';*/
								    
								    $datai = new DateTime($obra['DATA_INICIO']);
								    $dataf = new DateTime($obra['DATA_FIM']);
						?>
									<tr class="odd gradeX">
										<td><?php echo $obra['NOME_OBRA']?></td>
										<td><?php echo $obra['TIPO_OBRA']?></td>
										<td><?php echo $datai->format('d/m/Y') ?></td>
										<td><?php echo $dataf->format('d/m/Y')?></td>
										
										<?php
											if($ver || $edit || $delete)
											{
											  
										?>
												<td class="text-center">
												<?php 
												if($ver)
												{
												?>
															<a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal<?php echo $obra['ID_OBRA']?>"><i data-toggle="tooltip" data-placement="left" title="Informações da obra" class="fa fa-info"></i></a>
													<?php 
											    }
														//if($edit)
														{
													?>
															<!-- <a data-toggle="tooltip" data-placement="left" title="Editar as informações da obra" href="edit.php?id=<?php echo $obra['ID_OBRA'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a> -->
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $obra['ID_OBRA']?>" data-id="<?php echo $obra['ID_OBRA']?>" data-obra="<?php echo $obra['NOME_OBRA']?>" data-tipo='OBRA'><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="left" title="Exclua a obra selecionada" ></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
									
									
																		<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $obra['ID_OBRA']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações da obra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p><strong>Nome da obra:</strong> <?php echo $obra['NOME_OBRA']?> </p>
      	<p><strong>Tipo da obra:</strong> <?php echo $obra['TIPO_OBRA']?> </p>
      	<p><strong>Data de início:</strong> <?php echo $datai->format('d/m/Y')?></p>
      	<p><strong>Data de término:</strong> <?php echo $dataf->format('d/m/Y')?></p>
      	<p><strong>Descrição:</strong> <?php echo $obra['DESCRICAO']?></p>
      	<hr>
      	<h6><strong>Endereço</strong></h6>
      	<div id="accordion">
<?php 	
foreach ($enderecos as $endereco)
{
  // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO_ENDERECO'];echo $obra['NOME_OBRA']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO_ENDERECO'];echo $obra['NOME_OBRA']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO_ENDERECO'];echo $obra['NOME_OBRA']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $endereco['DESCRICAO_ENDERECO']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO_ENDERECO'];echo $obra['NOME_OBRA']?>" class="collapse" aria-labelledby="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO_ENDERECO'];echo $obra['NOME_OBRA']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong>CEP: </strong><?php echo $endereco['CEP']?></p>
      	<p><strong>Cidade:</strong> <?php echo $endereco['MUNICIPIO']?></p>
      	<p><strong>Logradouro:</strong> <?php echo $endereco['LOGRADOURO']?></p>
      	<p><strong>Número:</strong> <?php echo $endereco['NUMERO']?></p>
      	<p><strong>Bairro:</strong> <?php echo $endereco['BAIRRO']?></p>
      	<p><strong>Estado:</strong> <?php echo $endereco['ESTADO']?></p>
      	<p><strong>Complemento:</strong> <?php if($endereco['COMPLEMENTO']==""){ echo "-";}else{echo $endereco['COMPLEMENTO'];}?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>
      
      </div>
      <div class="modal-footer">
      	<a href="view.php?id=<?php echo $obra['ID_OBRA']?>" class="btn btn-primary"><i class="fa fa-plus"></i> Informações</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
						<?php 
								}
						?>
                
                
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>