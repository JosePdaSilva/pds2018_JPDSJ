<?php 
require_once 'functions.php';
index();
include HEADER;

?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Pedidos realizados</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


		<div class="row">
			<div class="col-lg-1">
				
			</div>
		</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
        	<div class="col-md-12">	
          		<i class="fa fa-table"></i> Pedidos realizados no sistema
          	</div>
          </div>
         </div>
          
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Email</th>
                  <th>Tell</th>
                  <th>Roupa</th>
                  <th>Marca</th>
                  <th>Tamanhos</th>
                  <th>Qtd</th>
                </tr>
              </thead>
 			  <tbody>
               
               <?php 
							foreach ($vendas as $venda)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $venda['NOME']?></td>
										<td><?php echo $venda['EMAIL']?></td>
										<td><?php echo $venda['TELEFONE'] ?></td>
										<td><?php echo $venda['DESCRICAO']?></td>
										<td><?php echo $venda['MARCA']?></td>
										<td><?php echo $venda['TAMANHOS'] ?></td>
										<td><?php echo $venda['QUANTIDADE'] ?></td>		
									</tr>

						<?php 
								}
						?>
                
              </tbody>
              
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>