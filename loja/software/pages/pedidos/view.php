<?php 
require_once 'functions.php';
view($_GET['id']);
include HEADER;

$enderecos = find_id('view_enderecos_obra','ID_OBRA',$_GET['id']);

$findPedido = find_id('view_orcamentos',['ID_OBRA', 'ENTREGUE'],[$_GET['id'], 'E']);

$valorPedido=array();
$somaTotal=0;
$valorPorSetor = array();

$res = find_all('TBL_SETOR');

foreach ($res as $setor)
{
    $valorPorSetor[$setor['SETOR']]['VALOR'] = 0;
    $valorPorSetor[$setor['SETOR']]['COR'] = $setor['COR_GRAFICO'];
}

if(count($findPedido)>0)
{
    foreach ($findPedido as $pedido)
    {
        $data = new DateTime($pedido['DATA_PEDIDO']);
        if(isset($valorPedido[$data->format('d/m/Y')]))
            $valorPedido[$data->format('d/m/Y')] += $pedido['QTD_SOLICITADA']*$pedido['VALOR_OFERTADO'];
        else 
            $valorPedido[$data->format('d/m/Y')] = $pedido['QTD_SOLICITADA']*$pedido['VALOR_OFERTADO'];
        
        $somaTotal+=$pedido['QTD_SOLICITADA']*$pedido['VALOR_OFERTADO'];
        
        $valorPorSetor[$pedido['SETOR']]['VALOR']+=$pedido['QTD_SOLICITADA']*$pedido['VALOR_OFERTADO'];
    }
}





?> 

<script type="text/javascript">
atual_end = <?php echo count($enderecos);?>
</script>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Informações da Obra </span>
		</h1>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card mb-3">
        <div class="card-header">
        <div class="row">
        	<div class="col-md-10">
          <i class="fa fa-table"></i> Informações da obra
          </div>
          <div class="col-md-2">
          <a href="edit.php?id=<?php echo $obra['ID_OBRA'];?>" class="btn btn-warning"><i data-toggle="tooltip" data-placement="left" title="Editar as informações da obra" class="fa fa-edit"></i></a>
				<a href="<?php echo BASEURL?>pages/solicitar-precos/add.php" class="btn btn-success"><i data-toggle="tooltip" data-placement="left" title="Solicitar preços" class="fa fa-shopping-cart"></i></a>
          </div>
          </div>
         </div>
        <div class="card-body">
		<div class="row">
			<div class="col-md-3">
				<label><strong>Data de início</strong></label><br>
				<?php $date= new DateTime($obra['DATA_INICIO']); echo $date->format('d/m/Y');?>
			</div>
			<div class="col-md-3">
				<label><strong>Data de término</strong></label><br>
				<?php $date2= new DateTime($obra['DATA_FIM']); echo $date2->format('d/m/Y');?>
			</div>
			<div class="col-md-6">
				<label><strong>Nome da obra</strong></label><br>
				<?php echo $obra['NOME_OBRA']?>
			</div>
			
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<label><strong>Tipo da obra</strong></label><br>
				<?php echo "Tipo da obra xD"?>
			
			</div>
			<div class="col-md-9">
			<label><strong>Descrição</strong></label><br>
				<?php echo $obra['DESCRICAO']?>
			</div>
		</div>
		</div>
		</div>
		
		<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Endereços da obra</div>
        <div class="card-body">
		<?php 
		foreach ($enderecos as $end){
		?>
		
		<div class="row">
			<div class="col-md-3">
				<label><strong>Descrição end.</strong></label><br>
				<?php echo $end['DESCRICAO_ENDERECO']?>
			</div>
			<div class="col-md-2">
				<label><strong>CEP</strong></label><br>
				<?php echo mask($end['CEP'],'#####-###');?>
			</div>
			<div class="col-md-5">
				<label><strong>Logradouro</strong></label><br>
				<?php echo $end['LOGRADOURO']?>
			</div>
			<div class="col-md-2">
				<label><strong>Número</strong></label><br>
				<?php echo $end['NUMERO']?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<label><strong>Complemento</strong></label><br>
				<?php if($end['COMPLEMENTO']=="")echo '-';else{ echo $end['COMPLEMNTO'];}?>
			</div>
			<div class="col-md-4">
				<label><strong>Bairro</strong></label><br>
				<?php echo $end['BAIRRO']?>
			</div>
			<div class="col-md-2">
				<label><strong>Cidade</strong></label><br>
				<?php echo $end['MUNICIPIO']?>			
			</div>
			<div class="col-md-2">
				<label><strong>Estado</strong></label><br>
				<?php echo $end['ESTADO']?>
			</div>
		</div>
		
		<hr>
		<?php 
		}
		?>
		</div>
		</div>
		
		
		<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Orçamentos - <strong>Total:</strong> R$ <?php echo number_format($somaTotal,2,',','.');?></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" style="border:1px solid #ccc" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Data do pedido</th>
                  <th>Valor do pedido</th>
                </tr>
              </thead>
              <tbody>
              
               <?php 
               /*
               echo '<pre>';
               print_r($itens);
               echo '</pre>';
               */
              // foreach ($itens as $qq)
               {
                   foreach ($valorPedido as $data => $valor)
								{
								  /* echo '<pre>';
								   print_r($item);
								   echo '</pre>';*/
								   
								    
						?>
									<tr class="odd gradeX">
										<td><?php echo $data;?> </td>
										<td>R$: <?php echo $valor;?></td>
									</tr>
									
						<?php 
								}
               }
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
      <?php 
          $labels = "";
          $valores = "";
          $cores = "";
          $first = true;
          foreach ($valorPorSetor as $k=>$v)
          {
              if($v['VALOR']>0)
              {
                  if($first)
                  {
                      $labels="'".$k."'";
                      $valores="".$v['VALOR']."";
                      $cores = "'".$v['COR']."'";
                      
                      $first = false;
                  }
                  else
                  {
                      $labels.=", '".$k."'";
                      $valores=", ".$v['VALOR']."";
                      $cores = ", '".$v['COR']."'";
                  }
              }
          }
      ?>
      
      <div class="row">
      <div class="col-md-12">
          <!-- Example Pie Chart Card-->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fa fa-pie-chart"></i> Gasto por setor</div>
            <div class="card-body">
            <div class="col-md-4 offset-md-4">
            	<?php 
            	   if($first)
            	   {
            	?>
            		<p>Sem registro para a geração do gráfico</p>
            	<?php 
            	   }
            	   else
            	   {
            	?>
              <canvas id="graficoPizza" width="100%" height="100"></canvas>
              	<?php
            	   }
              	?>
            </div>
            </div>
          </div>
        </div>
        </div>
      
			<div class="row">

				<div class="col-md-4 col-xs-6 offset-md-4" >
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-arrow-left"></i> Voltar
					</a>
				</div>
			</div>
			
			<br>	
		
	</div>
</div>


<?php 
include FOOTER;

?>


<script>
var ctx = document.getElementById("graficoPizza");
var myPieChart = new Chart(ctx, {
	  type: 'pie',
	  data: {
	    labels: [<?php echo $labels?>],
	    datasets: [{
	      data: [<?php echo $valores?>],
	      backgroundColor: [<?php echo $cores;?>],
	    }],
	  },
	});</script>
