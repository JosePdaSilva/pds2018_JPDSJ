<?php
require_once 'functions.php';
edit($_GET['id']);
include HEADER;


?>

<script type="text/javascript">
atual_end = <?php echo count($enderecos);?>

</script>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Editar Obra </span>
		</h1>
	</div>
</div>


<div class="row">
	<div class="col-md-6 offset-md-3 ">
		<form enctype="multipart/form-data" action="edit.php?id=<?php echo $_GET['id'];?>" method="post" >
		
		<div class="text-center">
        <img src="data:image/png;base64,<?php echo base64_encode($obra['FOTO']);?>" alt="FOTO DA ROUPA" style="height: 150px"><br>
        <p>Foto cadastrada</p>
      </div>
		
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Selecionar outra foto*</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser selecionado um arquivo contendo a foto da roupa em formaato .PNG"></i>
			
					<input type="file"
						class="form-control" id="campo_numero" name="foto"
						oninput="verificar_numero();" value="<?php base64_encode($obra['FOTO'])?>"/>
				</div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Descrição *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado uma descrição para a roupa"></i>
						<textarea class="form-control" name="roupa[DESCRICAO]" placeholder="Descreva as informações da roupa"><?php echo $obra['DESCRICAO']?></textarea>
				</div>
			</div>
		</div>
		
			<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Marca *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o marca da roupa"></i>
			
					<input type="text"
						class="form-control" id="campo_numero" name="roupa[MARCA]"
						required oninput="verificar_numero();" value="<?php echo $obra['MARCA']?>" placeholder="Marca da roupa"/>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Público  *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o público alvo da roupa"></i>
			<br>	
			<div class="row">
					<div class="col-md-2 col-xs-12">
					<input type="radio" name="roupa[SEXO]" <?php if($obra['SEXO']=="M")echo 'checked'?>value="M"> M
					</div>
					<div class="col-md-2 col-xs-12">
  					<input type="radio" name="roupa[SEXO]" <?php if($obra['SEXO']=="F")echo 'checked'?> value="F"> F
  					</div>
  					<div class="col-md-3 col-xs-12">
 					<input type="radio" name="roupa[SEXO]" <?php if($obra['SEXO']=="U")echo 'checked'?> value="U"> Unissex
 					</div>
 			</div>
 					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Tamanhos *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado os tamanhos disponíveis"></i>
			
					<input type="text"
						class="form-control" id="campo_numero" name="roupa[TAMANHOS]"
						required oninput="verificar_numero();" value="<?php echo $obra['TAMANHOS']?>" placeholder="Tamanhos disponíveis"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Preço Unitário *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o preço unitário do produto"></i>
			
					<input type="number" min="0.01" step="0.01"
						class="form-control" id="campo_numero" name="roupa[PRECO_UNITARIO]"
						required oninput="verificar_numero();" value="<?php echo $obra['PRECO_UNITARIO']?>" placeholder="Preço unitário da peça"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<input type="checkbox" name="principal" <?php if($obra['PRINCIPAL']=='S') echo 'checked';?> > Mostrar na tela principal		
			</div>
		</div>
			
	
			
			<br>
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Salvar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
			<br>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

