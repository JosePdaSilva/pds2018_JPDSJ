<?php 

require_once '../../config.php';
//verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $vendas;
	$vendas = find_all('VIEW_VENDAS');
}


function view($id)
{
	if(isset($id))
	{
		global $obra;
		$result = find_id('tbl_produtos','ID_PRODUTO',$id);
		if(count($result)==0)
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
		$obra = $result[0];
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}

function add()
{
    if(isset($_POST['submit']))
    {
        $roupa = $_POST['roupa'];
        
        if(isset($_POST['principal']))
        	$roupa['PRINCIPAL'] = 'S';
        	else $roupa['PRINCIPAL'] ='N';
        	
        if($_FILES['foto']['tmp_name']!="")
        {
            $file= $_FILES['foto'];
            $tamanho= $file['size'];
            
            if($tamanho > 10485760)
            {
                $_SESSION['message'] = "Roupa cadastrada com sucesso, por�m a foto � maior do que o permitido, edit a roupa para cadastrar uma foto desejada.";
                $_SESSION['type'] = 'danger';
            }
            else
            {
                $roupa['FOTO'] = file_get_contents($file['tmp_name']);
            }
        }
        
        $id= save('TBL_PRODUTOS', $roupa);
        
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Roupa adicionada com sucesso";
            header('Location: index.php');
            exit;
        }
        else{
            $_SESSION['message'] = "Não foi possível adicionar uma roupa";
            $_SESSION['type'] = 'danger';
            header('Location: index.php');//
            exit;
        }
	  
  
    }
    else
    {
    }
}


function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			$roupa = $_POST['roupa'];
			
			if(isset($_POST['principal']))
				$roupa['PRINCIPAL'] = 'S';
				else $roupa['PRINCIPAL'] ='N';
			
			if($_FILES['foto']['tmp_name']!="")
			{
				$file= $_FILES['foto'];
				$tamanho= $file['size'];
				
				if($tamanho > 10485760)
				{
					$_SESSION['message'] = "Roupa cadastrada com sucesso, por�m a foto � maior do que o permitido, edit a roupa para cadastrar uma foto desejada.";
					$_SESSION['type'] = 'danger';
				}
				else
				{
					$roupa['FOTO'] = file_get_contents($file['tmp_name']);
				}
			}
		    
		       update('TBL_PRODUTOS',$roupa,'ID_PRODUTO', $id);
				    
				if($_SESSION['type']=="success")
				{
					$_SESSION['message']= "Roupa editada com sucesso";
					header('Location: index.php');
					exit;
			}
		}
		view($id);
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
    if(isset($id))
    {
        view($id);
        remove('TBL_PRODUTOS', 'ID_PRODUTO', $id);
        if($_SESSION['type']=="success")
        {
            $_SESSION['message']= "Roupa excluida com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}
?>