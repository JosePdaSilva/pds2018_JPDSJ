<?php 
require_once 'functions.php';
add();
include HEADER;
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de Roupas </span>
		</h1>
	</div>
</div>


<div class="row">
	<div class="col-md-6 offset-md-3 ">
		<form action="add.php" method="post" enctype="multipart/form-data">
		
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Foto da roupa*</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser selecionado um arquivo contendo a foto da roupa"></i>
			
					<input type="file"
						class="form-control" id="campo_numero" name="foto"
						required oninput="verificar_numero();" />
				</div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Descrição *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado uma descrição para a roupa"></i>
						<textarea class="form-control" name="roupa[DESCRICAO]" placeholder="Descreva as informações da roupa"></textarea>
				</div>
			</div>
		</div>
		
			<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Marca *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o marca da roupa"></i>
			
					<input type="text"
						class="form-control" id="campo_numero" name="roupa[MARCA]"
						required oninput="verificar_numero();" placeholder="Marca da roupa"/>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Publico  *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado um nome para a obra"></i>
			<br>	
			<div class="row">
					<div class="col-md-2 col-xs-12">
					<input type="radio" name="roupa[SEXO]" value="M"> M
					</div>
					<div class="col-md-2 col-xs-12">
  					<input type="radio" name="roupa[SEXO]" value="F"> F
  					</div>
  					<div class="col-md-3 col-xs-12">
 					<input type="radio" name="roupa[SEXO]" value="U"> Unissex
 					</div>
 			</div>
 					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Tamanhos *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado os tamanhos disponíveis"></i>
			
					<input type="text"
						class="form-control" id="campo_numero" name="roupa[TAMANHOS]"
						required oninput="verificar_numero();" placeholder="Tamanhos disponíveis"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="div_campo_numero" class="form-group has-feedback">
					<label for="campo_numero">Preço Unitário *</label>
					<i class="fa fa-1-5x fa-question-circle-o pull-right"
						data-toggle="tooltip" data-placement="left"
						title="Deve ser informado o preço unitário do produto"></i>
			
					<input type="number" min="0.01" step="0.01"
						class="form-control" id="campo_numero" name="roupa[PRECO_UNITARIO]"
						required oninput="verificar_numero();" placeholder="Tamanhos disponíveis"/>
				</div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<input type="checkbox" name="principal"> Mostrar na tela principal		
			</div>
		</div>
	
			
			<br>
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
			<br>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

<script><!-- >src="<?php BASEURL?>/enderecos.js.download"> --></script>
