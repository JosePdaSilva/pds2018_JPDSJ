<?php 
require_once 'functions.php';
edit();
include HEADER;

$enderecos = find_id("view_enderecos_construtora","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$contatos = find_id("view_contatos_construtora","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$construtora = find_id("view_construtores","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$user = find_id("view_usuarios_login","ID_CONSTRUTOR",$_SESSION["id_construtor"]);

/*echo '<pre>';
print_r($contatos);
echo '</pre>';*/

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}


?>

<script type="text/javascript">
atual_end = <?php echo count($enderecos);?>
</script>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Informações da construtora</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
		
<div class="row">
	<div class="col-lg-12">
        
		<form action="edit.php" method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">  
                <label for="exampleInputName">Razão social</label>
                <input class="form-control" disabled value="<?php echo $construtora[0]['RAZAO_SOCIAL']?>" id="nome" name="empresa[NOME_RESPONSAVEL]" type="text" aria-describedby="nameHelp" placeholder="Nome do Representante *">
              </div>
              <div class="col-md-6">  
                <label for="exampleInputName">Nome do representante</label>
                <input class="form-control" disabled value="<?php echo $construtora[0]['NOME_RESPONSAVEL']?>" id="razao_social" name="empresa[RAZAO_SOCIAL]" type="text" aria-describedby="nameHelp" placeholder="Razão Social *">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              	<div class="col-md-6">  
                	<label for="exampleInputEmail1">CNPJ</label>
                	<input class="form-control" disabled value="<?php echo  mask($construtora[0]['CNPJ'],'##.###.###/####-##');?>" id="cnpj" type="text" OnKeyPress="formatar('##.###.###/####-##', this)" maxlength="18" name="empresa[CNPJ]" aria-describedby="emailHelp" placeholder="CNPJ *">
              </div>
              <div class="col-md-6">  
                	<label for="exampleInputEmail1">E-mail</label>
                	<input class="form-control" disabled value="<?php echo  $construtora[0]['EMAIL'];?>" id="cnpj" type="text" OnKeyPress="formatar('##.###.###/####-##', this)" maxlength="18" name="empresa[CNPJ]" aria-describedby="emailHelp" placeholder="CNPJ *">
              </div>
           	</div>
          </div>
          
     <h4>Endereços</h4>
          		<div id="enderecosAntigos">
          		<hr>
          		<?php 
          		$qq = 0;
          		
          		function Maskx($mask,$str){
          		    
          		    $str = str_replace(" ","",$str);
          		    
          		    for($i=0;$i<strlen($str);$i++){
          		        $mask[strpos($mask,"#")] = $str[$i];
          		    }
          		    
          		    return $mask;
          		    
          		}
          		
          		foreach ($enderecos as $endereco)
						{
						    
						   $xd =  Maskx("#####-###",$endereco['CEP']);
					    ?>
					    
					    
						
						
						<div id="enderecoAntigo<?php echo $qq?>">
						<input  type="hidden" name="enderecoEdit[<?php echo $qq?>][ID_ENDERECO]" value="<?php echo $endereco['ID_ENDERECO']?>">
						
						<div class="form-group">
						<div class="form-row">
						
						<div class="col-md-2 col-sm-4 col-xs-6 ">Descrição
						<input value="<?php echo $endereco['DESCRICAO']?>"  name="enderecoEdit[<?php echo $qq?>][DESCRICAO]" id="numero<?php echo $qq?>" type="text" placeholder="Descrição End." class="form-control">
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6 ">CEP
						<input value="<?php echo $xd?>" name="enderecoEdit[<?php echo $qq?>][CEP]" id="cep<?php echo $qq?>" type="text" placeholder="somente numeros" onkeypress="formatar('#####-###', this)" oninput="buscarCEP(<?php echo $qq?>)" maxlength="9" class="form-control" required="">
						</div>
						<div class="col-md-6 col-sm-4 col-xs-6 ">Logradouro
						<input value="<?php echo $endereco['LOGRADOURO']?>"   name="enderecoEdit[<?php echo $qq?>][LOGRADOURO]" id="logradouro<?php echo $qq?>" type="text" placeholder="Logradouro" class="form-control" required="">
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6 ">Número
						<input value="<?php echo $endereco['NUMERO']?>"  name="enderecoEdit[<?php echo $qq?>][NUMERO]" id="numero<?php echo $qq?>" type="text" placeholder="Número" class="form-control">
						</div>
						
						
						</div>
						</div>
						
						<div class="form-group">
						<div class="form-row">
						<div class="col-md-3 col-sm-3 col-xs-6 ">Complemento
						<input value="<?php echo $endereco['COMPLEMENTO']?>"  name="enderecoEdit[<?php echo $qq?>][COMPLEMENTO]" id="complemento<?php echo $qq?>" type="text" placeholder="Complemento" class="form-control">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6 ">Bairro
						<input value="<?php echo $endereco['BAIRRO']?>"  name="enderecoEdit[<?php echo $qq?>][BAIRRO]" id="bairro<?php echo $qq?>" type="text" placeholder="Bairro" class="form-control" required="">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6 ">Cidade
						<input value="<?php echo $endereco['MUNICIPIO']?>"  name="enderecoEdit[<?php echo $qq?>][MUNICIPIO]" id="cidade<?php echo $qq?>" type="text" placeholder="Cidade" class="form-control" required="">
						</div><div class="col-md-2 col-sm-3 col-xs-6 ">Estado
						<input value="<?php echo $endereco['ESTADO']?>"  name="enderecoEdit[<?php echo $qq?>][ESTADO]" id="estado<?php echo $qq?>" type="text" placeholder="Estado" class="form-control" required="">
						</div>
						<?php if($qq > 0) {?>
						<div class="col-md-1 col-sm-1 col-xs-1"><br>
						<a style="color:white" onclick="deleteEnderecoAntigo('enderecoAntigo<?php echo $qq?>',<?php echo $endereco['ID_ENDERECO_EMPRESA']?>,'ID_ENDERECO_EMPRESA'  )" class="btn btn-sm btn-danger" >X</a>
						</div>
						<?php }?>
						</div>
						</div>
						</div>
						
						<hr>
						<?php 
						$qq++;
						}
						?>
						</div>
						
						<div id="tabelaDeletados" hidden></div>
						
						
					<!-- ENDERECOS -->
					<div id="geral_enderecos">
						<div class="form-group">
							<div class="col-md-3 col-sm-3 col-xs-6 ">


								<input type="button" class="btn" onclick="addEndereco()"
									value="Adicionar endereco">
							</div>



						</div>
						<div id="enderecos" >
							<input type="hidden" id=qtde_enderecos
								value=1>

						</div>
					</div>
					
					<!-- ENDERECOS -->
					<hr>
					
					<h4>Contatos</h4>
					
          		<div id="contatosAntigos">
					<?php 
                  		$qq = 0;
                  		foreach ($contatos as $contato)
						{
					    ?>
						
						
						<div id="contatoAntigo<?php echo $qq?>">
						<input  type="hidden" name="contatoEdit[<?php echo $qq?>][ID_CONTATO]" value="<?php echo $contato['ID_CONTATO']?>">
						
						<div class="form-group">
						<div class="form-row">
						
						<div class="col-md-2 col-sm-4 col-xs-6 ">Tipo do contato
						<select name="contatoEdit[<?php echo $qq?>][TIPO_VALOR]" id="opcao<?php echo $qq?>" onchange="updatedContato(this,'valor<?php echo $qq?>');" class="form-control">
    						<option value="TELEFONE" <?php if($contato['TIPO_VALOR']=="TELEFONE"){echo "Selected";}?>>Telefone</option>
    						<option value="CELULAR" <?php if($contato['TIPO_VALOR']=="TELEFONE"){echo "Selected";}?>>Celular</option>
						</select>
						
						<!--  <input value="<?php// echo $contato['TIPO_VALOR']?>"  name="contatoEdit[<?php //echo $qq?>][TIPO_VALOR]" id="tipovalor<?php //echo $qq?>" type="text" placeholder="Descrição End." class="form-control">-->
						</div>
						<div class="col-md-2 col-sm-4 col-xs-6 ">Valor
						<input value="<?php echo $contato['VALOR']?>" placeholder="00-000000000" onkeypress="formatar('##-#########', this)" maxlength="12" name="contatoEdit[<?php echo $qq?>][VALOR]" id="valor<?php echo $qq?>" type="text" class="form-control" required="">
						</div>
						
						<?php if($qq > 0) {?>
						<div class="col-md-1 col-sm-1 col-xs-1"><br>
						<a  style="color:white" onclick="deleteContatoAntigo('contatoAntigo<?php echo $qq?>',<?php echo $contato['ID_CONTATO_EMPRESA']?>)" class="btn btn-sm btn-danger" >X</a>
						</div>
						<?php }?>
						
						</div>
						</div>
						</div>
						
						<hr>
						<?php 
						$qq++;
						}
						?>
					</div>
				
				
						<div id="tabelaDeletados2" hidden></div>
						
						
					<div class="ln_solid"></div>
					
					<!-- CONTATOS -->
					<div id="geral_contatos">
						<div class="form-group">
							<div class="col-md-3 col-sm-3 col-xs-6 ">


								<input type="button" class="btn" onclick="addContato()"
									value="Adicionar contato">
							</div>



						</div>
						<div id="contatos">
							<input type="hidden" id=qtde_contatos
								value=1>

						</div>
					</div>
					<!-- CONTATOS -->

					<!-- ********************************************************************** EDITAR CONTATOS *****************************************************************************************************************************************************-->
					
          		<hr>
   
          
         <div class="row">
				<div class="offset-md-2 col-md-4 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Salvar
					</button>
				</div>

				<div class="col-md-4 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
			<br>
          
        </form>
        
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>