<?php 
require_once 'functions.php';
index();
include HEADER;

$enderecos = find_id("view_enderecos_construtora","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$contatos = find_id("view_contatos_construtora","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$construtora = find_id("view_construtores","ID_CONSTRUTOR",$_SESSION["id_construtor"]);
$user = find_id("view_usuarios_login","ID_CONSTRUTOR",$_SESSION["id_construtor"]);

$edit = (isset($menu_acessos['/pages/construtor/edit.php']) && $menu_acessos['/pages/construtor/edit.php']);


function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}


?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Informações da construtora</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
        <div class="card mb-3">
           <div class="card-header">
           
           <div class="row">
           <div class="col-md-11">
           <i class="fa fa-wrench"></i> Dados da construtora
           </div>
               <?php 
	   if($edit)
	   {
	?>
           <div class="col-md-1">
           <a href="<?php echo BASEURL?>pages/construtor/edit.php"
    		class="btn btn-warning btn-sm btn-block" style="font-size: 15px;"> <i
    		class="fa fa-edit"></i>
    	   </a>
           </div>
           <?php 
	   }
           ?>
           </div>
           </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                    <h3><?php echo $construtora[0]['RAZAO_SOCIAL']?></h3>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                    <strong>Nome do representante: </strong><?php echo $construtora[0]['NOME_RESPONSAVEL']?>
                    </div>
                    <div class="col-md-6">
                    <strong>CNPJ: </strong><?php echo mask($construtora[0]['CNPJ'],'##.###.###/####-##');?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                    <strong>E-mail: </strong><?php echo $construtora[0]['EMAIL'];?>
                    </div>
                </div>
                
            </div>
        </div>	 
        
        <div class="card mb-3"><div class="card-header"><i class="fa fa-map-marker"></i> Endereços</div>
            <?php 
            foreach($enderecos as $endereco)
            {
            ?>
            <div class="card-body" style="border-bottom:1px solid #ccc">
                <div class="row">
                    <div class="col-md-3">
                    <strong>Descrição: </strong><?php echo $endereco['DESCRICAO']?>
                    </div>
                    <div class="col-md-3">
                    <strong>CEP: </strong><?php echo $endereco['CEP']?>
                    </div>
                    <div class="col-md-4">
                    <strong>Cidade: </strong><?php echo $endereco['MUNICIPIO']?>
                    </div>
                    <div class="col-md-2">
                    <strong>Estado: </strong><?php echo $endereco['ESTADO']?>
                    </div>
                </div>
                <br>
                <div class="row">
                	<div class="col-md-3">
                    <strong>Bairro: </strong><?php echo $endereco['BAIRRO']?>
                    </div>
                    <div class="col-md-7">
                    <strong>Logradouro: </strong><?php echo $endereco['LOGRADOURO']?>
                    </div>
                    <div class="col-md-2">
                    <strong>Número: </strong><?php echo $endereco['NUMERO']?>
                    </div>
                </div>
                <br>
                <div class="row">
                	<div class="col-md-12">
                    <strong>Complemento: </strong><?php if($endereco['COMPLEMENTO']!=""){echo $endereco['COMPLEMENTO'];}else{echo "Sem Complemento";}?>
                    </div>
                </div>
            </div>
            
            <?php 
                }
            ?>
        </div>
        
        <div class="card mb-3"><div class="card-header"><i class="fa fa-phone"></i> Contatos</div>
          <?php 
            foreach($contatos as $contato)
            {
            ?>
            <div class="card-body" style="border-bottom: 1px solid #ccc">
                <div class="row">
                    <div class="col-md-6">
                    <strong>Tipo do contato: </strong><?php echo $contato['TIPO_VALOR']?>
                    </div>
                    <div class="col-md-5">
                    <strong>Número: </strong><?php echo $contato['VALOR']?>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>