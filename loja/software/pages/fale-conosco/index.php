<?php 
require_once 'functions.php';
index();
include HEADER;

$ver = (isset($menu_acessos['/gestao/clinicasxplanos/view.php']) && $menu_acessos['/gestao/clinicasxplanos/view.php']);
$edit = (isset($menu_acessos['/gestao/clinicasxplanos/edit.php']) && $menu_acessos['/gestao/clinicasxplanos/edit.php']);
$delete = (isset($menu_acessos['/gestao/clinicasxplanos/delete.php']) && $menu_acessos['/gestao/clinicasxplanos/delete.php']);

?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Fale conosco</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
		
<div class="row">
	<div class="col-lg-12">
        <div class="card mb-3"><div class="card-header">
        
        <div class="row">
        <div class="col-md-12">
       		<i class="fa fa-paper-plane-o"></i> Envie um email para a orbrix
        </div>
        </div>
        </div>
            <div class="card-body">
				
				<div class="row">
          <div class="col-lg-12">
            <form id="" name="sentMessage" novalidate="novalidate" method="post" action="envia_email.php">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" name="email[NOME]" id="name" type="text" placeholder="Seu nome *" required="required" data-validation-required-message="Por favor, preencha seu nome.">
                    <p class="help-block" style="color: white"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="email[EMAIL]" id="email" type="email" placeholder="Seu e-mail *" required="required" data-validation-required-message="Por favor, preencha seu e-mail.">
                    <p class="help-block" style="color: white"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="email[TELL]" id="phone" type="tel" placeholder="Seu telefone *" required="required" data-validation-required-message="Por favor, preencha seu telefone.">
                    <p class="help-block" style="color: white"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="email[EMPRESA]" id="company" type="text" placeholder="Sua empresa *" required="required" data-validation-required-message="Por favor, preencha o nome da sua empresa.">
                    <p class="help-block" style="color: white"></p>
                  </div>
                   <div class="form-group">
                    <input class="form-control" name="email[RAMO]" id="ramo" type="text" placeholder="Seu Ramo (Fornecedora ou Construtora) *" required="required" data-validation-required-message="Por favor, preencha o ramo da empresa.">
                    <p class="help-block" style="color: white"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" name="email[MENSAGEM]" id="message" placeholder="Sua mensagem *" required="required" data-validation-required-message="Por favor, escreva sua mensagem."></textarea>
                    <p class="help-block" style="color: white"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <div class="col-md-4  offset-md-4">
                    <button type="submit" name="submit" class="btn btn-info  btn-block"><i class="fa fa-paper-plane-o"></i> Enviar e-mail</button>
					</div>
                </div>
              </div>
            </form>
          </div>
        </div>
                    
					<div class="row">
                    
					</div>
					<br>
				
            </div>
        </div>	 
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>

<script>
</script>