<?php 
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	
	if(isset($_POST['submit1']))
	{
	    
	    $pf = $_POST['pf'];
	    //$produtoFornecedor = $_POST['produtoFornecedor'];
	    
	    update("TBL_PRODUTOS_FORNECEDOR",$pf,'ID_PRODUTO_FORNECEDOR',$_POST['submit1']);
	   
	}
	
	global $produtos;
	$produtos = find_id('VIEW_PRODUTOS_FORNECEDORES',"ID_FORNECEDOR",$_SESSION["id_fornecedor"]); // mais pra frente isso aqui vai ser um find_id de acordo com o id da fornecedora logada
	
	
}


function add()
{
    if(isset($_POST['submit']))
    {
        if(isset($_POST['item']) && count($_POST['item'])>0)
        {
       saveAll($_POST['item']);
        }
        
        $itens = (isset($_POST['pf']))?$_POST['pf']:array();
        if(count($itens)>0)
        {
            
            foreach ($itens as $item)
            {
                update('tbl_produtos_fornecedor', $item, 'ID_PRODUTO_FORNECEDOR', $item['ID_PRODUTO_FORNECEDOR']);
            }
            
        }
        
        $itensDeletados = (isset($_POST['deletados']))?$_POST['deletados']:array();
        print_r($itensDeletados);
        
        if(count($itensDeletados)>0)
        {
            
            foreach ($itensDeletados as $itemDeletado)
            {
                remove('tbl_produtos_fornecedor',"ID_PRODUTO_FORNECEDOR",$itemDeletado['ID_PRODUTO_FORNECEDOR']);
            }
            
        }
        if($_SESSION['type']=="success")
        {
            $_SESSION['message']= "Produtos Vinculados com sucesso";
            header('Location: index.php');
            exit();
        }
   
    }
    
    global $itens;
    $itens = find_id('VIEW_PRODUTOS_FORNECEDORES',"ID_FORNECEDOR",$_SESSION["id_fornecedor"]);
}





function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			
			$perfil = $_POST['perfil'];
			
			$all = find_all('PERFIL_WEB');
			
			$already_exists=false;
			foreach ($all as $perfil_cad)
			{
				if(strtolower($perfil_cad['PERFIL'])==strtolower($perfil["'PERFIL'"]) && $perfil_cad['ID_PERFIL']!=$id)
				{
					$already_exists = true;
					break;
				}
			}
			
			if($already_exists)
			{
				$_SESSION['type']= "danger";
				$_SESSION['message']= "Este Perfil Já Existe existe";
			}
			else
			{
			
				update('PERFIL_WEB', $perfil, 'ID_PERFIL', $id);
				
				if($_SESSION['type']=="success")
				{
					remove('ACESSO_PAGINAS_WEB', 'ID_PERFIL', $id);
					remove('ACESSO_SISTEMAS_WEB', 'ID_PERFIL', $id);
					$acessoPagina= array();
					foreach ($_POST['acesso'] as $acesso)
					{
					    $acessoPagina[] = array
						(
								'ID_PAGINA'=>$acesso,
								'ID_PERFIL'=>$id
						);
						
						
					}
					$acessoSistema = array();
					foreach ($_POST['acessoSistema'] as $acesso)
					{
					    $acessoSistema[] = array
					    (
					        'ID_SISTEMA'=>$acesso,
					        'ID_PERFIL'=>$id
					    );
					}
					
					save_acessos($acessoPagina, $acessoSistema);
					if($_SESSION['type']=="success")
					{
					    $_SESSION['message']= "Perfil editado com sucesso";
					    header('Location: ver-perfis.php');
					    exit;
					}
				}
			}
		}
		view($id);
		global $paginas;
		$busca = find_all('VIEW_PAGINAS_WEB');
		
		foreach($busca as $pagina)
		{
		    $str="";
		    $arr=array();
		    $arr = explode('/',$pagina['URL']);
		    if(count($arr)>1)
		    {
		        unset($arr[count($arr)-1]);
		        $str = implode('/', $arr);
		    }
		    else
		        $str = "/";
		        
		        $paginas[$pagina['ID_SISTEMA']]['paginas'][$str][] = $pagina;
		        $paginas[$pagina['ID_SISTEMA']]['info'] = array("ID_SISTEMA"=>$pagina['ID_SISTEMA'], "SIGLA_SISTEMA"=>$pagina['SIGLA_SISTEMA'], "NOME_SISTEMA"=>$pagina['NOME_SISTEMA']);
		}
	}
	else 
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: ver-perfil.php');//
		exit;
	}
}

function delete($id)
{
    if(isset($id))
    {
        remove('TBL_PRODUTOS_FORNECEDOR', ['ID_PRODUTO_FORNECEDOR','ID_FORNECEDOR'], [$id,$_SESSION['id_fornecedor']]);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Vinsulação excluida com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}