<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/vincular-produtos/add.php']) && $menu_acessos['/pages/vincular-produtos/add.php']);
$edit = (isset($menu_acessos['/pages/vincular-produtos/edit.php']) && $menu_acessos['/pages/vincular-produtos/edit.php']);
$delete = (isset($menu_acessos['/pages/vincular-produtos/delete.php']) && $menu_acessos['/pages/vincular-produtos/delete.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Vincular produtos a fornecedora</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Produtos vinculados
          </div>
            <?php 
	   if($add)
	   {
	?>
          <div class="col-md-1">
      		<a data-toggle="tooltip" data-placement="left" title="Vincular novos produtos" href="<?php echo BASEURL?>pages/vincular-produtos/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>
          </div>
          <?php 
	   }
          ?>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Preço mínimo</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                
                	<?php 
							foreach ($produtos as $produto)
								{
						?>
									<tr class="odd gradeX">
										<td> <?php echo $produto['NOME_PRODUTO']?> </td>
										<td><?php echo $produto['ESPECIFICACAO']?></td>
										<td><?php echo $produto['UM']?></td>
										<td>R$ <?php echo $produto['PRECO_MEDIO']?></td>
										
										<?php
											if($edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													 
														if($edit)
														{
													?>	
															<a class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal<?php echo $produto['ESPECIFICACAO'];echo $produto['PRECO_MEDIO']?>"><i data-toggle="tooltip" data-placement="left" title="Editar preço mínimo do produto" class="fa fa-edit"></i></a>
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $produto['ID_PRODUTO_FORNECEDOR']?>" data-id="<?php echo $produto['ID_PRODUTO_FORNECEDOR']?>" data-vincular="<?php echo $produto['ESPECIFICACAO']?>" data-tipo='VINCULAR'><i data-toggle="tooltip" data-placement="left" title="Desvincular produto" class="fa fa-trash-o"></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
									
									<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $produto['ESPECIFICACAO'];echo $produto['PRECO_MEDIO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="index.php">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p><strong>Setor:</strong> <?php echo $produto['SETOR']?></p>
      	<p><strong>Produto:</strong> <?php echo $produto['NOME_PRODUTO']?></p>
      	<p><strong>Especificação:</strong> <?php echo $produto['ESPECIFICACAO']?></p>
      	<p><strong>Unidade de Medida:</strong> <?php echo $produto['UM']?></p>
      	<p><strong>Preço Mínimo:</strong> R$ <input type="number" name="pf[PRECO_MEDIO]" step="0.01" class="text-center" value="<?php echo $produto['PRECO_MEDIO']?>"></p>
      	
     
      
      </div>
      
      <div class="modal-footer">
        <button class="btn btn-success" type="submit" id="btn_submit1" name="submit1" value="<?php echo $produto['ID_PRODUTO_FORNECEDOR']?>"><i class="fa fa-check"></i></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
	
  </div>
</div>


						<?php 
						/*echo '<pre>';
						print_r($produto);
						echo '</pre>';*/
						  }
						?>
                
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>



<?php 
include DELETEMODAL;
include FOOTER;
?>