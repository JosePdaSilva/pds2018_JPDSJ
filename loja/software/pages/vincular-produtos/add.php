<?php 
require_once 'functions.php';
add();
include HEADER;

$setores = find_all("tbl_setor");
$produtos = find_all("tbl_setor_produto");
?>
<script type="text/javascript">

var indexAula=<?php echo count($itens) ?>;
var qtdeTotal=0;
function addAula()
{

	var divLinha = document.createElement("TR");
	//divLinha.setAttribute("class", "form-group aulas");
	
	divLinha.id="aula"+indexAula;
	//divLinha.setAttribute("style","border: 1px solid #ccc; margin-top: 15px; border-radius:5px ; padding: 10px");
	

		var idAula = document.createElement("INPUT");
		idAula.id="id-aula"+indexAula;
		idAula.value="0";
		idAula.type="hidden";

		//var divRow1 = document.createElement("DIV");
		//divRow1.setAttribute("class", "row");
		
		
		//-----Setor------//
			var divSetor = document.createElement("TD");
			divSetor.setAttribute("style", "padding:10px");
			//divProduto.setAttribute("class", "col-xs-12 col-md-8");
			//var labelProduto = document.createElement("LABEL");
			//labelProduto.appendChild(document.createTextNode("Selecione O produto"));
			var nameSetor="item-setor"+indexAula;
			var Setor = document.createElement("SELECT");
			Setor.setAttribute("class", "form-control");
			Setor.setAttribute("onchange", "pesquisarProduto("+indexAula+");");
			Setor.id="item-setor"+indexAula;
			//Setor.name="item["+indexAula+"][ID_SETOR]";
			
			/*
				var option = document.createElement("OPTION");
				option.setAttribute("value", especificacao[0]["ID_SETOR"]);
			    var texto = document.createTextNode(especificacao[0]["SETOR"]);
			    option.appendChild(texto);
			    Setor.appendChild(option);
			*/
            <?php 
			foreach ($setores as $setor)
			{
			?>
				var option = document.createElement("OPTION");
				option.setAttribute("value", '<?php echo $setor["ID_SETOR"]?>');
			    var texto = document.createTextNode('<?php echo $setor["SETOR"]?>');
			    option.appendChild(texto);
			    Setor.appendChild(option);
            <?php 
			}
			?>
			Setor.value = document.getElementById('item-setor').value;
			
			//divProduto.appendChild(labelProduto);
			divSetor.appendChild(Setor);
			//-----Setor------//
			
						
			
			//-----Produto------//
			var divProduto = document.createElement("TD");
			divProduto.setAttribute("style", "padding:10px");
			//divProduto.setAttribute("class", "col-xs-12 col-md-8");
			//var labelProduto = document.createElement("LABEL");
			//labelProduto.appendChild(document.createTextNode("Selecione O produto"));
			var nameProduto="item-produto"+indexAula;
			var Produto = document.createElement("SELECT");
			Produto.setAttribute("class", "form-control");
			Produto.setAttribute("onchange", "pesquisarEspecificacao("+indexAula+");");
			Produto.id="item-produto"+indexAula;
			//Produto.name="item["+indexAula+"][ID_SETOR_PRODUTO]";


			
			
			//divProduto.appendChild(labelProduto);
			divProduto.appendChild(Produto);
			//-----Produto------//
			
			
			
				//-----Especificação------//
			var divEspecificacao = document.createElement("TD");
			divEspecificacao.setAttribute("style", "padding:10px");
			//divProduto.setAttribute("class", "col-xs-12 col-md-8");
			//var labelProduto = document.createElement("LABEL");
			//labelProduto.appendChild(document.createTextNode("Selecione O produto"));
			var nameProduto="item-especificacao"+indexAula;
			var Especificacao = document.createElement("SELECT");
			Especificacao.setAttribute("class", "form-control");
			Especificacao.setAttribute("onchange", "infoProduto("+indexAula+")");
			Especificacao.id="item-especificacao"+indexAula;
			Especificacao.name="item["+indexAula+"][ID_ESPECIFICACAO]";
		
			
			
			//divProduto.appendChild(labelProduto);
			divEspecificacao.appendChild(Especificacao);
			//-----Especificação------//
			
			
	//var divRow2 = document.createElement("DIV");
		//divRow2.setAttribute("class", "row");
		
		//----- UM ------//
			var divUM = document.createElement("TD");
			divUM.setAttribute("style", "padding:10px");
			//divQtd.setAttribute("class", "col-xs-12 col-md-4");
			//var labelQtd = document.createElement("LABEL");
			//labelQtd.appendChild(document.createTextNode("Quantidade Estoque"));
			var nameUM="item-um"+indexAula;
			var UM = document.createElement("INPUT");
			UM.setAttribute("class", "form-control");
			UM.type="text";
			UM.id="item-um"+indexAula;
			UM.disabled = true;
			
			//divQtd.appendChild(labelQtd);
			divUM.appendChild(UM);
		//----- UM ------//


			
			//-----Valor Unitário------//
			var divPreco = document.createElement("TD");
			divPreco.setAttribute("style", "padding:10px");
			//divValorUnitario.setAttribute("class", "col-xs-12 col-md-3");
			//var labelValorUnitario = document.createElement("LABEL");
			//labelValorUnitario.appendChild(document.createTextNode("Valor Unitário"));
			var namePreco="item-preco"+indexAula;
			var Preco = document.createElement("INPUT");
			Preco.setAttribute("class", "form-control");
			Preco.type="number";
			Preco.id="item-preco"+indexAula;
			Preco.name="item["+indexAula+"][PRECO_MEDIO]";
			
			//divValorUnitario.appendChild(labelValorUnitario);
			divPreco.appendChild(Preco);
			Preco.value = document.getElementById('item-preco').value;
			//-----Valor Unitário------//
			
			
			
			//-----BTN deleteAula-----//
			var divBtnexc = document.createElement("TD");
			divBtnexc.setAttribute("style", "text-align:center");
			//divBtnexc.setAttribute("class", "col-md-2 col-xs-12");
			var btnexc = document.createElement("BUTTON");
			btnexc.type="button";
			btnexc.id="btnExcluir"+indexAula;
			btnexc.innerHTML="<i class='fa fa-trash'></i>";
			btnexc.setAttribute("onclick", "removeAula('"+divLinha.id+"')");
			btnexc.setAttribute("class", "btn btn-danger");

			btnexc.setAttribute("data-toggle", "tooltip");
			btnexc.setAttribute("data-placement", "left");
			btnexc.setAttribute("title", "Desvincular item selecionado");

			//divBtnexc.appendChild(document.createElement("br"));
			divBtnexc.appendChild(btnexc);
			//-----BTN deleteAula-----//

			
			
	divLinha.appendChild(divSetor);
	divLinha.appendChild(divProduto);
	divLinha.appendChild(divEspecificacao);
	
	
	divLinha.appendChild(divUM);
	divLinha.appendChild(divPreco);
	divLinha.appendChild(divBtnexc);
	//divLinha.appendChild(divRow1);
	//divLinha.appendChild(divRow2);
	
	if(indexAula==0)
		document.getElementById("tabela").appendChild(divLinha);
	else
		document.getElementById("tabela").insertBefore(divLinha, document.getElementById("aula"+(indexAula-1)));

	pesquisarProduto(indexAula);
	document.getElementById('item-produto'+indexAula).value = document.getElementById('item-produto').value;
	pesquisarEspecificacao(indexAula);
	document.getElementById('item-especificacao'+indexAula).value = document.getElementById('item-especificacao').value;

	 $('#btnExcluir'+indexAula).tooltip();
	
	indexAula++;

	qtdeTotal++;
	//liberaBotaoCadastro();
}

var contDelets = 0;

function removeAula(id)
{
	var idAula = id.replace("aula", "");
	$('#btnExcluir'+idAula).tooltip('dispose');
	
	var filha = document.getElementById(id);
	var pai = document.getElementById("tabela");

	pai.removeChild(filha);
	qtdeTotal--;
	//liberaBotaoCadastro();
}



function excluirAula(id)
{
	if (window.XMLHttpRequest) {
	    // code for IE7+, Firefox, Chrome, Opera, Safari
	    xmlhttp=new XMLHttpRequest();
	  } else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function() {
	    if (this.readyState==4 && this.status==200) {
			alert('aula excluída');
	    }
	  }
	  xmlhttp.open("GET","deleteaula.php?id="+id, true);
	  xmlhttp.send();
}


var indexDeletados=0;

function deleteRow(id, i){
	document.getElementById('tabelaDeletados').innerHTML+="<input type='hidden' name='deletados["+indexDeletados+"][ID_PRODUTO_FORNECEDOR]' value="+id+">";
	indexDeletados++;
    document.getElementById('myTable').deleteRow(i);
    
}

</script>



<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Vincular produtos </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-12">
		<form action="add.php" method="post" role="form">

			
			<div class="row">
						<div class="col-md-12">
    						<div class="card mb-3">
                                <div class="card-header">
                                  <i class="fa fa-table"></i> Selecione os produtos a serem vinculados</div>
                                <div class="card-body">
                                  <div class="table-responsive" >
    								<table class="table-striped table-bordered table-hover" width="100%" style="border:1px solid #dee2e6">
                					<thead>
                						<tr>
                							<th style="padding:10px">Setor</th>
                							<th style="padding:10px">Produto</th>
                							<th style="padding:10px">Especificação</th>
                							<th style="padding:10px">UM</th>
                							<th style="padding:10px">Preço Mínimo</th>
                							<th style="padding:10px">opção</th>
                							
                						</tr>
                					</thead>
					
					<tbody>
							<tr>
								<td style="padding:10px">
								
									<select class="form-control"
                					id="item-setor" onchange='pesquisarProduto("");'>
                    				
                    				<?php 
                        			foreach ($setores as $setor)
                        			{
                        			?>
                					<option value="<?php echo $setor['ID_SETOR']?>"> <?php echo $setor['SETOR']?></option>
                					<?php 
                    			    }
        						    ?>
                				
        							</select>
        							
        						
								</td>
								<td style="padding:10px">
        							<select class="form-control"
                					id="item-produto" onchange="pesquisarEspecificacao('');">
        							</select>
        						</td>
        						
								<td style="padding:10px">
									<select class="form-control"
                					 id="item-especificacao" onchange="infoProduto('')">
        							</select>
        						</td>
        						<td style="padding:10px">
        							<input class="form-control"
            						type="text" id="item-um" disabled>
        						</td>
        							<td style="padding:10px">
        							<input class="form-control"
            						type="number" id="item-preco" step="0.01">
        						</td>
								<td style="text-align:center;padding-top:10px">
								<button type="button" class="btn btn-success" id="btn-add-item" style="margin-bottom: 15px" onclick="addAula();"><i data-toggle="tooltip" data-placement="left" title="Vincular produto selecionado" class="fa fa-plus"></i></button>
								</td>
							</tr>
				
				</table>
    									
                    	</div>
                    </div>
               	</div>
          	</div>
         </div>
                    				
                 <div class="row">
						<div class="col-md-12">
    						<div class="panel panel-default" id="planejamento_aula" >
    							<div class="card mb-3">
    								<div class="card-header" id="geral_aulas">
                                  <i class="fa fa-tags"></i> Produtos a serem vinculados a fornecedora</div>
                                  <div class="card-body">
                                  
                                			 <div class="table-responsive" >				
                                               <table width="100%" class="table-striped table-bordered table-hover" style="border:1px solid #dee2e6" >
                            					<thead>
                            						<tr>
                            							<th style="padding:10px">Setor</th>
                            							<th style="padding:10px">Produto</th>
                            							<th style="padding:10px">Especificação</th>
                            							<th style="padding:10px">UM</th>
                            							<th style="padding:10px">Preço Mínimo</th>
                            							<th style="padding:10px">opção</th>
                            							
                            						</tr>
                            					</thead>
                            					
                            					<tbody id="tabela">
                            					</tbody>
                            				</table>
                            			</div>
                        			</div>
                    			</div>
    						</div>
						</div>
					</div> 
			
			
			<div class="row">
						<div class="col-md-12">
    						<div class="panel panel-default" id="planejamento_aula" >
    							<div class="card mb-3">
    								<div class="card-header" id="geral_aulas">
                                  <i class="fa fa-tags"></i> Produtos já vinculados   
                                  <a  class="btn btn-outline-info" style="margin-left:22%;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fa fa-search" aria-hidden="true"></i> Visualizar produtos
                                  </a>
                              		</div>
                                  
                          <div class="collapse" id="collapseExample">
  							  <div class="card card-body">
                                  <div class="card-body">
                                  
                                			 <div class="table-responsive" >				
                                               <table width="100%" class="table table-striped table-bordered table-hover" style="border:1px solid #dee2e6" id="myTable" >
                            					<thead>
                            						<tr>
                            							<th>Setor</th>
                            							<th>Produto</th>
                            							<th>Especificação</th>
                            							<th>UM</th>
                            							<th>Preço Mínimo</th>
                            							<th>opção</th>
                            							
                            						</tr>
                            					</thead>
                            					
                            					<tbody>
                            						<?php 
                            						$qq = 0;
                            						  foreach ($itens as $item)
                            						  {
                            						      
                            						?>
                            						<tr>
                            						<input type="hidden"  name="pf[<?php echo $qq;?>][ID_PRODUTO_FORNECEDOR]" value="<?php echo $item['ID_PRODUTO_FORNECEDOR']?>">
                            						<td><?php echo $item['SETOR']?></td>
                            						<td><?php echo $item['NOME_PRODUTO']?></td>
                            						<td><?php echo $item['ESPECIFICACAO']?></td>
                            						<td><?php echo $item['UM']?></td>
                            						<td><input  class="text-center" name="pf[<?php echo $qq;?>][PRECO_MEDIO]" value="<?php echo $item['PRECO_MEDIO']?>"></td>
                            						<td><a  onclick="deleteRow(<?php echo $item['ID_PRODUTO_FORNECEDOR']?>, this.parentNode.parentNode.rowIndex)" href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $perfil['ID']?>" data-id="<?php echo $perfil['ID']?>" data-perfil="<?php echo $perfil['PERFIL']?>" data-tipo='PERFIL'><i data-toggle="tooltip" data-placement="left" title="Desvincular produto" class="fa fa-trash-o"></i></a></td>
                            						
                            						</tr>
                            						<?php 
                            						$qq++;
                            						
                            						  }
                            						?>
                            					</tbody>
                            				</table>
                            			</div>
                        			</div>
                    			</div>
							</div>
            			</div>
					</div>
				</div>
			</div> 
			
			<div id="tabelaDeletados" hidden></div>
			

			<div class="row">
				<div class="offset-md-2 col-md-4 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Salvar
					</button>
				</div>

				<div class="col-md-4 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
			<br>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

<script>

var especificacao = [];


function infoProduto(id)
{
	
	
	var x = document.getElementById('item-especificacao'+id).value;
	document.getElementById('item-um'+id).value="";
	if(x!="")
	{
	$.ajax
	({
		url: "getEspecificacao.php", 
		type: "POST", 
		data: "ID_ESPECIFICACAO="+x,
		success: function(result)
		{
			unidade = JSON.parse(result)
			document.getElementById('item-um'+id).value=unidade[0]['UM'];
		}
	});
	}
}

function pesquisarEspecificacao(id)
{
	var x = document.getElementById('item-produto'+id).value;
	document.getElementById('item-especificacao'+id).innerHTML = "";
	if(x!="")
	{
	$.ajax
	({
		url: "getEspecificacao.php", 
		async: false,
		type: "POST", 
		data: "ID_SETOR_PRODUTO="+x,
		success: function(result)
		{
			especificacao = JSON.parse(result)
			
			for(i=0;i<especificacao.length;i++)
			{
				document.getElementById('item-especificacao'+id).innerHTML += "<option value='"+ especificacao[i]["ID_ESPECIFICACAO"] +"'>"+especificacao[i]["ESPECIFICACAO"]+"</option>";				
			}
			infoProduto(id);
		}
	});
	}
}

function pesquisarProduto(id)
{
	var x = document.getElementById('item-setor'+id).value;
	document.getElementById('item-produto'+id).innerHTML = "";
	if(x!="")
	{
	$.ajax
	({
		url: "getProduto.php", 
		async: false,
		type: "POST", 
		data: "ID_SETOR="+x,
		success: function(result)
		{
			var produto = JSON.parse(result)
			for(i=0;i<produto.length;i++)
			{
				document.getElementById('item-produto'+id).innerHTML += "<option value='"+ produto[i]["ID_SETOR_PRODUTO"] +"'>"+produto[i]["NOME_PRODUTO"]+"</option>";
			}
			pesquisarEspecificacao(id);
		}
	});
	}
}

pesquisarProduto("");

</script>