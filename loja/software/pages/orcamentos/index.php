<?php
require_once 'functions.php';
index();
include HEADER;

$cadastrar = (isset($menu_acessos['/pages/orcamentos/ofertar.php']) && $menu_acessos['/pages/orcamentos/ofertar.php']);
$rejeitar = (isset($menu_acessos['/pages/orcamentos/recusar.php']) && $menu_acessos['/pages/orcamentos/recusar.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Orçamentos </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          		<ul class="nav nav-tabs" role="tablist">
        			 <li class="nav-item">
                        <a class="list-group-item list-group-item-action active" data-toggle="list" href="#home" role="tab" href="#">Pedidos</a>
                      </li>
                       <li class="nav-item">
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab">Orçados</a>
                      </li>
                      <li class="nav-item">
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#messages" role="tab">Rejeitados</a>
                      </li>
        		</ul>
        </div>
        
      
     <div class="card-body">
      <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Cidade</th>
                  <th>Data do pedido</th>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Quantidade</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
               <?php 
                                foreach ($pedidos[''] as $pedido)
								{
								    /*echo '<pre>';
								    print_r($pedido);
								    echo '</pre>';*/
						?>
						
								
									<tr class="odd gradeX">
									
										<td><?php echo $pedido[0]['MUNICIPIO']?></td>
										<td><?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']))?></td>
										<td><?php echo $pedido[0]['NOME_PRODUTO']?></td>
										<td><?php echo $pedido[0]['ESPECIFICACAO']?></td>
										<td><?php echo $pedido[0]['UM']?></td>
										<td><?php echo $pedido[0]['QTD_SOLICITADA']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td class="text-center">
												<div class="row" style="padding:10%">
												
													<?php 
													 
													if($cadastrar)
														{
													?>
															<a href="edit.php?id=<?php //echo $perfil['ID'];?>" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"><t data-toggle="tooltip" data-placement="left" title="Ofertar um valor para o pedido">Ofertar</t></a>
													<?php 
														}
														if($rejeitar)
														{
													?>
													<form method="post" action="index.php">
															<input type="hidden"  name="oferta[ID_ITEM_PEDIDO]" value="<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>">
												  			<button data-toggle="tooltip" data-placement="left" title="Rejeitar oferta" style="margin-left: 3px" class="btn btn-sm btn-danger" type="submit" id="btn_submit2" name="submit2" ><i class="fa fa-remove"></i></button>
											  		
											  		</form>
											  		<?php 
														}
											  		?>
											  		</div>
												</td>
										<?php 
											}
										?>
										    
									</tr>
									
									
<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<div class="modal-content">
    <form method="post" action="index.php">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Pedido - 	<strong>DIA: <?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']));?></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<h5>Endereço da Obra</h5>
      	<p><strong>Cidade: </strong><?php echo $pedido[0]['MUNICIPIO']?></p>
      	<p><strong>Bairro: </strong><?php echo $pedido[0]['BAIRRO']?></p>
      	<p><strong>Estado: </strong><?php echo $pedido[0]['ESTADO']?></p>
      	<hr>
      	<h5>Info. Pedido</h5>
      	<p><strong>Produto: </strong><?php echo $pedido[0]['NOME_PRODUTO']?></p>
      	<p><strong>Especificação: </strong><?php echo $pedido[0]['ESPECIFICACAO']?> </p>
      	<p><strong>Prexo Max: </strong><?php if($pedido[0]['PRECO_MAX']==''){ echo 'Sem preço';}else{ echo $pedido[0]['PRECO_MAX'];}?></p>
      	<p><strong>Qtd. Solicitada:</strong> <input class="form-control" style="text-align:center" disabled id="qtd<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['QTD_SOLICITADA']?>"></p>
      	<a id="Mensagem<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" hidden style="color:red"></a>
      	<input type="hidden" id="precoMedio<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $validacao[$pedido[0]['ID_ITEM_PEDIDO']]['PRECO_MEDIO']?>">
      	<input type="hidden" id="precoMax<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['PRECO_MAX'];?>">
      	<input type="hidden"  name="oferta[ID_ITEM_PEDIDO]" value="<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"> 
      	<p>Preço Unitário: <input name="oferta[VALOR_OFERTADO]" class="form-control text-center" type="number" step="0.01" id="precoUnit<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" oninput="validaPreco(<?php echo $pedido[0]['ID_ITEM_PEDIDO'] ?>)"></p> 
      	
      	<p>Preço Total: <input class="form-control text-center" type="text" id="precoTotal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" Disabled></p>
      	
      
      </div>
      <div class="modal-footer">
        <button data-toggle="tooltip" data-placement="left" title="Ofertar valor para o pedido" class="btn btn-success" disabled type="submit" id="btn_submit1<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" name="submit1" ><i class="fa fa-check"></i> Ofertar</button>
        <button data-toggle="tooltip" data-placement="left" title="Recusar oferta" class="btn btn-danger" type="submit" id="btn_submit2" name="submit2" ><i class="fa fa-remove"></i> Recusar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </form>
    </div>
  </div>
</div>

						<?php 
								}
						?>
                
              </tbody>
            </table>
          </div>
        </div>
        
        
        
        
        
        <div class="tab-pane" id="profile" role="tabpanel">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
              <thead>
                <tr>
                 <th>Cidade</th>
                  <th>Data do pedido</th>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Quantidade</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
             
             
             <?php 
                                foreach ($pedidos['O'] as $pedido)
								{
						?>
						
									<tr class="odd gradeX">
										<td><?php echo $pedido[0]['MUNICIPIO']?></td>
										<td><?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']))?></td>
										<td><?php echo $pedido[0]['NOME_PRODUTO']?></td>
										<td><?php echo $pedido[0]['ESPECIFICACAO']?></td>
										<td><?php echo $pedido[0]['UM']?></td>
										<td><?php echo $pedido[0]['QTD_SOLICITADA']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													if($cadastrar)
														{
													?>
															<a href="edit.php?id=<?php //echo $perfil['ID'];?>" class="btn btn-sm btn-success" data-toggle="modal" data-target="#exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"><t data-toggle="tooltip" data-placement="left" title="Visualizar/Editar valor ofertado">Ofertado</t></a>
													<?php 
														}
													?>
												  
												</td>
										<?php 
											}
										?>
										    
									</tr>
									
<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="index.php">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Pedido - 	<strong>DIA: <?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']));?></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<h5>Endereço da Obra</h5>
      	<p><strong>Cidade: </strong><?php echo $pedido[0]['MUNICIPIO']?></p>
      	<p><strong>Bairro: </strong><?php echo $pedido[0]['BAIRRO']?></p>
      	<p><strong>Estado: </strong><?php echo $pedido[0]['ESTADO']?></p>
      	<hr>
      	<h5>Info. Pedido</h5>
      	<p><strong>Produto: </strong><?php echo $pedido[0]['NOME_PRODUTO']?></p>
      	<p><strong>Especificação: </strong><?php echo $pedido[0]['ESPECIFICACAO']?> </p>
      	<p><strong>Prexo Max: </strong><?php if($pedido[0]['PRECO_MAX']==''){ echo 'Sem preço';}else{ echo $pedido[0]['PRECO_MAX'];}?></p>
      	<p><strong>Qtd. Solicitada:</strong> <input class="form-control" style="text-align:center" disabled id="qtd<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['QTD_SOLICITADA']?>"></p>
      	<a id="Mensagem<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" hidden style="color:red"></a>
      	<input type="hidden" id="precoMedio<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $validacao[$pedido[0]['ID_ITEM_PEDIDO']]['PRECO_MEDIO']?>">
      	<input type="hidden" id="precoMax<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['PRECO_MAX'];?>">
      	<input type="hidden"  name="oferta[ID_ITEM_PEDIDO]" value="<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"> 
      	<p>Preço unitário: <input value="<?php echo $pedido[0]['VALOR_OFERTADO']?>" name="oferta[VALOR_OFERTADO]" class="form-control text-center" type="number" step="0.01" id="precoUnit<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" oninput="validaPreco(<?php echo $pedido[0]['ID_ITEM_PEDIDO'] ?>)"></p> 
      	
      	<p>Preço total: <input class="form-control text-center" type="text" id="precoTotal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo ($pedido[0]['VALOR_OFERTADO']*$pedido[0]['QTD_SOLICITADA'])?>" Disabled></p>
      	
      
      </div>
      <div class="modal-footer">
        <button data-toggle="tooltip" data-placement="left" title="Editar valor ofertado" class="btn btn-warning" disabled  type="submit" id="btn_submit1<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" name="submit3" value="<?php echo $pedido[0]['ID_ORCAMENTO']?>"><i class="fa fa-edit"></i> Editar</button>
        <button data-toggle="tooltip" data-placement="left" title="Recusar oferta" class="btn btn-danger" type="submit" id="btn_submit2" name="submit4" value="<?php echo $pedido[0]['ID_ORCAMENTO']?>"><i class="fa fa-remove"></i> Recusar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </form>
    </div>
  </div>
</div>

						<?php 
								}
						?>
                
                
              </tbody>
            </table>
          </div>
        </div>
        
        
        
        
        <div class="tab-pane" id="messages" role="tabpanel">
        
         <div class="table-responsive">
            <table class="table table-bordered" id="dataTable3" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Cidade</th>
                  <th>Data do pedido</th>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Quantidade</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                
                 
             <?php 
                                foreach ($pedidos['R'] as $pedido)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $pedido[0]['MUNICIPIO']?></td>
										<td><?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']))?></td>
										<td><?php echo $pedido[0]['NOME_PRODUTO']?></td>
										<td><?php echo $pedido[0]['ESPECIFICACAO']?></td>
										<td><?php echo $pedido[0]['UM']?></td>
										<td><?php echo $pedido[0]['QTD_SOLICITADA']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													if($cadastrar)
														{
													?>
															<a href="edit.php?id=<?php //echo $perfil['ID'];?>" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"><t data-toggle="tooltip" data-placement="left" title="Ofertar valor para o pedido">Ofertar</t></a>
													<?php 
														}
													?>
												  
												</td>
										<?php 
											}
										?>
										    
									</tr>
									
<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="index.php">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Pedido - 	<strong>DIA: <?php echo date('d/m/Y',strtotime($pedido[0]['DATA_PEDIDO']));?></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<h6>Endereço da Obra</h6>
      	<p>Cidade: <?php echo $pedido[0]['MUNICIPIO']?></p>
      	<p>Bairro: <?php echo $pedido[0]['BAIRRO']?></p>
      	<p>Estado: <?php echo $pedido[0]['ESTADO']?></p>
      	<hr>
      	<h6>Info. Pedido</h6>
      	<p>Produto: <?php echo $pedido[0]['NOME_PRODUTO']?></p>
      	<p>Especificação: <?php echo $pedido[0]['ESPECIFICACAO']?> </p>
      	<p><strong>Prexo Max: </strong> <?php if($pedido[0]['PRECO_MAX']==''){ echo 'Sem preço';}else{ echo $pedido[0]['PRECO_MAX'];}?></p>
      	<p><strong>Qtd. Solicitada:</strong> <input class="form-control" style="text-align:center" disabled id="qtd<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['QTD_SOLICITADA']?>"></p>
      	<a id="Mensagem<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" hidden style="color:red"></a>
      	<input type="hidden" id="precoMedio<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $validacao[$pedido[0]['ID_ITEM_PEDIDO']]['PRECO_MEDIO']?>">
      	<input type="hidden" id="precoMax<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo $pedido[0]['PRECO_MAX'];?>">
      	<input type="hidden"  name="oferta[ID_ITEM_PEDIDO]" value="<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>"> 
      	<p>Preço Unitário: <input value="<?php echo $pedido[0]['VALOR_OFERTADO']?>" name="oferta[VALOR_OFERTADO]" class="form-control text-center" type="number" step="0.01" id="precoUnit<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" oninput="validaPreco(<?php echo $pedido[0]['ID_ITEM_PEDIDO'] ?>)"></p> 
      	
      	<p>Preço Total: <input class="form-control text-center" type="text" id="precoTotal<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" value="<?php echo ($pedido[0]['VALOR_OFERTADO']*$pedido[0]['QTD_SOLICITADA'])?>" Disabled></p>
      	
      
      </div>
      <div class="modal-footer">
        <button data-toggle="tooltip" data-placement="left" title="Ofertar valor" class="btn btn-success" disabled type="submit" id="btn_submit1<?php echo $pedido[0]['ID_ITEM_PEDIDO']?>" name="submit3" value="<?php echo $pedido[0]['ID_ORCAMENTO']?>"><i class="fa fa-check"></i> Ofertar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
      </form>
    </div>
  </div>
</div>
						<?php 
								}
						?>
                
                
              </tbody>
            </table>
          </div>
        
        </div>
        
      </div>
      
      </div>
      
      
	</div>
  </div>
</div>






<?php 
include FOOTER;
?>

<script>



function validaPreco(id)
{
	var y = document.getElementById('precoUnit'+id).value;
if(y!="")
{
    var x = parseFloat(document.getElementById('precoMedio'+id).value);
    var qtd = parseFloat(document.getElementById('qtd'+id).value);
	var y = parseFloat(document.getElementById('precoUnit'+id).value);
	var z = parseFloat(document.getElementById('precoMax'+id).value);

    if(y<x)
    {
    	document.getElementById('Mensagem'+id).innerHTML = "<i class='fa fa-remove' id='icone'></i> Valor Superior ao Preço Mínimo: R$ "+x;
    	document.getElementById("Mensagem"+id).hidden = false;
    	//document.getElementById("precoUnit"+id).value = "";
    	document.getElementById('precoTotal'+id).value = "";
    	document.getElementById("btn_submit1"+id).disabled = true;
    	
    }
    else if(y>z)
    {
    	document.getElementById('Mensagem'+id).innerHTML = "<i class='fa fa-remove' id='icone'></i> Valor Superior ao Preço Máximo desejado: R$ "+z;
    	document.getElementById("Mensagem"+id).hidden = false;
    	//document.getElementById("precoUnit"+id).value = "";
    	document.getElementById('precoTotal'+id).value = "";
    	document.getElementById("btn_submit1"+id).disabled = true;
    	
    }
    else
    {
    	document.getElementById('Mensagem'+id).innerHTML = "";
    	document.getElementById('precoTotal'+id).value= qtd*y;
    	document.getElementById("btn_submit1"+id).disabled = false;
    }
}
else
{
	document.getElementById('Mensagem'+id).innerHTML = "";
	document.getElementById('precoTotal'+id).value= "";
	document.getElementById("btn_submit1"+id).disabled = true;
	
}

}
</script>