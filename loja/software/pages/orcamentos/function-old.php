<?php 
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
    
    
    if(isset($_POST['submit1']))
    {
        $oferta = $_POST['oferta'];
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "O";
        
        save('TBL_ORCAMENTO', $oferta);
        
        $_SESSION['message'] = "Oferta Registrda com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    if(isset($_POST['submit2']))
    {
        $oferta = $_POST['oferta'];
        
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "R";
        $oferta['VALOR_OFERTADO'] =  null;
        
        save('TBL_ORCAMENTO', $oferta);
        
        $_SESSION['message'] = "Oferta Recusada com sucesso!";
        $_SESSION['type'] = 'success';
    }
    
    
    if(isset($_POST['submit3']))
    {
        $oferta = $_POST['oferta'];
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "O";
        
        update('TBL_ORCAMENTO', $oferta,"ID_ORCAMENTO",$_POST['submit3']);
        
        $_SESSION['message'] = "Oferta Registrda com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    
    if(isset($_POST['submit4']))
    {
        $oferta = $_POST['oferta'];
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "R";
        $oferta['VALOR_OFERTADO'] =  null;
        
        update('TBL_ORCAMENTO', $oferta,"ID_ORCAMENTO",$_POST['submit4']);
        
        $_SESSION['message'] = "Oferta Recusada com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    
  
    
    global $pedidos, $validacao;
    $pedidos[''] =  array();
    $pedidos['O'] =  array();
    $pedidos['R'] =  array();
    $validacao = array();
    
    $produtosFornecedor = find_id('tbl_produtos_fornecedor', 'ID_FORNECEDOR', $_SESSION["id_fornecedor"]);
    
    
    
    if(count($produtosFornecedor)>0)
    {
        foreach($produtosFornecedor as $produto)
        {
            $busca = find_id("VIEW_ORCAMENTOS", ['ID_ESPECIFICACAO', 'ID_FORNECEDOR'], [$produto['ID_ESPECIFICACAO'], $_SESSION["id_fornecedor"]]);
            
            if(count($busca)>0)
            {
                foreach ($busca as $pedido)
                {
                    $pedidos[$pedido['STATUS_OFERTA']][$pedido['ID_ITEM_PEDIDO']][] = $pedido;
                }
            }
            $busca2 = find_id("VIEW_ITENS_PEDIDO", 'ID_ESPECIFICACAO', $produto['ID_ESPECIFICACAO']);
            
           /* echo '<pre>';
            print_r($busca2);
            echo '</pre>';*/
            
            if(count($busca2)>0)
            {
                foreach ($busca2 as $pedido)
                {
                    $pedidos[''][$pedido['ID_ITEM_PEDIDO']][] = $pedido;
                    
                }
            }
        }
    }

   /*echo '<pre>';
    print_r($pedidos);
    echo '</pre>';*/
   
    
    foreach ($pedidos as $qq)
    {
        foreach ($qq as $p)
        {
            $validacao[$p[0]['ID_ITEM_PEDIDO']] = find_id("tbl_produtos_fornecedor",["ID_ESPECIFICACAO", "ID_FORNECEDOR"],[$p[0]['ID_ESPECIFICACAO'], $_SESSION["id_fornecedor"]])[0];
        }
    }
    
    
    
    
}



?>