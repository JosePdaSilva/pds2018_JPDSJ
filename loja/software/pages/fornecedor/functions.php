<?php 

require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
    global $enderecos, $contatos, $fornecedora, $user;
    $enderecos = find_id("view_enderecos_fornecedora","ID_FORNECEDOR",$_SESSION["id_fornecedor"]);
    $contatos = find_id("view_contatos_fornecedora","ID_FORNECEDOR",$_SESSION["id_fornecedor"]);
    $fornecedora = find_id("view_fornecedores","ID_FORNECEDOR",$_SESSION["id_fornecedor"]);
    $user = find_id("view_usuarios_login","ID_FORNECEDOR",$_SESSION["id_fornecedor"]);
}


function view()
{
    if(isset($_SESSION['id_empresa']))
	{
		global $empresa;
		$result = find_id('view_fornecedores','ID_EMPRESA',$_SESSION['id_empresa']);
		if(count($result)==0)
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
		$empresa = $result[0];
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}


function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
		    
		    $enderecos=array();
		    if(isset($_POST['enderecosDeletados']) && count($_POST['enderecosDeletados']) > 0){
		    foreach ($_POST['enderecosDeletados'] as $endereco)
		    {
		        $enderecos[] = $endereco;
		        
		        remove('tbl_endereco_empresa','ID_ENDERECO_EMPRESA',$endereco['ID_ENDERECO_EMPRESA']);
		    }
		    }
		    
		    
		    $contatos=array();
		    if(isset($_POST['contatosDeletados']) && count($_POST['contatosDeletados']) > 0 ){
		        foreach ($_POST['contatosDeletados'] as $contato)
		        {
		            $contatos[] = $contato;
		           /* echo '<pre>';
		            print_r($contato);
		            echo '</pre>';
		            exit();*/
		            
		            remove('tbl_contato_empresa','ID_CONTATO_EMPRESA',$contato['ID_CONTATO_EMPRESA']);
		        }
		    }
		    
		    $enderecosNew=array();
		    if(isset($_POST['endereco']) && count($_POST['endereco']) > 0 ){
		    foreach ($_POST['endereco'] as $enderecoNew)
		    {
		        $enderecoNew['CEP'] = str_replace("-","",$enderecoNew['CEP']);
		        $enderecosNew[] = $enderecoNew;
		        /*echo '<pre>';
		        print_r($enderecoNew);
		        echo '</pre>';*/
		    }
		    }
		    
		    $contatosNew=array();
		    if(isset($_POST['contato']) && count($_POST['contato']) > 0 ){
		    foreach ($_POST['contato'] as $contatoNew)
		    {
		        $contatosNew[] = $contatoNew;
		        /*echo '<pre>';
		         print_r($enderecoNew);
		         echo '</pre>';*/
		    }
		    }
		    
		    
		    $enderecosUp=array();
		    if(isset($_POST['enderecoEdit']) && count($_POST['enderecoEdit']) > 0 ){
		    foreach ($_POST['enderecoEdit'] as $enderecoUp)
		    {
		        $enderecoUp['CEP'] = str_replace("-","",$enderecoUp['CEP']);
		        $enderecosUp[] = $enderecoUp;
		      /* echo '<pre>';
		        print_r($enderecoUp);
		        echo '</pre>';*/
		        
		        update('tbl_enderecos',$enderecoUp,"ID_ENDERECO",$enderecoUp['ID_ENDERECO']);
		        
		    }
		    }
		    
		    $contatosUp=array();
		    if(isset($_POST['contatoEdit']) && count($_POST['contatoEdit']) > 0 ){
		        foreach ($_POST['contatoEdit'] as $contatoUp)
		        {
		            $contatosUp[] = $contatoUp;
		            /*echo '<pre>';
		             print_r($contatoUp);
		             echo '</pre>';
		            exit();*/
		            update('tbl_contatos',$contatoUp,"ID_CONTATO",$contatoUp['ID_CONTATO']);
		            
		        }
		    }
		    
		    
		    
		       edit_empresa($enderecosNew, $contatosNew);
				    
				if($_SESSION['type']=="success")
				{
					$_SESSION['message']= "Fornecedora alterada com sucesso";
					header('Location: index.php');
					exit;
			}
		}
		view();
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}

?>