<?php 
require_once 'functions.php';
index();
include HEADER;

$ver = (isset($menu_acessos['/gestao/clinicasxplanos/view.php']) && $menu_acessos['/gestao/clinicasxplanos/view.php']);
$edit = (isset($menu_acessos['/gestao/clinicasxplanos/edit.php']) && $menu_acessos['/gestao/clinicasxplanos/edit.php']);
$delete = (isset($menu_acessos['/gestao/clinicasxplanos/delete.php']) && $menu_acessos['/gestao/clinicasxplanos/delete.php']);

?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Informações da fornecedora</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
		
<div class="row">
	<div class="col-lg-12">
        <div class="card mb-3"><div class="card-header">
        
        <div class="row">
        <div class="col-md-11">
       		<i class="fa fa-wrench"></i> Dados da fornecedora
        </div>
        <div class="col-md-1">
            <a data-toggle="tooltip" data-placement="left" title="Editar informações da fornecedora" href="<?php echo BASEURL?>pages/fornecedor/edit.php?id=<?php echo $fornecedora[0]['ID_EMPRESA']?>"
    		class="btn btn-warning btn-sm btn-block" style="font-size: 15px;"> <i
    		class="fa fa-edit"></i>
    		</a>
        </div>
        </div>
        </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                    <h3><?php echo $fornecedora[0]['RAZAO_SOCIAL']?></h3>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                    <strong>Nome do representante: </strong><?php echo $fornecedora[0]['NOME_RESPONSAVEL']?>
                    </div>
                    <div class="col-md-6">
                    <strong>CNPJ: </strong><?php echo mask($fornecedora[0]['CNPJ'],'##.###.###/####-##');?>
                    </div>
                </div>
                <br>
                 <div class="row">
                    <div class="col-md-12">
                    <strong>E-mail: </strong><?php echo $fornecedora[0]['EMAIL']?>
                    </div>
                </div>
            </div>
        </div>	 
        
        <div class="card mb-3"><div class="card-header"><i class="fa fa-map-marker"></i> Endereços</div>
            <?php 
            foreach($enderecos as $endereco)
            {
            ?>
            <div class="card-body" style="border-bottom:1px solid #ccc">
                <div class="row">
                    <div class="col-md-3">
                    <strong>Descrição: </strong><?php echo $endereco['DESCRICAO']?>
                    </div>
                    <div class="col-md-3">
                    <strong>CEP: </strong><?php echo $endereco['CEP']?>
                    </div>
                    <div class="col-md-4">
                    <strong>Cidade: </strong><?php echo $endereco['MUNICIPIO']?>
                    </div>
                    <div class="col-md-2">
                    <strong>Estado: </strong><?php echo $endereco['ESTADO']?>
                    </div>
                </div>
                <br>
                <div class="row">
                	<div class="col-md-3">
                    <strong>Bairro: </strong><?php echo $endereco['BAIRRO']?>
                    </div>
                    <div class="col-md-7">
                    <strong>Logradouro: </strong><?php echo $endereco['LOGRADOURO']?>
                    </div>
                    <div class="col-md-2">
                    <strong>Número: </strong><?php echo $endereco['NUMERO']?>
                    </div>
                </div>
                <br>
                <div class="row">
                	<div class="col-md-12">
                    <strong>Complemento: </strong><?php if($endereco['COMPLEMENTO']!=""){echo $endereco['COMPLEMENTO'];}else{echo "Sem Complemento";}?>
                    </div>
                </div>
            </div>
            
            <?php 
                }
            ?>
        </div>
        
        <div class="card mb-3"><div class="card-header"><i class="fa fa-phone"></i> Contatos</div>
          <?php 
            foreach($contatos as $contato)
            {
            ?>
            <div class="card-body" style="border-bottom: 1px solid #ccc">
                <div class="row">
                    <div class="col-md-6">
                    <strong>Tipo do contato: </strong><?php echo $contato['TIPO_VALOR']?>
                    </div>
                    <div class="col-md-5">
                    <strong>Número: </strong><?php echo $contato['VALOR']?>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>