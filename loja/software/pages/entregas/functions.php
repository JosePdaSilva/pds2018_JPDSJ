<?php
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
    
    
    if(isset($_POST['submit1']))
    {
        $oferta = $_POST['oferta'];
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "O";
        
        save('TBL_ORCAMENTO', $oferta);
        
        $_SESSION['message'] = "Oferta Registrada com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    if(isset($_POST['submit2']))
    {
        $oferta = $_POST['oferta'];
        
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "R";
        $oferta['VALOR_OFERTADO'] =  null;
        
        save('TBL_ORCAMENTO', $oferta);
        
        $_SESSION['message'] = "Oferta Recusada com sucesso!";
        $_SESSION['type'] = 'success';
    }
    
    
    if(isset($_POST['submit3']))
    {
        $oferta = $_POST['oferta'];
        $oferta['ID_FORNECEDOR'] =  $_SESSION["id_fornecedor"];
        $oferta['STATUS_OFERTA'] =  "O";
        
        update('TBL_ORCAMENTO', $oferta,"ID_ORCAMENTO",$_POST['submit3']);
        
        $_SESSION['message'] = "Oferta Registrada com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    
    if(isset($_POST['submit4']))
    {
        $qq = new DateTime();
        $oferta = $_POST['oferta'];
        $oferta['ENTREGUE'] =  "S";
        
        update('TBL_ORCAMENTO', $oferta,"ID_ORCAMENTO",$_POST['submit4']);
        
        $result = find('TBL_ORCAMENTO', "ID_ORCAMENTO",$_POST['submit4']);
        update('TBL_ITENS_PEDIDO', ['ENTREGUE'=>'S',"DATA_ENTREGUE"=>$qq->format('Y-m-d')] ,"ID_ITEM_PEDIDO", $result[0]['ID_ITEM_PEDIDO']);
        
        $_SESSION['message'] = "Entrega realizada com sucesso!";
        $_SESSION['type'] = 'success';
        
    }
    
    
    
    global $pedidos, $validacao;
    $pedidos[''] =  array();
    $pedidos['S'] =  array();
    $validacao = array();
    
    $produtosFornecedor = find_id('tbl_produtos_fornecedor', 'ID_FORNECEDOR', $_SESSION["id_fornecedor"]);
    
    $pedidos['S'] = find_id("VIEW_ORCAMENTOS", ['ID_FORNECEDOR', 'ENTREGUE'], [$_SESSION["id_fornecedor"], 'S']);
    
    if(count($produtosFornecedor)>0)
    {
        foreach($produtosFornecedor as $produto)
        {
            
            $founded = find_id("VIEW_ORCAMENTOS", ['ID_ESPECIFICACAO', 'ID_FORNECEDOR', 'ENTREGUE',  'STATUS_OFERTA'], [$produto['ID_ESPECIFICACAO'], $_SESSION["id_fornecedor"], 'N', 'F']);
            if(count($founded)>0)
            {
                foreach ($founded as $pedido)
                {
                    $pedidos[''][] = $pedido;
                }
            }
        }
    }
    /*
     echo '<pre>';
     print_r($pedidos['']);
     echo '</pre>';
     */
    
    
    
}



?>