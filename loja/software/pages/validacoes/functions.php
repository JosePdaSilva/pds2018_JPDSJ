<?php 
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function indexc()
{
    
    if(isset($_POST['submit1']))
    {
        require_once '../../sendEmail.php';
        try {
            $findEmail = find('view_usuarios_login', 'ID_EMPRESA', $_POST['submit1']);// find email da empresa
                $fields = $findEmail[0];
                $username = $fields['USERNAME'];
                $email = $fields['EMAIL'];
                
                if(new sendemail(array($email, $username)))
                {
                    $_SESSION['message'] = "email enviado com sua aprovação";
                    $_SESSION['type'] = "success";
                    update("TBL_EMPRESA",["APROVACAO"=>"S"],'ID_EMPRESA', $_POST['submit1']);
                }
                else 
                {
                    $_SESSION['message'] = "email não enviado";
                    $_SESSION['type'] = "danger";
                }
        } catch (Exception $e) {
            $_SESSION['message'] = $e->getMessage;
            $_SESSION['type'] = "danger";
        }
    }
    else if(isset($_POST['submit2']))
    {
        update("TBL_EMPRESA",["APROVACAO"=>"N"],'ID_EMPRESA', $_POST['submit2']);
        
        if($_SESSION['type']=="success")
        {
            $_SESSION['message']= "Empresa Reprovada com sucesso";
        }
    }
    
	global $construtores;
	$busca = find_all('VIEW_CONSTRUTORES');
	
	$construtores[''] = array();
	$construtores['S'] = array();
	$construtores['N'] = array();
	
	if(count($busca)>0)
	{
	    foreach($busca as $construtor)
	    {
	        
	        $construtores[$construtor['APROVACAO']][] = $construtor;
	        
	    }
	}
		
}

function indexf()
{
    
    if(isset($_POST['submit1']))
    {
        require_once '../../sendEmail.php';
        try {
            $findEmail = find('view_usuarios_login', 'ID_EMPRESA', $_POST['submit1']);// find email da empresa
            $fields = $findEmail[0];
            $username = $fields['USERNAME'];
            $email = $fields['EMAIL'];
            
            if(new sendemail(array($email, $username)))
            {
                $_SESSION['message'] = "email enviado";
                $_SESSION['type'] = "success";
                update("TBL_EMPRESA",["APROVACAO"=>"S"],'ID_EMPRESA', $_POST['submit1']);
            }
            else
            {
                $_SESSION['message'] = "email não enviado";
                $_SESSION['type'] = "danger";
            }
        } catch (Exception $e) {
            $_SESSION['message'] = $e->getMessage;
            $_SESSION['type'] = "danger";
        }
        
        update("TBL_EMPRESA",["APROVACAO"=>"S"],'ID_EMPRESA', $_POST['submit1']);
       
    }
    else if(isset($_POST['submit2']))
    {
        update("TBL_EMPRESA",["APROVACAO"=>"N"],'ID_EMPRESA', $_POST['submit2']);
        
        if($_SESSION['type']=="success")
        {
            $_SESSION['message']= "Empresa Reprovada com sucesso";
        }
        
    }
    
   
    
    global $fornecedores;
    $busca = find_all('VIEW_FORNECEDORES');
    
    $fornecedores[''] = array();
    $fornecedores['S'] = array();
    $fornecedores['N'] = array();
    
    if(count($busca)>0)
    {
        foreach($busca as $fornecedor)
        {
            
            $fornecedores[$fornecedor['APROVACAO']][] = $fornecedor;
            
        }
    }
}

?>