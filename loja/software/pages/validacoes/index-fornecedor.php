<?php 
require_once 'functions.php';
indexf();
include HEADER;

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Validações de fornecedores</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<form method="post" action="index-fornecedor.php">
<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          		<ul class="nav nav-tabs" role="tablist">
        			 <li class="nav-item">
                        <a class="list-group-item list-group-item-action active" data-toggle="list" href="#home" role="tab" href="#">Solicitados</a>
                      </li>
                       <li class="nav-item">
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#profile" role="tab">Aceitos	</a>
                      </li>
                      <li class="nav-item">
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#messages" role="tab">Rejeitados</a>
                      </li>
        		</ul>
        </div>
        
      <div class="card-body">
       <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Razão social</th>
                  <th>CNPJ</th>
                  <th>Nome representante</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                
                  <?php 
                                foreach ($fornecedores[''] as $fornecedor)
								{
								    
								    $enderecos = find_id("view_enderecos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
								    $contatos = find_id("view_contatos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
						?>
									<tr class="odd gradeX">
										<td><?php echo $fornecedor['RAZAO_SOCIAL']?></td>
										<td> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?> </td>
										<td><?php echo $fornecedor['NOME_RESPONSAVEL']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td style="text-align:center">
                                              					<a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>"><i data-toggle="tooltip" data-placement="left" title="Informações da fornecedora" class="fa fa-info"></i></a>
                            						<?php 
                            								//if(isset($edit))
                            								{
                            							?>
                            									<button data-toggle="tooltip" data-placement="left" title="Validar fornecedora" class="btn btn-sm btn-success" type="submit" id="btn_submit1" name="submit1" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-check"></i></button>
                            							<?php 
                            								}
                            								//if(isset($delete))
                            								{
                            							?>
                            						    		<button  data-toggle="tooltip" data-placement="left" title="Recusar fornecedora"  class="btn btn-sm btn-danger" type="submit" id="btn_submit2" name="submit2" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-remove"></i></button>
                            						    <?php 
                            								}
                            						    ?>
                            									    
                            				  </td>
										<?php 
											}
										?>
										    
									</tr>
							
								
									
									<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p><strong>Razão social:</strong> <?php echo $fornecedor['RAZAO_SOCIAL']?></p>
      	<p><strong>CNPJ:</strong> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?></p>
      	<p><strong>Nome do responsável:</strong> <?php echo $fornecedor['NOME_RESPONSAVEL']?></p>
      	<p><strong>E-mail empresa: </strong><?php echo $fornecedor['EMAIL']?></p>
      	<hr>
      	
      	<h6><strong>Endereço</strong></h6>
	
<div id="accordion">
<?php 	
foreach ($enderecos as $endereco)
{
   // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $endereco['DESCRICAO']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" class="collapse" aria-labelledby="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong>CEP: </strong><?php echo $endereco['CEP']?></p>
      	<p><strong>Cidade:</strong> <?php echo $endereco['MUNICIPIO']?></p>
      	<p><strong>Logradouro:</strong> <?php echo $endereco['LOGRADOURO']?></p>
      	<p><strong>Número:</strong> <?php echo $endereco['NUMERO']?></p>
      	<p><strong>Bairro:</strong> <?php echo $endereco['BAIRRO']?></p>
      	<p><strong>Estado:</strong> <?php echo $endereco['ESTADO']?></p>
      	<p><strong>Complemtno:</strong> <?php if($endereco['COMPLEMENTO']==""){ echo "-";}else{echo $endereco['COMPLEMENTO'];}?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>

      	<hr>
      	<h6><strong>Contato</strong></h6>
      	
<div id="accordion">
<?php 	
foreach ($contatos as $contato)
{
   // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $contato['TIPO_VALOR']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" class="collapse" aria-labelledby="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong><?php echo $contato['TIPO_VALOR']?></strong> : <?php echo $contato['VALOR']?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>
      	
      	   
      </div>
      <div class="modal-footer">
        <button data-toggle="tooltip" data-placement="left" title="Validar fornecedora" class="btn btn-success" type="submit" id="btn_submit1" name="submit1" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-check"></i></button>
        <button data-toggle="tooltip" data-placement="left" title="Recusar fornecedora" class="btn btn-danger" type="submit" id="btn_submit2" name="submit2" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-remove"></i></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
						<?php 
								}
						?>
                
              </tbody>
            </table>
          </div>
          </div>
          
          
          <div class="tab-pane" id="profile" role="tabpanel">
          
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Razão social</th>
                  <th>CNPJ</th>
                  <th>Nome representante</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                
				<?php 
                                foreach ($fornecedores['S'] as $fornecedor)
								{
								    
								    $enderecos = find_id("view_enderecos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
								    $contatos = find_id("view_contatos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
						?>
									<tr class="odd gradeX">
										<td><?php echo $fornecedor['RAZAO_SOCIAL']?></td>
										<td> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?> </td>
										<td><?php echo $fornecedor['NOME_RESPONSAVEL']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td style="text-align:center">
                                                  					<a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>"><i data-toggle="tooltip" data-placement="left" title="Informações da fornecedora" class="fa fa-info"></i></a>
                                						<?php 
                                					
                                								//if(isset($delete))
                                								{
                                							?>
                                						    		<button data-toggle="tooltip" data-placement="left" title="Recusar fornecedora" class="btn btn-sm btn-danger" type="submit" id="btn_submit2" name="submit2" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-remove"></i></button>
                                						    <?php 
                                								}
                                						    ?>
                                									    
                                				  </td>
										<?php 
											}
										?>
										    
									</tr>
									
									<div class="modal fade" id="exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p><strong>Razão social:</strong> <?php echo $fornecedor['RAZAO_SOCIAL']?></p>
      	<p><strong>CNPJ:</strong> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?></p>
      	<p><strong>Nome do responsável:</strong> <?php echo $fornecedor['NOME_RESPONSAVEL']?></p>
      	<p><strong>E-mail empresa: </strong><?php echo $fornecedor['EMAIL']?></p>
      	<hr>
      	<h6><strong>Endereço</strong></h6>
      	<div id="accordion">
<?php 	
foreach ($enderecos as $endereco)
{
   // print_r($endereco);
?>      
  <div class="card">
    <div class="card-header" id="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>">
      <h5 class="mb-0">
        <a class="btn btn-link" >
          <?php echo $endereco['DESCRICAO']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" class="collapse" aria-labelledby="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong>CEP:</strong> <?php echo $endereco['CEP']?></p>
      	<p><strong>Cidade:</strong> <?php echo $endereco['MUNICIPIO']?></p>
      	<p><strong>Logradouro:</strong> <?php echo $endereco['LOGRADOURO']?></p>
      	<p><strong>Número: </strong><?php echo $endereco['NUMERO']?></p>
      	<p><strong>Bairro: </strong> <?php echo $endereco['BAIRRO']?></p>
      	<p><strong>Estado: </strong> <?php echo $endereco['ESTADO']?></p>
      	<p><strong>Complemtno: </strong> <?php if($endereco['COMPLEMENTO']==""){ echo "-";}else{echo $endereco['COMPLEMENTO'];}?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>
      	<hr>
      	<h6><strong>Contato</strong></h6>
      
      <div id="accordion">
<?php 	
foreach ($contatos as $contato)
{
   // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $contato['TIPO_VALOR']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" class="collapse" aria-labelledby="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong><?php echo $contato['TIPO_VALOR']?></strong> : <?php echo $contato['VALOR']?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>
      
      </div>
      <div class="modal-footer">
        <button data-toggle="tooltip" data-placement="left" title="Recusar fornecedora" class="btn btn-danger" type="submit" id="btn_submit2" name="submit2" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-remove"></i></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
						<?php 
								}
						?>                
                
              </tbody>
            </table>
          </div>
          
          </div>
          
          <div class="tab-pane" id="messages" role="tabpanel">
          
          	<div class="table-responsive">
            <table class="table table-bordered" id="dataTable3" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Razão social</th>
                  <th>CNPJ</th>
                  <th>Nome representante</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
              		<?php 
                                foreach ($fornecedores['N'] as $fornecedor)
								{
								    $enderecos = find_id("view_enderecos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
								    $contatos = find_id("view_contatos_fornecedora","ID_EMPRESA",$fornecedor['ID_EMPRESA']);
						?>
									<tr class="odd gradeX">
										<td><?php echo $fornecedor['RAZAO_SOCIAL']?></td>
										<td> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?> </td>
										<td><?php echo $fornecedor['NOME_RESPONSAVEL']?></td>
										
										<?php
											//if($ver || $edit || $delete)
											{
										?>
												<td style="text-align:center">
                                                  					<a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>"><i data-toggle="tooltip" data-placement="left" title="Informações da fornecedora" class="fa fa-info"></i></a>
                                						<?php 
                                								//if(isset($edit))
                                								{
                                							?>
                                									<button data-toggle="tooltip" data-placement="left" title="Validar fornecedora" class="btn btn-sm btn-success" type="submit" id="btn_submit1" name="submit1" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-check"></i></button>
                                							<?php 
                                								}
                                							?>
                                									    
                                				  </td>
										<?php 
											}
										?>
										    
									</tr>
									
<div class="modal fade" id="exampleModal<?php echo $fornecedor['ID_FORNECEDOR']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p><strong>Razão social:</strong> <?php echo $fornecedor['RAZAO_SOCIAL']?></p>
      	<p><strong>CNPJ:</strong> <?php echo mask($fornecedor['CNPJ'],'##.###.###/####-##')?></p>
      	<p><strong>Nome do responsável: </strong><?php echo $fornecedor['NOME_RESPONSAVEL']?></p>
      	<p><strong>E-mail empresa: </strong><?php echo $fornecedor['EMAIL']?></p>
      	<hr>
      	<h6><strong>Endereço</strong></h6>
      	<div id="accordion">
<?php 	
foreach ($enderecos as $endereco)
{
   // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $endereco['DESCRICAO']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" class="collapse" aria-labelledby="headingOne<?php echo $endereco['ID_EMPRESA'];echo $endereco['DESCRICAO']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong>CEP:</strong> <?php echo $endereco['CEP']?></p>
      	<p><strong>Cidade:</strong> <?php echo $endereco['MUNICIPIO']?></p>
      	<p><strong>Logradouro:</strong> <?php echo $endereco['LOGRADOURO']?></p>
      	<p><strong>Número:</strong> <?php echo $endereco['NUMERO']?></p>
      	<p><strong>Bairro:</strong> <?php echo $endereco['BAIRRO']?></p>
      	<p><strong>Estado: </strong><?php echo $endereco['ESTADO']?></p>
      	<p><strong>Complemtno:</strong> <?php if($endereco['COMPLEMENTO']==""){ echo "-";}else{echo $endereco['COMPLEMENTO'];}?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>

      	<hr>
      	<h6><strong>Contato</strong></h6>
      	<div id="accordion">
<?php 	
foreach ($contatos as $contato)
{
   // print_r($endereco);
?>      
 <div class="card">
    <div class="card-header" id="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-toggle="collapse" data-target="#collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" aria-expanded="true" aria-controls="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>">
      <h5 class="mb-0">
        <a class="btn btn-link">
          <?php echo $contato['TIPO_VALOR']?>
        </a>
      </h5>
    </div>

    <div id="collapseOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" class="collapse" aria-labelledby="headingOne<?php echo $contato['ID_EMPRESA'];echo $contato['VALOR']?>" data-parent="#accordion">
      <div class="card-body">
        <p><strong><?php echo $contato['TIPO_VALOR']?></strong> : <?php echo $contato['VALOR']?></p>
      </div>
    </div>
  </div>
  
<?php 
		}
?>
</div>
      
      </div>
      <div class="modal-footer">
      	<button data-toggle="tooltip" data-placement="left" title="Validar fornecedora" class="btn btn-success" type="submit" id="btn_submit1" name="submit1" value="<?php echo $fornecedor['ID_EMPRESA']?>"><i class="fa fa-check"></i></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
						<?php 
								}
						?>
              	
              </tbody>
            </table>
          </div>
          
        </div>
          
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
  </div>
</div>
</form>




<?php 
include FOOTER;
?>


<script>
//$(document).ready(function() {
  //  $('#dataTable1').DataTable({
    //    responsive: true
   // });
    //$('#dataTable2').DataTable({
     //   responsive: true
    //});
    //$('#dataTable3').DataTable({
      //  responsive: true
    //});
//});
</script>