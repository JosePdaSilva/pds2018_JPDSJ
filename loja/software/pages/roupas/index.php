<?php 
require_once 'functions.php';
index();
include HEADER;

?>


<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Roupas</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


		<div class="row">
			<div class="col-lg-1">
				
			</div>
		</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
        	<div class="col-md-11">	
          		<i class="fa fa-table"></i> Roupas cadastradas no sistema
          	</div>
          	<div class="col-md-1">
          		<a data-toggle="tooltip" data-placement="left" title="Cadastrar um novo usuário"
          			href="<?php echo BASEURL?>pages/roupas/add.php"
					class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
					class="fa fa-plus"></i>
				</a>
          	</div>
          </div>
         </div>
          
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Descrição</th>
                  <th>Marca</th>
                  <th>Tamanhos</th>
                  <th>Preço Unit.</th>
                  <th>Opções</th>
                </tr>
              </thead>
 			  <tbody>
               
               <?php 
							foreach ($obras as $obra)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $obra['DESCRICAO']?></td>
										<td><?php echo $obra['MARCA']?></td>
										<td><?php echo $obra['TAMANHOS'] ?></td>
										<td>R$ <?php echo number_format($obra['PRECO_UNITARIO'],2,",",".");?></td>
										
												<td class="text-center">
															<a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal<?php echo $obra['ID_PRODUTO']?>"><i data-toggle="tooltip" data-placement="left" title="Informações da obra" class="fa fa-info"></i></a>
															<a href="edit.php?id=<?php echo $obra['ID_PRODUTO']?>" class="btn btn-sm btn-warning" ><i data-toggle="tooltip" data-placement="left" title="Informações da obra" class="fa fa-edit"></i></a>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $obra['ID_PRODUTO']?>" data-id="<?php echo $obra['ID_PRODUTO']?>" data-obra="<?php echo $obra['DESCRICAO']?>" data-tipo='OBRA'><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="left" title="Exclua a obra selecionada" ></i></a>
												</td>
									</tr>
									
<!-- Modal -->
<div class="modal fade" id="exampleModal<?php echo $obra['ID_PRODUTO']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações da roupa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="text-center">
        <img src="data:image/png;base64,<?php echo base64_encode($obra['FOTO']);?>" alt="FOTO DA ROUPA" style="height: 200px">
      </div>
      <div class="text-center">
      <p><strong>Descrição:</strong> <?php echo $obra['DESCRICAO']?></p>
      <p><strong>Marca:</strong> <?php echo $obra['MARCA']?></p>
      <p><strong>Tamanhos disponíveis:</strong> <?php echo $obra['TAMANHOS']?></p>
      <p><strong>Público alvo:</strong> <?php if($obra['SEXO']=='M')echo 'Masculino'; elseif($obra['SEXO']=='F') echo 'Feminino';else echo 'Unissex';?></p>
      <p><strong>Preço Unitário:</strong> R$ <?php echo number_format($obra['PRECO_UNITARIO'],2,",",".")?></p>
      
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
						<?php 
								}
						?>
                
              </tbody>
              
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>