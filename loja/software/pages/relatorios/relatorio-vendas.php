<?php 
require_once 'functions.php';
relatorio("faturamento");

include HEADER;

if (!isset($_POST['submit']))
{
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Relatório de Vendas</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
    	<div class="col-md-12">
    		<form action="" method="post">
    			<div class="row">
    						<div class="col-md-3 offset-md-3">
    							<div id="div_campo_data_inicio" class="form-group has-feedback">
    								<div class="row">
    									<div class="col-md-12">
    										<label for="campo_data_inicio">Data de Início</label> <i
    											class="" id="icon_campo_data_inicio"></i> <i
    											class="fa fa-1-5x fa-question-circle-o pull-right"
    											data-toggle="tooltip" data-placement="left"
    											title="Deve ser informado a data de início"></i>
    									</div>
    								</div>
    
    
    								<div class="row">
    									<div class="col-md-12">
    										<input type="date" class="form-control" id="campo_data_inicio"
    											name="relatorio[DATA_INICIO]" required
    											oninput="verificar_data_inicio();" />
    									</div>
    								</div>
    
    								<div class="row">
    									<div class="col-md-12">
    										<span class="font-weight-light" style="color: red"
    											id="resposta_campo_data_inicio"></span>
    									</div>
    								</div>
    							</div>
    						</div>
    						
    						<div class="col-md-3">
    							<div id="div_campo_data_fim" class="form-group has-feedback">
    								<div class="row">
    									<div class="col-md-12">
    										<label for="campo_data_fim">Data de Término</label> <i
    											class="" id="icon_campo_data_fim"></i> <i
    											class="fa fa-1-5x fa-question-circle-o pull-right"
    											data-toggle="tooltip" data-placement="left"
    											title="Deve ser informada a data de término da pesquisa"></i>
    									</div>
    								</div>
    
    
    								<div class="row">
    									<div class="col-md-12">
    										<input type="date" class="form-control" id="campo_data_fim"
    											name="relatorio[DATA_FIM]" required
    											oninput="verificar_termino();" />
    									</div>
    								</div>
    
    								<div class="row">
    									<div class="col-md-12">
    										<span class="font-weight-light" style="color: red"
    											id="resposta_campo_data_fim"></span>
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    
    					<div class="row">
    						<div class="col-md-3 offset-md-5">
    
    							<button type="submit" name="submit"
    								class="btn btn-primary btn-block">
    								<i class="fa fa-check-square"></i> Pesquisar
    							</button>
    
    						</div>
    
    					</div>
    		</form>
    	</div>
    </div>

<?php
}
else
{
    //$representacao = $_POST["relatorio"]["REPRESENTACAO"];
?>
  <div class="row">
    	<div class="col-md-10 offset-md-1">
    		<button class="pull-right nao_imprimir" onclick="window.print();"
    			style="width: 50px; height: 50px;">
    			<i style="font-size: 30px;" class="fa fa-print"></i>
    		</button>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-10 offset-md-1">
    		<div class="row" style="text-align: center;margin-bottom:15px;">
    					<div class="col-md-12">
            				<h4>
            					Relatório de Faturamento
            				</h4>	
            				<p>
            					<strong>Período:  </strong><?php $dt_i= new DateTime($_POST["relatorio"]['DATA_INICIO']); echo $dt_i->format('d/m/Y');?>  <strong>à</strong>  <?php $dt_f= new DateTime($_POST["relatorio"]['DATA_FIM']); echo $dt_f->format('d/m/Y'); ?>
            				</p>
    					</div>
    		</div>
    		
    		
    		
						<div class="panel panel-default">
                    		<div class="panel-heading" style="text-align:center"><?php echo 'QUALQUER COISA'?></div>
                    		<!-- /.panel-heading -->
                    		<div class="panel-body">
                       			<table width="100%"
                    				class="table table-striped table-bordered table-hover"
                    				id="dataTables-example">
                    				<thead>
                    					<tr>
                    						<th>Data venda/compra</th>
                    						<th>Comprador</th>
                    						<th>Forneceodr</th>
                    						<th>Produto</th>
                    						<th>Valor venda</th>
                    					</tr>
                    				</thead>
                    				<tbody>
                    					<?php 
                                            $total = 0;
                                            foreach ($relatorios as $fatura)
                    						{
                    						    $data_ini = new DateTime($_POST["relatorio"]['DATA_INICIO']);
                    						    $data_fim = new DateTime($_POST["relatorio"]['DATA_FIM']);						
                    			        ?>
                        						<tr class="odd gradeX">
                    								<td><?php $dt_i= new DateTime($fatura['DATA']); echo $dt_i->format('d/m/Y'); ?></td>
                    								<td><?php echo $fatura['ID_FATURA']?></td>
                    								<td><?php echo $fatura['ID_FATURA']?></td>
                    								<td><?php echo $fatura['ID_FATURA']?></td>
                    								<td>R$ <span class="pull-right"><?php echo number_format($fatura['VALOR_FATURA'], 2, ',', '.')?></span></td>
                    							</tr>
                        				<?php
                        				        $total+=$fatura['VALOR_FATURA'];
                    						}
                                            $total_geral+=$total;
                    					?>
                    						<tr>
                								<td colspan="2" style="text-align:right"><strong>Total das Faturas - <?php echo $operadora['RAZAO_SOCIAL']?></strong></td>
                								<td>R$ <span class="pull-right"><?php echo number_format($total, 2, ',', '.')?></span></td>
                							</tr>
                    				</tbody>
                    			</table>
                    			<!-- /.table-responsive -->
                			</div>
                    		<!-- /.panel-body -->
                    	</div>
    		<div class="col-md-4 offset-md-4">
    			<a href="faturamentos.php"
    				class="btn btn-primary btn-block nao_imprimir">
    					<i class="fa fa-arrow-left"></i> Voltar
    			</a>
    		</div>
    		<br>
    	</div>
    	<!-- /.col-lg-12 -->
    </div>


<?php 
}
include FOOTER;
?>