<?php
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function relatorio($tipo)
{
    if(isset($_POST['submit']))
    {        
        global $relatorios;
        $fields = $_POST['relatorio'];
        $relatorios = array();
        if($tipo=="recebimento")
        {
            $relatorios = relatorios("VIEW_RELATORIO_RECEBIMENTOS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="faturamento")
        {
            $relatorios = relatorios("VIEW_ORCAMENTOS", $fields["DATA_INICIO"], $fields["DATA_FIM"]);
        }
        else if($tipo=="gtoAReceber")
        {
            $relatorios = relatorios("VIEW_GTO_NAO_PAGAS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="glosa")
        {
            $relatorios = relatorios("VIEW_GLOSAS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="comparativoFaturamentosXRecebimentos")
        {
            $relatorios = relatorios("VIEW_RELATORIO_FATURASXRECEBIMENTOS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="comparativoGlosasXRecebimentos")
        {
            $relatorios = relatorios("VIEW_RELATORIO_GLOSASXRECEBIMENTOS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="MediaDias")
        {
            $relatorios = relatorios("VIEW_RELATORIO_MEDIA_DIAS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        else if($tipo=="comparativoFaturasXGlosas")
        {
            $relatorios = relatorios("VIEW_RELATORIO_FATURASXGLOSASXRECEBIMENTOS", $fields["ID_CLINICA"], $fields["DATA_INICIO"], $fields["DATA_FIM"], $operadoras);
        }
        

    }
    else
    {
       // global $clinicas, $operadoras;
       // $clinicas = find_all('VIEW_CLINICAS');
       // $operadoras = find_all('VIEW_OPERADORAS');
    }
}
?>