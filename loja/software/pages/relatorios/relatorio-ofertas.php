<?php
require_once 'functions.php';
index();
include HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Relatório de Ofertas</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
    	<div class="col-md-12">
    		<form action="" method="post">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="row">
    						<div class="col-md-6">
    							<div id="div_campo_clinica" class="form-group has-feedback">
    								<div class="row">
    									<div class="col-md-12">
    										<label for="campo_clinica">Fornecedor *</label> <i class=""
    											id="icon_campo_clinica"></i>
    										<!-- icone de certo ou errado -->
    										<i class="fa fa-1-5x fa-question-circle-o pull-right"
    											data-toggle="tooltip" data-placement="left"
    											title="Deve ser informada o fornecedor"></i>
    									</div>
    								</div>
    
    
    								<div class="row">
    									<div class="col-md-12">
    										<select id="campo_clinica" class="form-control"
    											name="relatorio[ID_CLINICA]" autofocus required
    											onchange="verificar_razao_social()">
    											<option>Todos </option>
    											<?php
                                                    if (count($clinicas) > 0)
                                                    {
                                                        foreach ($clinicas as $clinica)
                                                        {
                                                ?>
    														<option value="<?php echo $clinica['ID_CLINICA']?>"><?php echo $clinica['RAZAO_SOCIAL']?></option>
    											<?php
                                                        }
                                                    }
                                                    else
                                                    {
                                                ?>
    													<!-- <option value="">Não Existem Clínicas Cadastradas</option> -->
    											<?php
                                                    }
                                                ?>
    										</select>
    									</div>
    								</div>
    
    								<div class="row">
    									<div class="col-md-12">
    										<span class="font-weight-light" style="color: red"
    											id="resposta_campo_clinica"></span>
    										<!-- se icone errado ou warning, o motivo -->
    									</div>
    								</div>
    							</div>
    						</div>
    						
    						<div class="col-md-3">
    							<div id="div_campo_data_inicio" class="form-group has-feedback">
    								<div class="row">
    									<div class="col-md-12">
    										<label for="campo_data_inicio">Data de Início</label> <i
    											class="" id="icon_campo_data_inicio"></i> <i
    											class="fa fa-1-5x fa-question-circle-o pull-right"
    											data-toggle="tooltip" data-placement="left"
    											title="Deve ser informado a data de início"></i>
    									</div>
    								</div>
    
    
    								<div class="row">
    									<div class="col-md-12">
    										<input type="date" class="form-control" id="campo_data_inicio"
    											name="relatorio[DATA_INICIO]" required
    											oninput="verificar_data_inicio();" />
    									</div>
    								</div>
    
    								<div class="row">
    									<div class="col-md-12">
    										<span class="font-weight-light" style="color: red"
    											id="resposta_campo_data_inicio"></span>
    									</div>
    								</div>
    							</div>
    						</div>
    						
    						<div class="col-md-3">
    							<div id="div_campo_data_fim" class="form-group has-feedback">
    								<div class="row">
    									<div class="col-md-12">
    										<label for="campo_data_fim">Data de Término</label> <i
    											class="" id="icon_campo_data_fim"></i> <i
    											class="fa fa-1-5x fa-question-circle-o pull-right"
    											data-toggle="tooltip" data-placement="left"
    											title="Deve ser informada a data de término da pesquisa"></i>
    									</div>
    								</div>
    
    
    								<div class="row">
    									<div class="col-md-12">
    										<input type="date" class="form-control" id="campo_data_fim"
    											name="relatorio[DATA_FIM]" required
    											oninput="verificar_termino();" />
    									</div>
    								</div>
    
    								<div class="row">
    									<div class="col-md-12">
    										<span class="font-weight-light" style="color: red"
    											id="resposta_campo_data_fim"></span>
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    
    					<div class="row">
    						<div class="col-md-3 offset-md-5">
    
    							<button type="submit" name="submit"
    								class="btn btn-primary btn-block">
    								<i class="fa fa-check-square"></i> Pesquisar
    							</button>
    
    						</div>
    
    					</div>
    				</div>
    			</div>
    		</form>
    	</div>
    </div>


<?php 
include FOOTER;
?>