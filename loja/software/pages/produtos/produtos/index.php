<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/produtos/produtos/add.php']) && $menu_acessos['/pages/produtos/produtos/add.php']);
$edit = (isset($menu_acessos['/pages/produtos/produtos/edit.php']) && $menu_acessos['/pages/produtos/produtos/edit.php']);
$delete = (isset($menu_acessos['/pages/produtos/produtos/delete.php']) && $menu_acessos['/pages/produtos/produtos/delete.php']);


?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Produtos</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
        <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Produtos cadastrados
          </div>
              <?php 
	   if($add)
	   {
	?>
          <div class="col-md-1">
		    <a data-toggle="tooltip" data-placement="left" title="Cadastrar um novo produto" href="<?php echo BASEURL?>pages/produtos/produtos/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>      
          </div>
          <?php 
	   }
          ?>
          </div>
          </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome do setor</th>
                  <th>Produto</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
              	 
               <?php 
                                foreach ($sps as $sp)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $sp['SETOR']?></td>
										<td><?php echo $sp['NOME_PRODUTO']?></td>
										<?php
											if($edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													 
														if($edit)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Editar produto selecionado" href="edit.php?id=<?php echo $sp['ID_SETOR_PRODUTO'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $sp['ID_SETOR_PRODUTO'];?>" data-id="<?php echo $sp['ID_SETOR_PRODUTO'];?>" data-produto="<?php echo $sp['NOME_PRODUTO'];?>" data-tipo='PRODUTO'><i data-toggle="tooltip" data-placement="left" title="Excluir produto selecionado" class="fa fa-trash-o"></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
	</div>
</div>


<?php 

include DELETEMODAL;
include FOOTER;
?>