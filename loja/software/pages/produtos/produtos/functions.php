<?php 
require_once '../../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $sps;
	$sps = find_all('VIEW_SETOR_PRODUTO');
	
}

function view($id)
{
    if(isset($id))
    {
        global $produtos;
        $result = find_id('VIEW_SETOR_PRODUTO','ID_SETOR_PRODUTO',$id);
        if(count($result)!=0)
        {
            $produtos = $result[0];
        }
        else
        {
            echo "<script>alert('ID Não Encontrado');</script>";
            $_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
            $_SESSION['type'] = 'danger';
            header('Location: index.php');//
            exit;
        }
        
    }
    else
    {
        echo "<script>alert('ID não encontrado');</script>";
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
        
    }
}

function add()
{
	if(isset($_POST['submit']))
	{
		$produto = $_POST['produto'];
		
		$all = find_all('tbl_setor_produto');
		
		$already_exists=false;
		
		//echo $setor['SETOR'];
		
		foreach ($all as $set)
		{
		    //echo $set['SETOR'];
		    if(strtolower($set['NOME_PRODUTO'])==strtolower($produto['NOME_PRODUTO']))
			{
				$already_exists = true;
				//echo "existe";
				break;
			}
		}
		
		if($already_exists)
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Este Setor Já Existe";
		}
		else
		{
		    save('tbl_setor_produto', $produto);
			
			if($_SESSION['type']=="success")
			{
			    $_SESSION['message']= "Produto adicionado com sucesso";
			    header('Location: index.php');
			    exit;
			}
		}
	}
	global $setores;
	$setores = find_all('tbl_setor ');
}


function edit($id)
{
    if(isset($id))
    {
        if(isset($_POST['submit']))
        {
            $produto = $_POST['produto'];
            
            $all = find_all('VIEW_SETOR_PRODUTO');
            
            $already_exists=false;
            foreach ($all as $prod)
            {
                if(strtolower($prod['NOME_PRODUTO'])==strtolower($produto["NOME_PRODUTO"]))
                {
                    echo $prod['NOME_PRODUTO']."-".$produto['NOME_PRODUTO'];
                    exit();
                    $already_exists = true;
                    break;
                }
            }
            
            if($already_exists)
            {
                echo "<script>alert('Produto Ja Existente');</script>";
                $_SESSION['type']= "danger";
                $_SESSION['message']= "Este nome de usuário ou E-Mail já existe";
                
            }
            else
            {
                
                update('TBL_SETOR_PRODUTO', $produto, 'ID_SETOR_PRODUTO', $id);
                
                if($_SESSION['type']=="success")
                {
                    
                    echo "<script>alert('Setor Cadastrado com Sucesso');</script>";
                    $_SESSION['message']= "Usuário alterado com sucesso";
                    header('Location: index.php');
                    exit;
                }
            }
        }
        global $setores;
        $setores = find_all("tbl_setor");
        view($id);
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
    }
}

function delete($id)
{
    if(isset($id))
    {
        remove('TBL_SETOR_PRODUTO', 'ID_SETOR_PRODUTO', $id);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Produto excluido com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}