<?php 
require_once 'functions.php';
add();
include HEADER;

?>


<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de produtos </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-4 offset-md-4">
		<form action="add.php" method="post" role="form">

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione o setor*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado um setor"></i> 
							<select
							class="form-control" name="produto[ID_SETOR]">
							<?php
							   foreach ($setores as $setor) 
							  {
					        ?>
									<option value="<?php echo $setor['ID_SETOR']?>"><?php echo $setor['SETOR']?></option>
							<?php
							    }
						    ?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Nome do produto*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o nome do produto"></i> 
							
							<input type="text" class="form-control" id="campo_senha"
							name="produto[NOME_PRODUTO]" placeholder="Nome do Produto" required
							/>
							
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

