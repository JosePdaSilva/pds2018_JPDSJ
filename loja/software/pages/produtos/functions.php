<?php 
require_once '../../config.php';
//require_once '../../verify.php';

function index()
{
	global $produtos;
	$produtos = find_all('TBL_PRODUTOS');
	
}

function view($id)
{
	if(isset($id))
	{
		global $perfil;
		$result = find_id('PERFIL_WEB','ID_PERFIL',$id);
		if(count($result)!=0)
		{
			$perfil = $result[0];
		}
		else 
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: ver-perfis.php');//
			exit;
		}
		
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: ver-perfil.php');//
		exit;
		
	}
}

function add()
{
	
	if(isset($_POST['submit']))
	{
		$perfil = $_POST['perfil'];
		
		$all = find_all('PERFIL_WEB');
		
		$already_exists=false;
		foreach ($all as $perfil_cad)
		{
			if(strtolower($perfil_cad['PERFIL'])==strtolower($perfil["'PERFIL'"]))
			{
				$already_exists = true;
				break;
			}
		}
		
		if($already_exists)
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Este Perfil Já Existe existe";
			
		}
		else
		{
			save('PERFIL_WEB', $perfil);
			
			
			if($_SESSION['type']=="success")
			{
				$res = find_id('PERFIL_WEB', 'PERFIL', $perfil["'PERFIL'"]);
				$id = $res[0];
				
				$acessosPagina= array();
				foreach ($_POST['acesso'] as $acesso)
				{
				    $acessosPagina[] = array
								(
									'ID_PAGINA'=>$acesso,
									'ID_PERFIL'=>$id['ID_PERFIL']
								);
				}
				$acessoSistema = array();
				foreach ($_POST['acessoSistema'] as $acesso)
				{
				    $acessoSistema[] = array
				    (
				        'ID_SISTEMA'=>$acesso,
				        'ID_PERFIL'=>$id['ID_PERFIL']
				    );
				}
				
				save_acessos($acessosPagina, $acessoSistema);
				if($_SESSION['type']=="success")
				{
    				$_SESSION['message']= "Perfil adicionado com sucesso";
    				header('Location: ver-perfis.php');
    				exit;
				}
				
			}
		}
	}
	global $paginas;
	$busca = find_all('VIEW_PAGINAS_WEB');
	
	foreach($busca as $pagina)
	{
		$str="";
		$arr=array();
		$arr = explode('/',$pagina['URL']);
		if(count($arr)>1)
		{
			unset($arr[count($arr)-1]);
			$str = implode('/', $arr);
		}
		else
			$str = "/";
			
			$paginas[$pagina['ID_SISTEMA']]['paginas'][$str][] = $pagina;
			$paginas[$pagina['ID_SISTEMA']]['info'] = array("ID_SISTEMA"=>$pagina['ID_SISTEMA'], "SIGLA_SISTEMA"=>$pagina['SIGLA_SISTEMA'], "NOME_SISTEMA"=>$pagina['NOME_SISTEMA']);
	}
}


function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			
			$perfil = $_POST['perfil'];
			
			$all = find_all('PERFIL_WEB');
			
			$already_exists=false;
			foreach ($all as $perfil_cad)
			{
				if(strtolower($perfil_cad['PERFIL'])==strtolower($perfil["'PERFIL'"]) && $perfil_cad['ID_PERFIL']!=$id)
				{
					$already_exists = true;
					break;
				}
			}
			
			if($already_exists)
			{
				$_SESSION['type']= "danger";
				$_SESSION['message']= "Este Perfil Já Existe existe";
			}
			else
			{
			
				update('PERFIL_WEB', $perfil, 'ID_PERFIL', $id);
				
				if($_SESSION['type']=="success")
				{
					remove('ACESSO_PAGINAS_WEB', 'ID_PERFIL', $id);
					remove('ACESSO_SISTEMAS_WEB', 'ID_PERFIL', $id);
					$acessoPagina= array();
					foreach ($_POST['acesso'] as $acesso)
					{
					    $acessoPagina[] = array
						(
								'ID_PAGINA'=>$acesso,
								'ID_PERFIL'=>$id
						);
						
						
					}
					$acessoSistema = array();
					foreach ($_POST['acessoSistema'] as $acesso)
					{
					    $acessoSistema[] = array
					    (
					        'ID_SISTEMA'=>$acesso,
					        'ID_PERFIL'=>$id
					    );
					}
					
					save_acessos($acessoPagina, $acessoSistema);
					if($_SESSION['type']=="success")
					{
					    $_SESSION['message']= "Perfil editado com sucesso";
					    header('Location: ver-perfis.php');
					    exit;
					}
				}
			}
		}
		view($id);
		global $paginas;
		$busca = find_all('VIEW_PAGINAS_WEB');
		
		foreach($busca as $pagina)
		{
		    $str="";
		    $arr=array();
		    $arr = explode('/',$pagina['URL']);
		    if(count($arr)>1)
		    {
		        unset($arr[count($arr)-1]);
		        $str = implode('/', $arr);
		    }
		    else
		        $str = "/";
		        
		        $paginas[$pagina['ID_SISTEMA']]['paginas'][$str][] = $pagina;
		        $paginas[$pagina['ID_SISTEMA']]['info'] = array("ID_SISTEMA"=>$pagina['ID_SISTEMA'], "SIGLA_SISTEMA"=>$pagina['SIGLA_SISTEMA'], "NOME_SISTEMA"=>$pagina['NOME_SISTEMA']);
		}
	}
	else 
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: ver-perfil.php');//
		exit;
	}
}

function delete($id)
{
	if(isset($id))
	{
		
		$usuarios = find_id('USUARIOS_WEB', 'ID_PERFIL', $id);
		if(count($usuarios)==0)
		{
			remove('PERFIL_WEB', 'ID_PERFIL', $id);
			if($_SESSION['type']=="success")
			{
				if($_SESSION['ID_PERFIL']==$id)
				{
					header('Location: ver-perfis.php');
					exit;
				}
				else
				{
					$_SESSION['message']= "Perfil excluido com sucesso";
					header('Location: ver-perfis.php');
					exit;
				}
			}
			else
			{
				header('Location: ver-perfis.php');
				exit;
			}
		}
		else
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Não é possível deletar. Há usuários associados a este perfil";
			header('Location: ver-perfis.php');
			exit;
		}
		
		
	}
	else
	{
		$_SESSION['message'] = "não foi possível deletar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: ver-perfis.php');
		exit;
	}
}