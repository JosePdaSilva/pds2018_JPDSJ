<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/produtos/setores/add.php']) && $menu_acessos['/pages/produtos/setores/add.php']);
$edit = (isset($menu_acessos['/pages/produtos/setores/edit.php']) && $menu_acessos['/pages/produtos/setores/edit.php']);
$delete = (isset($menu_acessos['/pages/produtos/setores/delete.php']) && $menu_acessos['/pages/produtos/setores/delete.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Setores</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
        <div class="row">
        <div class="col-md-11">
          <i class="fa fa-table"></i> Setores cadastrados
         </div>
            <?php 
	   if($add)
	   {
	?>
         <div class="col-md-1">
    		<a data-toggle="tooltip" data-placement="left" title="Cadastrar novo setor" href="<?php echo BASEURL?>pages/produtos/setores/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>
         </div>
         <?php 
	   }
         ?>
          </div>
          </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome do setor</th>
                  <th>Cor para gráfico</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
              	 
               <?php 
                                foreach ($setores as $setor)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $setor['SETOR']?></td>
										<td><button type="button" style="background-color:<?php echo $setor['COR_GRAFICO']?>; padding:10px 30px"></button></td>
										<?php
											if($edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													 
														if($edit)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Editar setor selecionado" href="edit.php?id=<?php echo $setor['ID_SETOR'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $setor['ID_SETOR']?>" data-id="<?php echo $setor['ID_SETOR']?>" data-setor="<?php echo $setor['SETOR']?>" data-tipo='SETOR'><i data-toggle="tooltip" data-placement="left" title="Excluir setor selecionado" class="fa fa-trash-o"></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>