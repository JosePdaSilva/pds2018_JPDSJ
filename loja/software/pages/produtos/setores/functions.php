<?php 
require_once '../../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $setores;
	$setores = find_all('TBL_SETOR');
	
}

function view($id)
{
	if(isset($id))
	{
		global $setor;
		$result = find_id('tbl_setor','ID_SETOR',$id);
		if(count($result)!=0)
		{
			$setor = $result[0];
		}
		else 
		{
		    echo "<script>alert('ID Não Encontrado');</script>";
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		
	}
	else
	{
	    echo "<script>alert('ID não encontrado');</script>";
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
		
	}
}

function add()
{
	if(isset($_POST['submit']))
	{
	    $setor = $_POST['setor'];
	    
	    $all = find_all('tbl_setor');
	    
	    $already_exists=false;
	    
	    foreach ($all as $set)
	    {
	        if(strtolower($set['SETOR'])==strtolower($setor['SETOR']))
	        {
	            $already_exists = true;
	            break;
	        }
	    }
	    
	    if($already_exists)
	    {
	        $_SESSION['type']= "danger";
	        $_SESSION['message']= "Este Setor Já Existe";
	        // fazer um modal para se existe e tals
	    }
	    else
	    {
	        save('tbl_setor', $setor);
	        
	        if($_SESSION['type']=="success")
	        {
	        $_SESSION['message']= "Setor adicionado com sucesso";
	        header('Location: index.php');
	        exit;
	        }
	    }
	}
}


function edit($id)
{
    if(isset($id))
    {
        if(isset($_POST['submit']))
        {
            $setor = $_POST['setor'];
            
            $all = find_all('TBL_SETOR');
            
            $already_exists=false;
            foreach ($all as $set)
            {
                if(strtolower($set['SETOR'])==strtolower($setor["SETOR"]) && $id!=$set['ID_SETOR'])
                {
                   // echo $set['SETOR']."-".$set['SETOR']."\n".$setor["SETOR"]."-".$id;
                    //exit();
                    $already_exists = true;
                    break;
                }
            }
            
            if($already_exists)
            {
                echo "<script>alert('Setor Ja Existente');</script>";
                $_SESSION['type']= "danger";
                $_SESSION['message']= "Este nome de usuário ou E-Mail já existe";
                
            }
            else
            {
                
                update('TBL_SETOR', $setor, 'ID_SETOR', $id);
      
                if($_SESSION['type']=="success")
                {
                    
                    echo "<script>alert('Setor Cadastrado com Sucesso');</script>";
                    $_SESSION['message']= "Setor alterado com sucesso";
                    header('Location: index.php');
                    exit;
                }
            }
        }
        view($id);
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
    }
}




function delete($id)
{
    if(isset($id))
    {
        remove('TBL_SETOR', 'ID_SETOR', $id);
        if($_SESSION['type']=="success")
        {
           
                $_SESSION['message']= "Setor excluido com sucesso";
                header('Location: index.php');
                exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}