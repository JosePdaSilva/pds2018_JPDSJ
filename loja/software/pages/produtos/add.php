<?php 
require_once 'functions.php';
index();
include HEADER;


$descprods = find_all("TBL_DESC_PROD");
$setores = find_all("TBL_SETOR");
$medida = find_all("TBL_UM");
?>





<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de Produtos </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-4 offset-md-4">
		<form action="add.php" method="post" role="form">

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione o Setor*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado um perfil"></i> 
							<select
							class="form-control" name="usuario[ID_PERFIL]">
							<?php
							   foreach ($setores as $setor) 
							  {
					        ?>
									<option value="<?php echo $setor['ID_SETOR']?>"><?php echo $setor['SETOR']?></option>
							<?php
							    }
						    ?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_usuario" class="form-group has-feedback">
						<label for="campo_usuario">Produto *</label> <i class=""
							id="icon_campo_usuario"></i>
						<select
							class="form-control" name="usuario[ID_PERFIL]">
							<?php
							   foreach ($descprods as $dp) 
							  {
					        ?>
									<option value="<?php echo $dp['ID_DESCO_PROD']?>"><?php echo $dp['NOME_PRODUTO']?></option>
							<?php
							    }
						    ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_senha" class="form-group has-feedback">
						<label for="campo_senha">Especificação *</label> <i class=""
							id="icon_campo_senha"></i>
						<!-- icone de certo ou errado -->
						<span class="font-weight-light" style="color: red"
							id="resposta_campo_senha"></span>
						<!-- se icone errado ou warning, o motivo -->
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informada a especificação do produto"></i>

						<input type="password" class="form-control" id="campo_senha"
							name="usuario[PASSWORD]" placeholder="Especificação" required
							oninput="verificar_senha();" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Unidade de Medida *</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado a unidade de medida"></i> 
							
							<select
							class="form-control" name="usuario[ID_PERFIL]">
							<?php
							   foreach ($medida as $um) 
							  {
					        ?>
									<option value="<?php echo $um['ID_UM']?>"><?php echo $um['UM']?></option>
							<?php
							    }
						    ?>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

