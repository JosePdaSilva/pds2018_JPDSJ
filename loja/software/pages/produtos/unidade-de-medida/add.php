<?php 
require_once 'functions.php';
add();
include HEADER;

?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de unidade de medida </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-4 offset-md-4">
		<form action="add.php" method="post" role="form">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_senha" class="form-group has-feedback">
						<label for="campo_senha">Unidade de medida*</label> <i class=""
							id="icon_campo_senha"></i>
						<span class="font-weight-light" style="color: red"
							id="resposta_campo_senha"></span>
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o nome da unidade de medida"></i>

						<input type="text" class="form-control" id="campo_senha"
							name="um[UM]" placeholder="Unidade de medida" required />
							
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

