<?php 
require_once '../../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $unidadeMedida;
	$unidadeMedida = find_all('TBL_UM');
	
}

function view($id)
{
	if(isset($id))
	{
		global $um;
		$result = find_id('TBL_UM','ID_UM',$id);
		if(count($result)!=0)
		{
		    $um = $result[0];
		}
		else 
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
		
	}
}

function add()
{
	
	if(isset($_POST['submit']))
	{
		$unidade = $_POST['um'];
		
		$all = find_all('TBL_UM');
		
		$already_exists=false;
		
		foreach ($all as $perfil_cad)
		{
		    if(strtolower($perfil_cad['UM'])==strtolower($unidade['UM']))
			{
				$already_exists = true;
				break;
			}
		}
		
		if($already_exists)
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Esta Unidade de Medida Já Existe existe";
			
		}
		else
		{
		    save('TBL_UM', $unidade);
			
			
		    if($_SESSION['type']=="success")
		    {
		        $_SESSION['message']= "Unidade de Medida adicionada com sucesso";
		        header('Location: index.php');
		        exit;
		    }
		}
	}
}



function edit($id)
{
    if(isset($id))
    {
        if(isset($_POST['submit']))
        {
            $um = $_POST['um'];
            
            $all = find_all('TBL_UM');
            
            $already_exists=false;
            
            foreach ($all as $unidade)
            {
                if(strtolower($unidade['UM'])==strtolower($um["UM"]))
                {
                    echo $unidade['UM']."-".$um['UM'];
                    exit();
                    $already_exists = true;
                    break;
                }
            }
            
            if($already_exists)
            {
                echo "<script>alert('Unidade Ja Existente');</script>";
                $_SESSION['type']= "danger";
                $_SESSION['message']= "Este nome de usuário ou E-Mail já existe";
                
            }
            else
            {
                
                update('TBL_UM', $um, 'ID_UM', $id);
                
                if($_SESSION['type']=="success")
                {
                    
                    echo "<script>alert('UM Cadastrado com Sucesso');</script>";
                    $_SESSION['message']= "Usuário alterado com sucesso";
                    header('Location: index.php');
                    exit;
                }
            }
        }
        view($id);
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
    }
}

function delete($id)
{
    if(isset($id))
    {
        remove('TBL_UM', 'ID_UM', $id);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Unidade de Medida excluida com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}