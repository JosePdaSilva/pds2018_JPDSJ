<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/produtos/unidade-de-medida/add.php']) && $menu_acessos['/pages/produtos/unidade-de-medida/add.php']);
$edit = (isset($menu_acessos['/pages/produtos/unidade-de-medida/edit.php']) && $menu_acessos['/pages/produtos/unidade-de-medida/edit.php']);
$delete = (isset($menu_acessos['/pages/produtos/unidade-de-medida/delete.php']) && $menu_acessos['/pages/produtos/unidade-de-medida/delete.php']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Unidades de medida</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
        <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Unidades de medida cadastradas
          </div>
                <?php 
	   if($add)
	   {
	?>
          <div class="col-md-1">
     		 <a data-toggle="tooltip" data-placement="left" title="Cadastrar uma nova unidade de medida" href="<?php echo BASEURL?>pages/produtos/unidade-de-medida/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>
          </div>
          <?php 
	   }
          ?>
          
          </div>
          </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Unidade de medida</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
              	 
               <?php 
                                foreach ($unidadeMedida as $um)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $um['UM']?></td>
										
										<?php
											if($edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													 
														if($edit)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Editar unidade de medida selecionada" href="edit.php?id=<?php echo $um['ID_UM'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $um['ID_UM'];?>" data-id="<?php echo $um['ID_UM'];?>" data-um="<?php echo $um['UM'];?>" data-tipo='UM'><i data-toggle="tooltip" data-placement="left" title="Excluir unidade de medida selecionada" class="fa fa-trash-o"></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      	<p>Razão Social:</p>
      	<p>CNPJ:</p>
      	<p>Nome do Responsável:</p>
      	<hr>
      	<h6>Endereço</h6>
      	<p>CEP:</p>
      	<p>Cidade: </p>
      	<p>Logradouro:</p>
      	<p>Número:</p>
      	<p>Bairro:</p>
      	<p>	Estado</p>
      	<hr>
      	<h6>Contato</h6>
      	<p>Opção: </p>
      	<p>Tipo: </p>
      	<p>Valor: </p>
      
      </div>
      <div class="modal-footer">
        <a href="edit.php?id=<?php //echo $perfil['ID'];?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
        <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $perfil['ID']?>" data-id="<?php echo $perfil['ID']?>" data-perfil="<?php echo $perfil['PERFIL']?>" data-tipo='PERFIL'><i class="fa fa-remove"></i></a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<?php 

include DELETEMODAL;
include FOOTER;
?>
