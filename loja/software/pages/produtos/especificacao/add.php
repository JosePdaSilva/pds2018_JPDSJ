<?php 
require_once 'functions.php';
add();
include HEADER;

?>


<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Cadastro de especificações </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-6 offset-md-3">
		<form action="add.php" method="post" role="form">

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione o setor*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado um setor"></i> 
							
							<select class="form-control" id="cod_set" onchange="pesquisarProduto();">
							<?php
							   foreach ($setores as $setor) 
							  {
					        ?>
									<option value="<?php echo $setor['ID_SETOR']?>"><?php echo $setor['SETOR']?></option>
							<?php
							    }
						    ?>
							</select>
							
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione o produto*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado um produto"></i> 
							
							<select class="form-control" name="especificacao[ID_SETOR_PRODUTO]" id="produto">
							</select>
							
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Especificação*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado uma especificação"></i> 
							
							<input type="text" class="form-control" id="campo_senha"
							name="especificacao[ESPECIFICACAO]" placeholder="Especificação" required
							oninput="verificar_senha();" />
							
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione a unidade de medida*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado uma unidade de medida"></i> 
							
							<select class="form-control" name="especificacao[ID_UM]">
							<?php
							   foreach ($unidades as $um) 
							  {
					        ?>
									<option value="<?php echo $um['ID_UM']?>"><?php echo $um['UM']?></option>
							<?php
							    }
						    ?>
							</select>
							
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>


<script>

function pesquisarProduto()
{
	var x = document.getElementById('cod_set').value;
	$.ajax
	({
		url: "getProduto.php", 
		type: "POST", 
		data: "ID_SETOR="+x,
		success: function(result)
		{
			var produto = JSON.parse(result)
			document.getElementById('produto').innerHTML = "";
			for(i=0;i<produto.length;i++)
			{
				document.getElementById('produto').innerHTML += "<option value='"+ produto[i]["ID_SETOR_PRODUTO"] +"'>"+produto[i]["NOME_PRODUTO"]+"</option>";				
			}
		}
	});
}

pesquisarProduto();

</script>
