<?php 
require_once '../../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $especificacoes;
	$especificacoes = find_all('VIEW_ESPECIFICACAO');
	
}

function view($id)
{
    if(isset($id))
    {
        global $especificacoes;
        $result = find_id('VIEW_ESPECIFICACAO','ID_ESPECIFICACAO',$id);
        if(count($result)!=0)
        {
            $especificacoes = $result[0];
        }
        else
        {
            echo "<script>alert('ID Não Encontrado');</script>";
            $_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
            $_SESSION['type'] = 'danger';
            header('Location: index.php');//
            exit;
        }
        
    }
    else
    {
        echo "<script>alert('ID não encontrado');</script>";
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
        
    }
}

function add()
{
	if(isset($_POST['submit']))
	{
	  
	    $especificacao = $_POST['especificacao'];
	    
	    $all = find_all('tbl_especificacao');
	    
	    $already_exists=false;
	    
	    foreach ($all as $set)
	    {
	        
	        if(strtolower($set['ID_SETOR_PRODUTO'])==strtolower($especificacao['ID_SETOR_PRODUTO']) && 
	           strtolower($set['ESPECIFICACAO'])==strtolower($especificacao['ESPECIFICACAO']) && 
	           strtolower($set['ID_UM'])==strtolower($especificacao['ID_UM']))
	        {
	            
	            $already_exists = true;
	            break;
	        }
	    }
	    
	    if($already_exists)
	    {
	        $_SESSION['type']= "danger";
	        $_SESSION['message']= "Este Setor Já Existe";
	        // fazer um modal para se existe e tals
	    }
	    else
	    {
	        save('tbl_especificacao', $especificacao);
	        
	        if($_SESSION['type']=="success")
	        {
	        $_SESSION['message']= "Setor adicionado com sucesso";
	        header('Location: index.php');
	        exit;
	        }
	    }
	}
	
	global $setores, $unidades;
	$setores = find_all('tbl_setor');
    $unidades = find_all('tbl_um');
}


function edit($id)
{
    if(isset($id))
    {
        if(isset($_POST['submit']))
        {
            $especificacao = $_POST['especificacao'];
            
            $all = find_all('VIEW_ESPECIFICACAO');
            
            $already_exists=false;
            foreach ($all as $esp)
            {
                if(strtolower($esp['ESPECIFICACAO'])==strtolower($especificacao["ESPECIFICACAO"]))
                {
                    echo $esp['ESPECIFICACAO']."-".$especificacao["ESPECIFICACAO"];
                    exit();
                    $already_exists = true;
                    break;
                }
            }
            
            if($already_exists)
            {
                echo "<script>alert('Especificação Ja Existente');</script>";
                $_SESSION['type']= "danger";
                $_SESSION['message']= "Este nome de usuário ou E-Mail já existe";
                
            }
            else
            {
                
                update('tbl_especificacao', $especificacao, 'ID_ESPECIFICACAO', $id);
                
                if($_SESSION['type']=="success")
                {
                    
                    echo "<script>alert('Especificação Cadastrada com Sucesso');</script>";
                    $_SESSION['message']= "Usuário alterado com sucesso";
                    header('Location: index.php');
                    exit;
                }
            }
        }
        
    global $setores, $unidades;
	$setores = find_all('tbl_setor');
    $unidades = find_all('tbl_um');
    
        view($id);
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
    }
}


function delete($id)
{
    if(isset($id))
    {
        remove('TBL_ESPECIFICACAO', 'ID_ESPECIFICACAO', $id);
        if($_SESSION['type']=="success")
        {
            
            $_SESSION['message']= "Especificação excluida com sucesso";
            header('Location: index.php');
            exit;
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}