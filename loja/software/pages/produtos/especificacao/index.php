<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/produtos/especificacao/add.php']) && $menu_acessos['/pages/produtos/especificacao/add.php']);
$edit = (isset($menu_acessos['/pages/produtos/especificacao/edit.php']) && $menu_acessos['/pages/produtos/especificacao/edit.php']);
$delete = (isset($menu_acessos['/pages/produtos/especificacao/delete.php']) && $menu_acessos['/pages/produtos/especificacao/delete.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Especificação dos produtos</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


		<div class="row">
			<div class="col-lg-1">
				
			</div>
		</div>


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
        <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Especificações cadastradas
          </div>
              <?php 
	   if($add)
	   {
	?>
          <div class="col-md-1">
      		<a data-toggle="tooltip" data-placement="left" title="Cadastrar uma nova especificação" href="<?php echo BASEURL?>pages/produtos/especificacao/add.php"
				class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				class="fa fa-plus"></i>
			</a>
          </div>
          <?php 
	   }
          ?>
          </div>
          </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>UM</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
              	 
               <?php 
                                foreach ($especificacoes as $especificacao)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $especificacao['NOME_PRODUTO']?></td>
										<td><?php echo $especificacao['ESPECIFICACAO']?></td>
										<td><?php echo $especificacao['UM']?></td>
										<?php
											if($edit || $delete)
											{
										?>
												<td class="text-center">
													<?php 
													 
														if($edit)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Editar especificação selecionada" href="edit.php?id=<?php echo $especificacao['ID_ESPECIFICACAO'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
													<?php 
														}
														if($delete)
														{
													?>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $especificacao['ID_ESPECIFICACAO'];?>" data-id="<?php echo $especificacao['ID_ESPECIFICACAO'];?>" data-especificacao="<?php echo $especificacao['ESPECIFICACAO'];?>" data-tipo='ESPECIFICAÇÃO'><i data-toggle="tooltip" data-placement="left" title="Excluir especificação selecionada" class="fa fa-trash-o"></i></a>
												    <?php 
														}
												    ?>
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
	</div>
</div>


<?php 

include DELETEMODAL;
include FOOTER;
?>