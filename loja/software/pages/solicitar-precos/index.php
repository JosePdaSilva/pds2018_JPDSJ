<?php 
require_once 'functions.php';
index();
include HEADER;

$add = (isset($menu_acessos['/pages/solicitar-precos/add.php']) && $menu_acessos['/pages/solicitar-precos/add.php']);
$view = (isset($menu_acessos['/pages/solicitar-precos/view.php']) && $menu_acessos['/pages/solicitar-precos/view.php']);
$ofertas = (isset($menu_acessos['/pages/verificar-ofertas/view.php']) && $menu_acessos['/pages/verificar-ofertas/view.php']);

?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Solicitação de preços</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


		


<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
          <div class="col-md-11">
          <i class="fa fa-table"></i> Solicitações de preços
          </div>
             <?php 
	   if($add)
	   {
	?>
			<div class="col-md-1">
				<a href="<?php echo BASEURL?>pages/solicitar-precos/add.php"
				  data-toggle="tooltip" data-placement="left"
				  title="Cadastrar uma nova solicitação"
				  class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
				  class="fa fa-plus"></i>
				</a>
			</div>
			<?php 
	   }
			?>
		</div>
          </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Data da solicitação</th>
                  <th>Data da entrega</th>
                  <th>Nome da construtora</th>
                  <th>Nome da obra</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
              
               <?php 
                                foreach ($pedidos as $pedido)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo date('d/m/Y',strtotime($pedido['DATA_PEDIDO']))?></td>
										<td><?php echo date('d/m/Y',strtotime($pedido['DATA_ENTREGA']))?></td>
										
										<td><?php echo $pedido['RAZAO_SOCIAL']?> </td>
										<td><?php echo $pedido['NOME_OBRA']?></td>
										
										<?php
											if($view || $ofertas)
											{
										?>
												<td class="text-center">
													<?php 
													 
														//if($edit)
														{
													?>
															<a data-toggle="tooltip" data-placement="left" title="Informações do pedido" href="view.php?id=<?php echo $pedido['ID_PEDIDO'];?>" class="btn btn-sm btn-info"><i class="fa fa-info"></i></a>
        													
        													<?php 
														}
													?>
        													
													
												   
												</td>
										<?php 
											}
										?>
										    
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
	</div>
</div>



<?php 
include FOOTER;
?>