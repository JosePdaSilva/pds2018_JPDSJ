 <?php 
require_once '../../config.php';
verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $pedidos;
	$pedidos = find_id('VIEW_PEDIDO',"ID_CONSTRUTOR",$_SESSION["id_construtor"]);
}

function view($id)
{
    if(isset($id))
    {
        $qq = new DateTime();
        if(isset($_POST['submit24']))
        {
            update('tbl_itens_pedido',["ENTREGUE"=>"E","DATA_ENTREGUE"=>$qq->format('Y-m-d')], "ID_ITEM_PEDIDO", $_POST['submit24']);
            update('tbl_orcamento',["ENTREGUE"=>"E"], "ID_ITEM_PEDIDO", $_POST['submit24']);
            
            if($_SESSION['type']=='success'){
            $_SESSION['message'] = "Produto recebido.";                
            }
        }
        
        global $pedidos, $itens, $qtdeOfertados, $qtdeFinalizados, $qtdeEntregues, $qtdeEnviados;
        
        $qtdeOfertados = $qtdeFinalizados = $qtdeEntregues = $qtdeEnviados = 0;
        $result = find_id('view_pedido',['ID_PEDIDO','ID_CONSTRUTOR'],[$id,$_SESSION['id_construtor']]);
        
        $result2 = find_id('view_orcamentos','ID_PEDIDO',$id);
        
        if(count($result)!=0)
        {
            $pedidos = $result[0];
            if(count($result2)!=0)
            {
                foreach ($result2 as $item)
                {
                    if(isset($itens[$item['ID_ITEM_PEDIDO']]))
                    {
                        if($itens[$item['ID_ITEM_PEDIDO']]['ENTREGUE']=='E')
                        {
                            
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['ENTREGUE']=='S' && $item['ENTREGUE']=='E')
                        {
                            $qtdeEntregues++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['STATUS_OFERTA']=='F' && $item['ENTREGUE']=='E')
                        {
                            $qtdeEntregues++;
                            $qtdeEnviados++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['STATUS_OFERTA']=='F' && $item['ENTREGUE']=='S')
                        {
                            $qtdeEnviados++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['STATUS_OFERTA']=='O' && $item['ENTREGUE']=='E')
                        {
                            $qtdeFinalizados++;
                            $qtdeEntregues++;
                            $qtdeEnviados++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['STATUS_OFERTA']=='O' && $item['ENTREGUE']=='S')
                        {
                            $qtdeFinalizados++;
                            $qtdeEnviados++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                        elseif($itens[$item['ID_ITEM_PEDIDO']]['STATUS_OFERTA']=='O' && $item['STATUS_OFERTA']=='F')
                        {
                            $qtdeFinalizados++;
                            $itens[$item['ID_ITEM_PEDIDO']] = $item;
                        }
                    }
                    else 
                    {
                        if($item['ENTREGUE']=='E')
                        {
                            $qtdeOfertados++;
                            $qtdeFinalizados++;
                            $qtdeEnviados++;
                            $qtdeEntregues++;
                        }
                        elseif($item['ENTREGUE']=='S')
                        {
                            $qtdeOfertados++;
                            $qtdeFinalizados++;
                            $qtdeEnviados++;
                        }
                        elseif($item['STATUS_OFERTA']=='F')
                        {
                            $qtdeOfertados++;
                            $qtdeFinalizados++;
                        }
                        elseif($item['STATUS_OFERTA']=='O')
                        {
                            $qtdeOfertados++;
                        }
                        $itens[$item['ID_ITEM_PEDIDO']] = $item;
                    }
                }
            }
            else
            {
                $result2 = find_id('view_itens_pedido','ID_PEDIDO',$id);
                foreach ($result2 as $item)
                {
                    $item['STATUS_OFERTA']="";
                    $itens[$item['ID_ITEM_PEDIDO']] = $item;
                }
            }
        }
        else
        {
            $_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
            $_SESSION['type'] = 'danger';
            header('Location: index.php');//
            exit;
        }
        
    }
    else
    {
        $_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');//
        exit;
        
    }
    /*
	if(isset($id))
	{
	    
		global $pedidos, $itens;
		
		$result = find_id('view_pedido',['ID_PEDIDO','ID_CONSTRUTOR'],[$id,$_SESSION['id_construtor']]);
		
		$result2 = find_id('view_itens_pedido','ID_PEDIDO',$id);
		
		if(count($result)!=0 && count($result2)!=0)
		{
		    $pedidos = $result[0];
		    foreach ($result2 as $item)
		    {
		        $itens[$item['ID_ITEM_PEDIDO']] = $item;
		    }
		}
		else 
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
		
	}
	*/
}

function add()
{
    if(isset($_POST['submit']))
    {        
        if(isset($_POST['item']))
        {
            $pedido = $_POST['pedido'];
            $item= $_POST['item'];
            if($item['PRECO_MAX']=='')
                $item['PRECO_MAX']=null;
            
            $id = save_pedido($pedido,$item);
            
            
            if($_SESSION['type']=="success")
            {
                $_SESSION['message']= "Pedido adicionado com sucesso";
                header('Location: view.php?id='.$id);
                exit();
            }
        }
        else 
        {
            $_SESSION['type']= "Danger";
            $_SESSION['message']= "Cadastre ao menos 1 item";
        }
    }
}


function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			
			$perfil = $_POST['perfil'];
			
			$all = find_all('PERFIL_WEB');
			
			$already_exists=false;
			foreach ($all as $perfil_cad)
			{
				if(strtolower($perfil_cad['PERFIL'])==strtolower($perfil["'PERFIL'"]) && $perfil_cad['ID_PERFIL']!=$id)
				{
					$already_exists = true;
					break;
				}
			}
			
			if($already_exists)
			{
				$_SESSION['type']= "danger";
				$_SESSION['message']= "Este Perfil Já Existe existe";
			}
			else
			{
			
				update('PERFIL_WEB', $perfil, 'ID_PERFIL', $id);
				
				if($_SESSION['type']=="success")
				{
					remove('ACESSO_PAGINAS_WEB', 'ID_PERFIL', $id);
					remove('ACESSO_SISTEMAS_WEB', 'ID_PERFIL', $id);
					$acessoPagina= array();
					foreach ($_POST['acesso'] as $acesso)
					{
					    $acessoPagina[] = array
						(
								'ID_PAGINA'=>$acesso,
								'ID_PERFIL'=>$id
						);
						
						
					}
					$acessoSistema = array();
					foreach ($_POST['acessoSistema'] as $acesso)
					{
					    $acessoSistema[] = array
					    (
					        'ID_SISTEMA'=>$acesso,
					        'ID_PERFIL'=>$id
					    );
					}
					
					save_acessos($acessoPagina, $acessoSistema);
					if($_SESSION['type']=="success")
					{
					    $_SESSION['message']= "Perfil editado com sucesso";
					    header('Location: index.php');
					    exit;
					}
				}
			}
		}
		view($id);
		global $paginas;
		$busca = find_all('VIEW_PAGINAS_WEB');
		
		foreach($busca as $pagina)
		{
		    $str="";
		    $arr=array();
		    $arr = explode('/',$pagina['URL']);
		    if(count($arr)>1)
		    {
		        unset($arr[count($arr)-1]);
		        $str = implode('/', $arr);
		    }
		    else
		        $str = "/";
		        
		        $paginas[$pagina['ID_SISTEMA']]['paginas'][$str][] = $pagina;
		        $paginas[$pagina['ID_SISTEMA']]['info'] = array("ID_SISTEMA"=>$pagina['ID_SISTEMA'], "SIGLA_SISTEMA"=>$pagina['SIGLA_SISTEMA'], "NOME_SISTEMA"=>$pagina['NOME_SISTEMA']);
		}
	}
	else 
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
	if(isset($id))
	{
		
		$usuarios = find_id('USUARIOS_WEB', 'ID_PERFIL', $id);
		if(count($usuarios)==0)
		{
			remove('PERFIL_WEB', 'ID_PERFIL', $id);
			if($_SESSION['type']=="success")
			{
				if($_SESSION['ID_PERFIL']==$id)
				{
					header('Location: index.php');
					exit;
				}
				else
				{
					$_SESSION['message']= "Perfil excluido com sucesso";
					header('Location: index.php');
					exit;
				}
			}
			else
			{
				header('Location: index.php');
				exit;
			}
		}
		else
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Não é possível deletar. Há usuários associados a este perfil";
			header('Location: index.php');
			exit;
		}
		
		
	}
	else
	{
		$_SESSION['message'] = "não foi possível deletar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}