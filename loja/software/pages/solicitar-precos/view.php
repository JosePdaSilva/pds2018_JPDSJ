<?php
require_once 'functions.php';
view($_GET['id']);
include HEADER;
$verificar = (isset($menu_acessos['/pages/verificar-ofertas/view.php']) && $menu_acessos['/pages/verificar-ofertas/view.php']);
?>



<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Informações do pedido</span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-12">
	
	
	<div class="card mb-3">
        <div class="card-header">
        <div class="row">
          <div class="col-md-10">
          <i class="fa fa-table"></i> Dados do pedido
          </div>
          <div class="col-md-2">
            <a class=" btn btn-warning btn-sm" data-toggle="tooltip" data-placement="left"
    		   title="Exibição das ofertas deste pedido"
    		   href="<?php echo BASEURL?>pages/verificar-ofertas/view.php?id=<?php echo $_GET['id']?>"
    			 style="font-size: 15px;"> <strong>Ofertas</strong>
    		</a>
          </div>
          </div>
          </div>
        <div class="card-body">
          <div class="row">
          <div class="col-md-4">
          <strong>Data do pedido: </strong><?php 
          $date = new DateTime($pedidos['DATA_PEDIDO']);
          echo $date->format('d / m / Y');?>
          </div>
          <div class="col-md-4">
          <strong>Data da entrega: </strong><?php 
          $date = new DateTime($pedidos['DATA_ENTREGA']);
          echo $date->format('d / m / Y');?>
          </div>
          <div class="col-md-4">
          <strong>Nome da obra: </strong><?php echo $pedidos['NOME_OBRA']?>
          </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-12">
             
              <div class="row">
              <div class="col-md-12">
              		<strong>Logradouro:</strong> <?php echo $pedidos['LOGRADOURO']?> - <strong>Nº:</strong> <?php echo $pedidos['NUMERO']?>
              	</div>
              </div>
               <div class="row">
              
                  <div class="col-md-2">
                  	<strong>CEP:</strong> <?php echo $pedidos['CEP']?>
                  </div>
                  <div class="col-md-3">
                  	<strong>Cidade:</strong> <?php echo $pedidos['MUNICIPIO']?>
                  </div>
                  	  
             	 <div class="col-md-7">
              		<strong>Bairro:</strong> <?php echo $pedidos['BAIRRO']?>
              	</div>
                
              </div>
              </div>
          </div>
          
        </div>
      </div>
	<div class="card mb-3">
		<div class="card-header">
			<i class="fa fa-spinner"></i> Acompanhamento do pedido
		</div>
		<div class="card-body">
			<div class="row">
				
				<div class="col-md-1" style="text-align: center;">
					
				</div>
				
				<div class="col-md-2" style="text-align: center;">
					<span class="fa-stack fa-2x"> <i
						class="fa fa-circle-thin fa-stack-2x" style="color: green"></i> <i
						class="fa fa-check fa-stack-1x" style="color: green"></i>
					</span>
					<p style="color: green">Solicitação de orçamento enviada</p>
					<p style="color: green"><?php echo count($itens) ?> / <?php echo count($itens) ?> produtos solicitados</p>
				</div>
				<div class="col-md-2" style="text-align: center;">
					
					<?php 
						if($qtdeOfertados == count($itens) )
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin fa-stack-2x" style="color: green"></i> <i
								class="fa fa-check fa-stack-1x" style="color: green"></i>
							</span>
							<p style="color: green">Todos os itens foram Ofertados</p>
							<p style="color: green"><?php echo $qtdeOfertados; ?> / <?php echo count($itens) ?> produtos ofertados</p>
					<?php 
						}
						else 
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x"
								style="color: rgb(252, 176, 13)"></i> <i
								class="fa fa-spinner fa-stack-1x"
								style="color: rgb(252, 176, 13)"></i>
							</span>
							<p style="color: rgb(252, 176, 13)">Os fornecedores estão
								analisando sua demanda</p>
							<p style="color: rgb(252, 176, 13)"><?php echo $qtdeOfertados; ?> / <?php echo count($itens) ?> produtos ofertados</p>
					<?php 
						}
					?>
				</div>
				
				<div class="col-md-2" style="text-align: center;">
					<?php 
						if($qtdeFinalizados == count($itens) )
						{
					?>
								<span class="fa-stack fa-2x"> <i
									class="fa fa-circle-thin fa-stack-2x" style="color: green"></i> <i
									class="fa fa-check fa-stack-1x" style="color: green"></i>
								</span>
								<p style="color: green">Sua empresa já confirmou todas as ofertas</p>
								<p style="color: green"><?php echo $qtdeFinalizados; ?> / <?php echo count($itens) ?> ofertas confirmadas</p>
					<?php
						}
						elseif($qtdeFinalizados>0 || $qtdeOfertados>0 )
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x"
								style="color: rgb(252, 176, 13)"></i> <i
								class="fa fa-spinner fa-stack-1x"
								style="color: rgb(252, 176, 13)"></i>
							</span>
							<p style="color: rgb(252, 176, 13)">Aguardando sua empresa confirmar</p>
							<p style="color: rgb(252, 176, 13)"><?php echo $qtdeFinalizados; ?> / <?php echo count($itens) ?> ofertas confirmadas</p>
					<?php 
						}
						else
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x" style="color: gray"></i> <i
								class="fa fa-ellipsis-h fa-stack-1x" style="color: gray"></i>
							</span>
							<p style="color: gray">Aguardando ofertas</p>
							<p style="color: gray"><?php echo $qtdeFinalizados; ?> / <?php echo count($itens) ?> ofertas confirmadas</p>
								
					<?php 
						}
					?>
				</div>
				<div class="col-md-2" style="text-align: center;">
					<?php 
					   if($qtdeEnviados == count($itens) )
						{
					?>
								<span class="fa-stack fa-2x"> <i
									class="fa fa-circle-thin fa-stack-2x" style="color: green"></i> <i
									class="fa fa-check fa-stack-1x" style="color: green"></i>
								</span>
								<p style="color: green">Todos os produtos foram enviados</p>
								<p style="color: green"><?php echo $qtdeEnviados; ?> / <?php echo count($itens) ?> Pedidos saíram para entrega</p>
					<?php
						}
						elseif($qtdeEnviados>0 || $qtdeFinalizados>0)
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x"
								style="color: rgb(252, 176, 13)"></i> <i
								class="fa fa-spinner fa-stack-1x"
								style="color: rgb(252, 176, 13)"></i>
							</span>
							<p style="color: rgb(252, 176, 13)">Aguardando pedidos saírem para entrega</p>
							<p style="color: rgb(252, 176, 13)"><?php echo $qtdeEnviados; ?> / <?php echo count($itens) ?> Pedidos saíram para entrega</p>
					<?php 
						}
						else 
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x" style="color: gray"></i> <i
								class="fa fa-ellipsis-h fa-stack-1x" style="color: gray"></i>
							</span>
							<p style="color: gray">Aguardando finalizações da empresa</p>
							<p style="color: gray"><?php echo $qtdeEnviados; ?> / <?php echo count($itens) ?> Pedidos saíram para entrega</p>
							
					<?php 
						}
					?>
					
				</div>
				<div class="col-md-2" style="text-align: center;">
					<?php 
					   if($qtdeEntregues == count($itens) )
						{
					?>
								<span class="fa-stack fa-2x"> <i
									class="fa fa-circle-thin fa-stack-2x" style="color: green"></i> <i
									class="fa fa-check fa-stack-1x" style="color: green"></i>
								</span>
								<p style="color: green">Todos os produtos foram recebidos</p>
								<p style="color: green"><?php echo $qtdeEntregues; ?> / <?php echo count($itens) ?> produtos recebidos</p>
					<?php
						}
						elseif($qtdeEntregues>0 || $qtdeEnviados>0)
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x"
								style="color: rgb(252, 176, 13)"></i> <i
								class="fa fa-spinner fa-stack-1x"
								style="color: rgb(252, 176, 13)"></i>
							</span>
							<p style="color: rgb(252, 176, 13)">Aguardando recebimento dos produtos</p>
							<p style="color: rgb(252, 176, 13)"><?php echo $qtdeEntregues; ?> / <?php echo count($itens) ?> produtos recebidos</p>
					<?php 
						}
						else 
						{
					?>
							<span class="fa-stack fa-2x"> <i
								class="fa fa-circle-thin  fa-stack-2x" style="color: gray"></i> <i
								class="fa fa-ellipsis-h fa-stack-1x" style="color: gray"></i>
							</span>
							<p style="color: gray">Aguardando itens serem enviados</p>
							<p style="color: gray"><?php echo $qtdeEntregues; ?> / <?php echo count($itens) ?> produtos reecbidos</p>
							
					<?php 
						}
					?>
					
				</div>
				<div class="col-md-1" style="text-align: center;">
					
				</div>
			</div>
		</div>
	</div>
	
	<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Itens do pedido</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" style="border:1px solid #ccc" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Especificação</th>
                  <th>Preço Max</th>
                  <th>Qtd Solicitada</th>
                  <th>UM</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              
               <?php 
               /*echo '<pre>';
               print_r($itens);
               echo '</pre>';*/
               if(count($itens)>0)
                                foreach ($itens as $item)
								{
								    /*echo '<pre>';
								    print_r($item);
								    echo '</pre>';*/
						?>
									<tr class="odd gradeX">
										<td><?php echo $item['NOME_PRODUTO']?> </td>
										<td><?php echo $item['ESPECIFICACAO']?></td>
										<td><?php echo $item['PRECO_MAX']?></td>
										<td><?php echo $item['QTD_SOLICITADA']?></td>
										<td><?php echo $item['UM']?></td>   
										<td style="text-align:center">
									<?php 
										  if($item['STATUS_OFERTA']== '' || $item['STATUS_OFERTA']== 'R') 
										  {
								      ?>
								      		<button type="button" data-toggle="tooltip" data-placement="left" title="Sem ofertas para este produto" class="btn btn-warning">Sem ofertas</button>
								      <?php 
										  }
										  elseif($item['STATUS_OFERTA']=='O')
										  {
										      if($verificar)
										      {
						              ?>
						              		<a href="<?php echo BASEURL.'pages/verificar-ofertas/view.php?id='.$item['ID_PEDIDO'];?>" target="_blank" data-toggle="tooltip" data-placement="left" title="Aguardando aprovação" class="btn btn-warning"><i class="fa fa-edit"></i> Aguardando aprovação</a>
						              <?php
										      }
										      else
										      {
									   ?>
									   				<button type="button" data-toggle="tooltip" data-placement="left" title="Aguardando aprovação" class="btn btn-warning">Aguardando aprovação</button>
									   <?php
										      }
										  }
    						              elseif($item['STATUS_OFERTA']=='F' && $item['ENTREGUE']=='N')
    						              {
						              ?>
 						              		<button type="button" data-toggle="tooltip" data-placement="left" title="Aguardando o envio do produto" class="btn btn-warning" ><i class="fa fa-truck"></i> Aguardando envio</button>
						              <?php 
    						              }
    						              elseif($item['ENTREGUE']=='S')
    						              {
						              ?>	<form action="view.php?id=<?php echo $_GET['id']?>" method="post">
					              			<button data-toggle="tooltip" data-placement="left" title="Produto recebido" class="btn btn-warning" type="submit" id="btn_submit24" name="submit24" value="<?php echo $item['ID_ITEM_PEDIDO']?>"><i class="fa fa-square-o"></i> Recebi</button>
								     		</form>
								      <?php 
    						              }
    						             elseif($item['ENTREGUE'] == 'E')
    						             {
								      ?>
								      <button type="button" data-toggle="tooltip" data-placement="left" title="Produto recebido" class="btn btn-success" ><i class="fa fa-check-square-o"></i> Produto Recebido</button>
										<?php 
    						             }
										?>
										</td> 
									</tr>
						<?php 
								}
						?>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
      		<div class="row">
				<div class="col-md-4 offset-md-4 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-arrow-left" aria-hidden="true"></i>  Voltar
					</a>
				</div>
			</div>
		<br>
	</div>
</div>


<?php 
include FOOTER;
?>

