<?php 
require_once 'functions.php';
index();
include HEADER;


?>



<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Usuários </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card mb-3">
        <div class="card-header">
          <div class="row">
        	<div class="col-md-11">	
          		<i class="fa fa-table"></i> Usuários cadastrados
          	</div>
          	<div class="col-md-1">
          		<a data-toggle="tooltip" data-placement="left" title="Cadastrar um novo usuário"
          			href="<?php echo BASEURL?>pages/usuarios/add.php"
					class="btn btn-primary btn-sm btn-block" style="font-size: 15px;"> <i
					class="fa fa-plus"></i>
				</a>
          	</div>
          </div>
         </div>
          
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead class="text-center">
                <tr>
                  <th>Usuário</th>
                  <th>E-mail</th>
                  <th>Perfil</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
							foreach ($usuarios as $usuario)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $usuario['USERNAME']?></td>
										<td> <?php echo $usuario['EMAIL']?> </td>
										<td><?php echo $usuario['NOME_PERFIL']?></td>
										
												<td class="text-center">
															<a data-toggle="tooltip" data-placement="left" title="Editar usuário selecionado" href="edit.php?id=<?php echo $usuario['ID_USUARIO'];?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
												    		<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $usuario['ID_USUARIO']?>" data-id="<?php echo $usuario['ID_USUARIO']?>" data-usuario="<?php echo $usuario['USERNAME']?>" data-tipo='USUÁRIO'><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="left" title="Excluir usuário selecionado"></i></a>
												</td>
									</tr>
						<?php 
								}
						?>
                
              </tbody>
              
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Atualizado ontem às 11:59</div> -->
      </div>
	</div>
</div>


<?php 
include DELETEMODAL;
include FOOTER;
?>