<?php 
require_once 'functions.php';
edit($_GET['id']);

include HEADER;



?>
<script type="text/javascript">
function changeDoc()
{
	if(document.getElementById('fnome').checked)
		tipoChanged('fnome');
	else
		tipoChanged('fcodigo');
	document.getElementById("codi_uoper").disabled=document.getElementById("check_docente").checked;

}

function tipoChanged(id)
{
	if(id=="fnome")
	{
	  document.getElementById('codigo').hidden= true;
	  document.getElementById('nome').hidden= false;
	  document.getElementById('codigo').disabled= true;
	  if(document.getElementById("check_docente").checked)
	  {
    	  document.getElementById('nome').disabled= false;
	  }
	  else
	  {
		  document.getElementById('nome').disabled= true;
	  }
	}
	else
	{
		document.getElementById('codigo').hidden= false;
		document.getElementById('nome').hidden= true;
		document.getElementById('nome').disabled= true;
		if(document.getElementById("check_docente").checked)
        {
			document.getElementById('codigo').disabled= false;
        }
		else
		{
			document.getElementById('codigo').disabled= true;
		}
	}
}
</script>


<div class="row">
	<div class="col-md-12">
		<h1 class="page-header text-center">
			<span>Edição de usuários </span>
		</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>


<div class="row">
	<div class="col-md-4 offset-md-4">
		<form action="edit.php?id=<?php echo $_GET['id'];?>" method="post" role="form">

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label>Selecione o Perfil*</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado um perfil"></i> <select
							class="form-control" name="usuario[ID_PERFIL]">
								<?php
							
								$perfil = find_all('TBL_PERFIS');
								foreach ($perfis as $perfil){
					        ?>
									<option value="<?php echo $perfil['ID_PERFIL']?>" <?php if($perfil['ID_PERFIL']==$_GET['id']){echo 'selected';}?>><?php echo $perfil['NOME_PERFIL']?></option>
							<?php 
								}
							?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_usuario" class="form-group has-feedback">
						<label for="campo_usuario">Username*</label> <i class=""
							id="icon_campo_usuario"></i>
						<!-- icone de certo ou errado -->
						<span class="font-weight-light" style="color: red"
							id="resposta_campo_usuario"></span>
						<!-- se icone errado ou warning, o motivo -->
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o nome do usuario"></i> <input
							type="text" class="form-control" id="campo_usuario"
							name="usuario[USERNAME]" placeholder="Username" required
							autofocus oninput="verificar_username();" value="<?= $usuario['USERNAME']?>" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_senha" class="form-group has-feedback">
						<label for="campo_senha">Senha*</label> <i class=""
							id="icon_campo_senha"></i>
						<!-- icone de certo ou errado -->
						<span class="font-weight-light" style="color: red"
							id="resposta_campo_senha"></span>
						<!-- se icone errado ou warning, o motivo -->
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informada uma senha com pelo menos 4 caracteres para o referido usuario"></i>

						<input type="password" class="form-control" id="campo_senha"
							name="usuario[PASSWORD]" placeholder="Senha"
							oninput="verificar_senha();"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div id="div_campo_e-mail" class="form-group has-feedback">
						<label for="campo_e-mail">E-mail*</label> <i class=""
							id="icon_campo_e-mail"></i>
						<!-- icone de certo ou errado -->
						<span class="font-weight-light" style="color: red"
							id="resposta_campo_e-mail"></span>
						<!-- se icone errado ou warning, o motivo -->
						<i class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado um email válido para recuperação de senha"></i>

						<input type="email" class="form-control" id="campo_e-mail"
							name="usuario[EMAIL]" placeholder="Email" required
							oninput="verificar_email();" value="<?= $usuario['EMAIL']?>" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-xs-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Editar
					</button>
				</div>

				<div class="col-md-6 col-xs-6">
					<a href="index.php" class="btn btn-secondary btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>


<?php 
include FOOTER;
?>

