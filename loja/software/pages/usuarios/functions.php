<?php 

require_once '../../config.php';
//verify(str_replace(BASEURL, "/", $_SERVER['PHP_SELF']));

function index()
{
	global $usuarios;
	
	
	if(isset($_SESSION['id_empresa'])){
	    $usuarios = find_id('VIEW_USUARIOS',"ID_EMPRESA",$_SESSION['id_empresa']);
	}
	else 
	{
	    $usuarios = find_all('VIEW_USUARIOS');
	}
}

function view($id)
{
	if(isset($id))
	{
		global $usuario;
		$props = ["ID_USUARIO"];
		$values = [$id];
		
		if($_SESSION['id_empresa']!='')
		{
		    $props[] = "ID_EMPRESA";
		    $values[] = $_SESSION['id_empresa'];
		}
		$result = find_id('VIEW_USUARIOS',$props,$values);
		
		if(count($result)==0)
		{
			$_SESSION['message'] = "não foi possível vizualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
		$usuario = $result[0];
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}

function add()
{
	if(isset($_POST['submit']))
	{
		$usuario = $_POST['usuario'];
		
		if(isset($_SESSION['id_empresa']))
		{
		    $usuario['ID_EMPRESA'] = $_SESSION['id_empresa'];
		}
		$all = find_all('TBL_USUARIOS');
		
		$already_exists=false;
		foreach ($all as $user)
		{
		    if(strtolower($user['USERNAME'])==strtolower($usuario["USERNAME"]) || strtolower($user['EMAIL'])==strtolower($usuario["EMAIL"]))
			{
				$already_exists = true;
				break;
			}
		}
		
		if($already_exists)
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Este nome de usuário já existe";
			global $perfis;
			$perfis=find_all('TBL_PERFIS');
		}
		else
		{
			$usuario["PASSWORD"] = md5($usuario["PASSWORD"]);
			save('TBL_USUARIOS', $usuario);
			
			if($_SESSION['type']=="success")
			{
				$_SESSION['message']= "Usuário adicionado com sucesso";
				header('Location: index.php');
				exit;
			}
			else
			{
				global  $perfis;
				$perfis=find_all('TBL_PERFIS');
			}
		}
	}
	else
	{
		global  $perfis;
		
		$perfis=find_all('TBL_PERFIS');
	}
}



function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			$usuario = $_POST['usuario'];
			
			
			$all = find_all('view_usuarios');
			
			$already_exists=false;
			foreach ($all as $user)
			{
			    if((strtolower($user['USERNAME'])==strtolower($usuario["USERNAME"]) && $user['ID_USUARIO']!=$id) || (strtolower($user['EMAIL'])==strtolower($usuario["EMAIL"]) && $user['ID_USUARIO']!=$id))
				{
					$already_exists = true;
					break;
				}
			}
			
			if($already_exists)
			{
				$_SESSION['type']= "danger";
				$_SESSION['message']= "Este nome de usuário ou E-Mail já existe";
				
			}
			else
			{
				if($usuario["PASSWORD"]!="")$usuario["PASSWORD"] = md5($usuario["PASSWORD"]);
				else
				{
				    unset($usuario["PASSWORD"]);
				    $usuario['TEMP_PASSWORD']='N';
				}
				update('TBL_USUARIOS', $usuario, 'ID_USUARIO', $id);
				
				if($_SESSION['id_usuario']==$id)
				{
					$_SESSION['usuario'] = $usuario["USERNAME"];
					$_SESSION['id_perfil'] = $usuario["ID_PERFIL"];
					
				}
				
				if($_SESSION['type']=="success")
				{
					
					$_SESSION['message']= "Usuário alterado com sucesso";
					header('Location: index.php');
					exit;
				}
			}
		}
		view($id);
		global $perfis;
		
		$perfis=find_all('VIEW_PERFIS');
	}
	else
	{
		$_SESSION['message'] = "não foi possível vizualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}



function delete($id)
{
    if(isset($id))
    {
        view($id);
        remove('TBL_USUARIOS', 'ID_USUARIO', $id);
        if($_SESSION['type']=="success")
        {
            if($_SESSION['id_usuario']==$id)
            {
                header('Location: '.BASEURL.'gestao/logoff.php');
                exit;
            }
            else
            {
                $_SESSION['message']= "Usuário excluido com sucesso";
                header('Location: index.php');
                exit;
            }
        }
    }
    else
    {
        $_SESSION['message'] = "não foi possível deletar: ID não especificado";
        $_SESSION['type'] = 'danger';
        header('Location: index.php');
        exit;
    }
}

?>