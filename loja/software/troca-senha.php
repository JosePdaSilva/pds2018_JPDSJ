<?php
require_once('config.php');

if(isset($_SESSION['usuario']))
{
	header('Location: home.php');
}
require_once 'sendEmail.php';
if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['token']):
	$e_user = $_POST['email_user'];
	$_SESSION['tokenSession'] = $_POST['token'];

	if (filter_var($e_user, FILTER_VALIDATE_EMAIL)) {
		try {
			$findAccount = find('USUARIOS_WEB', 'EMAIL', $e_user);

			if (count($findAccount) > 0) {
				$fields = $findAccount[0];
				$username = $fields['USERNAME'];

				$bytes = openssl_random_pseudo_bytes(4);
				$pwdGenerator = bin2hex($bytes);
				
				$today = new DateTime();
				$update = update('USUARIOS_WEB', array('PASSWORD' => md5($pwdGenerator), 'TEMP_PASSWORD'=>'S', 'DT_CHANGED_PASS'=>$today->format('d.m.Y H:i:s')), 'ID_USUARIO', $fields['ID_USUARIO']);

				if((new sendemail(array($e_user, $username),  $pwdGenerator))==true)
				{
					$em = explode("@",$e_user);
					$name = $em[0];
					$len = strlen($name);
					$showLen = floor($len/2);
					$str_arr = str_split($name);
					for($ii=$showLen;$ii<$len;$ii++){
						$str_arr[$ii] = '*';
					}
					$em[0] = implode('',$str_arr);
					$hide_e_user = implode('@',$em);
					
					$_SESSION['hide_e_user']=$hide_e_user;
					header('Location: ?transaction='.$_POST['token']);
					exit();
				}
				
					
			}
			else {
				$response = array(
						'status' => array(
								'error' => 'Este email não foi cadastrado em nossa base de dados'
						)
				);
				unset($_SESSION['type']);
				unset($_SESSION['message']);
			}
		
		} catch (Exception $e) {
			$response = array(
					'status' => array(
							'error' => $e->getMessage()
					)
			);
			unset($_SESSION['type']);
			unset($_SESSION['message']);
		}
	  
	} else {
	  $response = array(
	  	'status' => array(
	  		'error' => 'E-mail invalido!'
	  	)
	  );
	}
	$class = isset($response['status']['error']) ? 'danger' : '';
	$statusMessage = isset($response['status']['error']) ? $response['status']['error'] : '';
	
	

endif;



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Diário de classe On-Line</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Digite seu e-mail</h3>
                    </div>
                    <div class="panel-body">
                    <?php 
                    if(isset($statusMessage))
                    {
                    ?>
                    <div class="row">
                    	<div class="col-lg-12">
							<div class="alert alert-<?php echo $class ?>" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo $statusMessage; ?>
							</div>
						</div>
					</div>
					<?php 
                    }
					?>

						<?php
						$transaction = isset($_GET['transaction'])? $_GET['transaction']:"";
						if (!empty($transaction) && isset($_SESSION['tokenSession'])){
							if($transaction == $_SESSION['tokenSession'])
							{
						?>
						<div class="row">
							<div class="col-lg-12 ">
								<div class="alert alert-success" role="alert">
									Sua novo senha foi enviado para seu e-mail <strong> (<?php echo $_SESSION['hide_e_user']; ?>) </strong>
								</div>
								<a href="index.php" class="pull-right">Fazer login</a>
							</div>
						</div>

						<?php 
							unset($_SESSION['hide_e_user']);
							unset($_SESSION['tokenSession']);
							}
						}
						else{ ?>
                        <form role="form" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
                            <fieldset>
                            	<input type="hidden" name="token" value="<?php echo md5(md5(sha1("token"))); ?>">
								<div class="row" >
									<div class="col-lg-12 form-group">
	                                    <input class="form-control" id="email_user" placeholder="E-Mail" name="email_user" type="text" autofocus>
	                                </div>
                                </div>
                               	 <div class="row" >
                               	 	<div class="col-lg-12 form-group">
										<button type="submit" class="btn btn-primary" id="btn_login">Enviar senha para o e-email</button>
									</div>
								 </div>
                            </fieldset>
                        </form>
                    	<?php }?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>
</html>