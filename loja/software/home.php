<?php
require_once 'functions.php';
index();
include HEADER;


?>
       <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-tags"></i>
              </div>
              <div class="mr-5">Cadastrar novas roupas</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo BASEURL?>pages/roupas/index.php">
              <span class="float-left">Cadastrar</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-user"></i>
              </div>
              <div class="mr-5">Cadastrar perfis</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo BASEURL?>pages/perfis/index.php">
              <span class="float-left">Perfis</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-users"></i>
              </div>
              <div class="mr-5">Cadastre novos usuários</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo BASEURL?>pages/usuarios/index.php">
              <span class="float-left">Cadastrar</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-home"></i>
              </div>
              <div class="mr-5">Ir para o site</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="../index.php">
              <span class="float-left">Site</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
      
    
<?php 
include FOOTER;
?>

<script>
var ctx = document.getElementById("graficoMovimentacao");
var d = new Date();
var month = new Array();
month[1] = "Janeiro";
month[2] = "Fevereiro";
month[3] = "Março";
month[4] = "Abril";
month[5] = "Maio";
month[6] = "Junho";
month[7] = "Julho";
month[8] = "Agosto";
month[9] = "Setembro";
month[10] = "Outubro";
month[11] = "Novembro";
month[12] = "Dezembro";
var mesAtual = d.getMonth()+1;
var mesesColocar = new Array();
var itensMeses = new Array();
var vendasMax=0;
/*
for(i=0;i<6;i++)
{
	if(mesAtual-(6-i)<1)
	{
		mesesColocar[i] = 12+mesAtual-(6-i);
	}
	else
	{
		mesesColocar[i] = mesAtual-(6-i);
	}

	$.ajax
	({
		url: "getInfoMes.php", 
		type: "POST", 
		data: "mes="+mesesColocar[i],
		async: false,
		success: function(result)
		{
			itensMeses[i] = JSON.parse(result)
		}
	});
	if(itensMeses[i].length>vendasMax)
		vendasMax=itensMeses[i].length;
}



var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [month[mesesColocar[0]], month[mesesColocar[1]], month[mesesColocar[2]], month[mesesColocar[3]], month[mesesColocar[4]], month[mesesColocar[5]]],
    datasets: [{
      label: "Compras",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: [itensMeses[0].length, itensMeses[1].length, itensMeses[2].length, itensMeses[3].length, itensMeses[4].length, itensMeses[5].length],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'mês'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: vendasMax,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

data: [<?php //echo count($itens[0]);?>, <?php //echo count($itens[1]);?>, <?php //echo count($itens[2]);?>, <?php //echo count($itens[3]);?>, <?php //echo count($itens[4]);?>, <?php //echo count($itens[5]);?>]
*/

var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["<?php echo $meses[$mesesColocar[0]]?>", "<?php echo $meses[$mesesColocar[1]]?>", "<?php echo $meses[$mesesColocar[2]]?>", "<?php echo $meses[$mesesColocar[3]]?>", "<?php echo $meses[$mesesColocar[4]]?>", "<?php echo $meses[$mesesColocar[5]]?>"],
    datasets: [{
      label: "Compras feitas",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: [<?php echo $vendasRealizadas[0];?>, <?php echo $vendasRealizadas[1];?>, <?php echo $vendasRealizadas[2];?>, <?php echo $vendasRealizadas[3];?>, <?php echo $vendasRealizadas[4];?>, <?php echo $vendasRealizadas[5];?>]
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'mês'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: <?php echo $qtdeMax;?>,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
</script>
   