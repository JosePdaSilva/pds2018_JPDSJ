//document.getElementById("qtde_contatos").value
//var atual_end=document.getElementById("qtde_enderecos").value;
var atual_end =0;
function addEndereco(excluir=true) {
	
	
	var divEnd = document.createElement("DIV");
	divEnd.id="endereco"+atual_end;
	
	var divTipoEnd = document.createElement("DIV");
	divTipoEnd.setAttribute("class", "form-group");
	
	
	//--LINHA 1--//
	var divLinha1 = document.createElement("DIV");
	divLinha1.setAttribute("class", "form-group");
	

	var divLinhaRow1 = document.createElement("DIV");
	divLinhaRow1.setAttribute("class", "form-row");
		
		//--CEP--//
		var divCEP = document.createElement("DIV");
		divCEP.setAttribute("class", "col-md-2 col-sm-4 col-xs-6 ");
		
		var txtcep = document.createElement("INPUT");
		txtcep.name="endereco["+atual_end+"][CEP]";
		txtcep.id="cep"+atual_end;
		txtcep.type="text";
		txtcep.placeholder="Somente Números";
		txtcep.setAttribute("OnKeyPress", "formatar('#####-###', this)");
		txtcep.setAttribute("oninput","buscarCEP("+atual_end+")");
		txtcep.setAttribute("maxlength", "9");
		txtcep.setAttribute("class", "form-control");
		txtcep.required="required";
		
		divCEP.appendChild(document.createTextNode("CEP"));
		divCEP.appendChild(txtcep);
		//--CEP--//
		
		//--RUA--//
		var divRua = document.createElement("DIV");
		divRua.setAttribute("class", "col-md-5 col-sm-4 col-xs-6 ");
		
		var txtrua = document.createElement("INPUT");
		txtrua.name="endereco["+atual_end+"][LOGRADOURO]";
		txtrua.id="logradouro"+atual_end;
		txtrua.type="text";
		txtrua.placeholder="Logradouro";
		txtrua.setAttribute("class", "form-control");
		txtrua.required="required";
		
		divRua.appendChild(document.createTextNode("Logradouro"));
		divRua.appendChild(txtrua);
		//--RUA--//
		
		//--NUMERO--//
		var divN = document.createElement("DIV");
		divN.setAttribute("class", "col-md-2 col-sm-4 col-xs-6 ");
		
		var txtn = document.createElement("INPUT");
		txtn.name="endereco["+atual_end+"][NUMERO]";
		txtn.id="numero"+atual_end;//
		txtn.type="text";
		txtn.placeholder="Número";
		txtn.setAttribute("class", "form-control");
		
		divN.appendChild(document.createTextNode("Número"));
		divN.appendChild(txtn);
		//--NUMERO--//
		
		//--DESCRICAO--//
		var divDescricao = document.createElement("DIV");
		divDescricao.setAttribute("class", "col-md-3 col-sm-4 col-xs-6 ");
		
		var txtDescricao = document.createElement("INPUT");
		txtDescricao.name="endereco["+atual_end+"][DESCRICAO]";
		txtDescricao.id="descricao"+atual_end;//
		txtDescricao.type="text";
		txtDescricao.placeholder="Principal, Estoque, Sede, Loja...";
		txtDescricao.setAttribute("class", "form-control");
		txtDescricao.required="required";
		
		divDescricao.appendChild(document.createTextNode("Descrição"));
		divDescricao.appendChild(txtDescricao);
		//--DESCRICAO--//
	

	divLinhaRow1.appendChild(divDescricao);
	divLinhaRow1.appendChild(divCEP);
	divLinhaRow1.appendChild(divRua);
	divLinhaRow1.appendChild(divN);
	
	divLinha1.appendChild(divLinhaRow1);
	
	//--LINHA 1--//
	
	//--LINHA 2--//
	var divLinha2 = document.createElement("DIV");
	divLinha2.setAttribute("class", "form-group");
	
	var divLinhaRow2 = document.createElement("DIV");
	divLinhaRow2.setAttribute("class", "form-row");
		
		//--COMPLEMENTO--//
		var divCompl = document.createElement("DIV");
		divCompl.setAttribute("class", "col-md-3 col-sm-3 col-xs-6 ");
		
		var txtcompl = document.createElement("INPUT");
		txtcompl.name="endereco["+atual_end+"][COMPLEMENTO]";
		txtcompl.id="complemento"+atual_end;
		txtcompl.type="text";
		txtcompl.placeholder="Complemento";
		txtcompl.setAttribute("class", "form-control");
		
		divCompl.appendChild(document.createTextNode("Complemento"));
		divCompl.appendChild(txtcompl);
		//--COMPLEMENTO--//
		
		//--BAIRRO--//
		var divBair = document.createElement("DIV");
		divBair.setAttribute("class", "col-md-4 col-sm-3 col-xs-6 ");
		
		var txtbair = document.createElement("INPUT");
		txtbair.name="endereco["+atual_end+"][BAIRRO]";
		txtbair.id="bairro"+atual_end;
		txtbair.type="text";
		txtbair.placeholder="Bairro";
		txtbair.setAttribute("class", "form-control");
		txtbair.required="required";
		
		divBair.appendChild(document.createTextNode("Bairro"));
		divBair.appendChild(txtbair);
		//--BAIRRO--//
		
		//--CIDADE--//
		var divCid = document.createElement("DIV");
		divCid.setAttribute("class", "col-md-2 col-sm-3 col-xs-6 ");
		
		var txtcid = document.createElement("INPUT");
		txtcid.name="endereco["+atual_end+"][MUNICIPIO]";
		txtcid.id="cidade"+atual_end;
		txtcid.type="text";
		txtcid.placeholder="Cidade";
		txtcid.setAttribute("class", "form-control");
		txtcid.required="required";
		
		divCid.appendChild(document.createTextNode("Cidade"));
		divCid.appendChild(txtcid);
		//--CIDADE--//
		
		//--ESTADO--//
		var divUF = document.createElement("DIV");
		divUF.setAttribute("class", "col-md-2 col-sm-3 col-xs-6 ");
		
		var txtuf = document.createElement("INPUT");
		txtuf.name="endereco["+atual_end+"][ESTADO]";
		txtuf.id="estado"+atual_end;
		txtuf.type="text";
		txtuf.placeholder="Estado";
		txtuf.setAttribute("class", "form-control");
		txtuf.required="required";
		
		divUF.appendChild(document.createTextNode("Estado"));
		divUF.appendChild(txtuf);
		//--ESTADO--//
		
		
		
		//-----BTN deleteEndereco------//
		if(excluir)
		{
			var divBtnexc = document.createElement("DIV");
			divBtnexc.setAttribute("class", "col-md-1 col-sm-1 col-xs-1");
			var btnexc = document.createElement("INPUT");
			btnexc.type="button";
			btnexc.value="X";
			btnexc.setAttribute("onclick", "deleteEndereco('"+divEnd.id+"')");
			btnexc.setAttribute("class", "btn btn-danger");
		
		divBtnexc.appendChild(document.createElement("br"));
		divBtnexc.appendChild(btnexc);
		}
		//-----BTN deleteEndereco------//
		
		
		
		divLinhaRow2.appendChild(divCompl);
		divLinhaRow2.appendChild(divBair);
		divLinhaRow2.appendChild(divCid);
		divLinhaRow2.appendChild(divUF);
		if(excluir)
		{
			divLinhaRow2.appendChild(divBtnexc);
		}
		
		
		divLinha2.appendChild(divLinhaRow2);
		divLinha2.appendChild(document.createElement("HR"));
		divEnd.appendChild(divTipoEnd);
		divEnd.appendChild(divLinha1);
		divEnd.appendChild(divLinha2);
		
		document.getElementById("enderecos").appendChild(divEnd);
		
		
    //var text = "<input type=id='contato['" +atual_end+ "']' value=' "+atual_end+ "'>";
    /*var text = 	"<div class='form-group'>"
					+"<div class='col-md-2 col-sm-3 col-xs-6' >"
						+"Selecione a Opção"
						+"<select class='form-control' id='opcao' name="+"contato['"+name+"']"+" onchange='updatedContato("+atual_end+")'>"
							+"<option value='T'>Telefone</option>"
							+"<option value='C'>Celular</option>"
							+"<option value='E'>Email</option>"
						+"</select>"
					+"</div>"
					+"<div class='col-md-2 col-sm-3 col-xs-6' >"
						+"Selecione o Tipo"
						+"<select class='form-control' id='tipo' name='contato['tipo']'>"
							+"<option value='R'>Residencial</option>"
							+"<option value='C'>Comercial</option>"
							+"<option value='R'>Recado</option>"
						+"</select>"
					+"</div>"
					+"<div class='col-md-2 col-sm-3 col-xs-6 '>"
						+"Valor"
						+"<input type='text' id='valor' name='contato['valor']' required='required' OnKeyPress='formatar('##-#####-####', this)' maxlength='13' placeholder='Telefone' class='form-control col-md-6 col-xs-12'>"
					+"</div>"
				+"</div>"
    */
		if(excluir)
		{
			document.getElementById("qtde_enderecos").value++;
		}
	
	atual_end++;
}

function deleteEndereco(id)
{
	var filha = document.getElementById(id);
	var pai = document.getElementById("enderecos");

	pai.removeChild(filha);
	document.getElementById("qtde_enderecos").value--;
}

var indexDeletados=0;
function deleteEnderecoAntigo(id,id_end,ID_ENDE)
{
	var filha = document.getElementById(id);
	var pai = document.getElementById("enderecosAntigos");

	pai.removeChild(filha);
	
	document.getElementById('tabelaDeletados').innerHTML+="<input type='hidden' name='enderecosDeletados["+indexDeletados+"]["+ID_ENDE+"]' value="+id_end+">";
	
	indexDeletados++;
}


function formatar(mascara, documento){
	  var i = documento.value.length;
	  var saida = mascara.substring(0,1);
	  var texto = mascara.substring(i)
	  
	  if (texto.substring(0,1) != saida){
	            documento.value += texto.substring(0,1);
	  }
	  
	}